@extends('layouts.website')
@section('titulo', 'EETAD Núcleo 0325')
@section('subtitle')
@if($config->nmescola != null && $config->nmescola != "")
{{$config->nmescola}}
@endif
@endsection

@section('styles') 
<link href="css/interno.css" rel="stylesheet">  
<link href="css/quiz.css" rel="stylesheet">  
@endsection

@section('scripts') 
<script type="text/javascript">
    @if (isset($perguntas))
            var q = [
                    @forelse($perguntas as $p)
                    @php
                    $resp_certa = 0
                    @endphp
            {'Q': '{{$p->pergunta}}',
                    'C': [
                            @forelse($p -> respostas as $r)
                            '{{$r->resposta}}',
                            @php
                            if ($r -> certa == 'S'){
                    $resp_certa = $loop -> iteration;
                    }
                    @endphp
                            @empty
                            @endforelse
                    ],
                    'A': {{$resp_certa}},
            },
                    @empty

                    @endforelse
            ];
    @endif
//        var q = [
//            {'Q': 'Carla do you write "Hello World" in an alert box?', 'A': 2, 'C': ['msg("Hello World");', 'alert("Hello World");', 'alertBox("Hello World");']},
//            {'Q': 'Carla do you create a function in JavaScript?', 'A': 3, 'C': ['function:myFunction()', 'function = myFunction()', 'function myFunction()']},
//            {'Q': 'Carla to write an IF statement in JavaScript?', 'A': 1, 'C': ['if (i == 5)', 'if i = 5 then', 'if i == 5 then']},
//            {'Q': 'Carla does a FOR loop start?', 'A': 2, 'C': ['for (i = 0; i <= 5)', 'for (i = 0; i <= 5; i++)', 'for i = 1 to 5']},
//            {'Q': 'Carla is the correct way to write a JavaScript array?', 'A': 3, 'C': ['var colors = "red", "green", "blue"', 'var colors = (1:"red", 2:"green", 3:"blue")', 'var colors = ["red", "green", "blue"]']}
//        ];
</script>    
<script src="{{asset('js/quiz.js')}}"></script> 
@endsection

@section('conteudo') 
<!--    Quiz Section 
-->  <section class="container page-section quiz">

    <div class="row justify-content-center">
        <h3 class=" mt-0 text-center">
            <span class="">Quiz Bíblico</span> -    
           
            <small>Teste já seus conhecimentos!</small></h3>

    </div>
    <br>



    <form class="container" id="form" name="contact-form" action="{{ route('quiz.gerar') }}" method="get">
        {{ csrf_field() }}

        <div class="row " >
            <div class=" col-lg-4 col-md-4 col-sm-12 " >
                <div class="form-group">
                    <label>Name *</label>
                    <input type="text" name="name" id="name" class="form-control form-control-sm" required="required" value="{{old('name')}}">
                </div>
            </div>
            <div class=" col-lg-4 col-md-4 col-sm-12 " >
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" id="email" name="email" class="form-control form-control-sm" required="required" value="{{old('email')}}">
                </div>
            </div>
            <div class=" col-lg-4 col-md-4 col-sm-12 " >
                <div class="form-group">
                    <label>Telefone(Whatsapp)</label>
                    <input type="text" name="telefone" id="telefone" class="form-control form-control-sm tel" value="{{old('telefone')}}">
                </div>
            </div>
        </div>
        <div class="row " >
            <div class=" col-lg-5 col-md-5 col-sm-12 " >
                <div class="form-group ">
                    <label for="nivel" >Nível: </label>

                    <select class="form-control form-control-sm" id="nivel" name="nivel">

                        <option value="FÁCIL" {{ old("nivel") == "FÁCIL"?"selected":""}} >FÁCIL</option>
                        <option value="MÉDIO" {{ old("nivel") == "MÉDIO"?"selected":""}} >MÉDIO</option>
                        <option value="DIFÍCIL" {{ old("nivel") == "DIFÍCIL"?"selected":""}} >DIFÍCIL</option>



                    </select>
                </div>

            </div>
            <div class="col-lg-5 col-md-5 col-sm-12" >
                <div class="form-group">
                    <label for="categoria" >Categoria: </label>

                    <select class="form-control form-control-sm" id="categoria" name="categoria">
                        <option value="TODAS" {{ old("limpar") !== "limpar"?"":"selected"}} >TODAS</option>
                        @foreach($categoria as $a)


                        <option value="{{ $a->id }}" {{(old("categoria")== $a->id && old("limpar") !== "limpar")?'selected':''}} > {{ $a->categoria }}</option>

                        @endforeach


                    </select>
                </div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-12  "  >
                <label for="" > </label>

                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-warning  btn-block" required="required">Gerar Quiz</button>
                </div>    
            </div>

        </div>
    </form> <!--/.row-->

    @include('partials.messages')




    @php
    //dd($perguntas);
    @endphp
    @if(isset($perguntas))
    @if($perguntas->count() > 0)
<!--    <div class="alert alert-success" role="alert">
        <strong>{{strtoupper(old('name'))}}</strong> , responda {{$perguntas->count()}} questão(ões) abaixo e receba o resultado em seu email!
    </div>-->

    <div class="container">
        <!--        <div class="row"><br>
                    <span class="image-position"><a href="https://www.facebook.com/meuix/?ref=settings" target="_blank">
                           <img src="https://lh4.googleusercontent.com/fLEIj3iQb7O1FhjOpLFbJtHmsMlLGmLynSWUvAP70qF0HLEBty-FANvwweg7Sv2XqSpzOKNI=w1366-h638"></a>
                        </span>
                </div>-->
        <div class="row"><br><br>
            <div class="col-sm-12">
                <div class="loader">
                    <div class="col-xs-3 col-xs-offset-5">
                        <div id="loadbar" style="display: none;">
                            <img src="https://8finatics.s3.amazonaws.com/static/reload_emi.gif" alt="Loading" class="center-block loanParamsLoader" style="">
                        </div>
                    </div>

                    <div id="quiz">
                        <div class="question h4">
                            <div class="text-center" id="qid"> Questão 1 de {{$perguntas->count()}} </div>
                                <span id="question"> {{$perguntas[0]->pergunta}}</span>
                            
                        </div>
                        <ul>

                            @forelse($perguntas[0]->respostas as $r)
                            <li id="{{$loop->iteration}}-li">
                                <input type="radio" id="{{$loop->iteration}}-option" name="selector" value="{{$loop->iteration}}">
                                <label for="{{$loop->iteration}}-option" class="element-animation">{{$r->resposta}}</label>
                                <div class="check"></div>
                            </li>
                            @php
                            if($loop->last){
                            $count = $loop->iteration;
                            }
                            @endphp

                            @empty
                            @endforelse
                            @php
                            //dd($count);
                            @endphp
                            @if($count<5)
                            @for($i=$count+1; $i<=5; $i++)
                            <li id="{{$i}}-li" style="display: none;">
                                <input type="radio" id="{{$i}}-option" name="selector" value="{{$i}}">
                                <label for="{{$i}}-option" class="element-animation"></label>
                                <div class="check"></div>
                            </li>
                            @endfor
                            @endif




                            <!--                            <li>
                                                            <input type="radio" id="s-option" name="selector" value="2">
                                                            <label for="s-option" class="element-animation">comment //</label>
                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>
                            
                                                        <li>
                                                            <input type="radio" id="t-option" name="selector" value="3">
                                                            <label for="t-option" class="element-animation">comment [ ]</label>
                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>-->
                        </ul>
                    </div>
                </div>
                <div class="text-muted">
                    <span id="answer"></span>
                </div>

            </div>
        </div>
        <div class="row" >
            <div class="col-sm-12 col-md-12 col-lg-12 " >
                <div id="result-of-question" class="pulse animated" style="display: none;">
                    <span id="totalCorrect-title" class="pull-right">Total de acertos: </span>
                    <span id="totalCorrect" class="pull-right"></span>
                    <table id="tb-result" class="table table-hover   table-striped" >
                        <thead>
                            <tr>
                                <th>Questão</th>
                                <th>Sua resposta</th>
                                <th>Resultado</th>
                            </tr>
                        </thead>
                        <tbody id="quizResult"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @else
    <div class="alert alert-danger" role="alert">
        <strong>{{strtoupper(old('name'))}}</strong>, infelizmente não existe perguntas para esse Nível e Categoria. Por favor, tente outras opções!
    </div>
    @endif
    @endif
    

</section>




@endsection

