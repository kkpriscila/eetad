@extends('layouts.website')
@section('titulo', 'EETAD Núcleo 0325')
@section('subtitle')
@if($config->nmescola != null && $config->nmescola != "")
{{$config->nmescola}}
@endif
@endsection

@section('styles') 
 <link href="css/interno.css" rel="stylesheet">       
@endsection

@section('conteudo') 
<!--    About Section 
-->  <section class="page-section-header margim-top-20" style="padding-top: 100px" >
  
        <div class="row justify-content-center">
             <h2 class=" mt-0">Portifólio de Fotos</h2>
           
        </div>
  
  </section>



<!-- Portfolio Section -->
<section id="portfolio" class="">
  
    <div class="container-fluid p-0  ">
       
        <div class="row no-gutters">
            
            @forelse($galeria as $foto) 
            <div class="col-lg-4 col-sm-6">
                
                <a class="portfolio-box" href="{{$foto->imagem}}"  title="{{$foto->nmfoto}}">
             
                    <img class="img-fluid" src="{{$foto->imagemp}}" alt="{{$foto->nmfoto}}">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            <!--Category-->
                        </div>
                        <div class="project-name">
                          
                            {{$foto->nmfoto}}
                          
                        </div>
                    </div>
                </a>
            </div>
           
            @empty
<!--            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/3.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/3.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
                    <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
                    <div class="portfolio-box-caption p-3">
                        <div class="project-category text-white-50">
                            Category
                        </div>
                        <div class="project-name">
                            Project Name
                        </div>
                    </div>
                </a>
            </div>-->
            @endforelse
            
         
    </div>
         <infinite-scroll src="{{route('fotos')}}?page=" pagination-box=".paginated-items">
          
      </infinite-scroll>
      <!--{-- $galeria->links() --}-->
         
        </div>
   
    <!--<a href="{{route('monitores')}}" alt="Mais Fotos" title="Mais Fotos"><div class="fotos" >+</div></a>-->
</section>


 

@endsection

