@extends('layouts.website')
@section('titulo', 'EETAD Núcleo 0325')
@section('subtitle')
@if($config->nmescola != null && $config->nmescola != "")
{{$config->nmescola}}
@endif
@endsection

@section('conteudo') 

<!-- Masthead -->
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-10 align-self-end">
                <h1 class="text-uppercase text-white font-weight-bold">A Escola de Ensino Teológico da Assembleia de Deus no Vale do Aço</h1>
                <hr class="divider my-4">
            </div>
            <div class="col-lg-10 align-self-baseline">
                <p class="text-white-75 font-weight-light mb-5">A EETAD é, reconhecidamente, uma escola que prima pelo genuíno ensino teológico, demonstrado através de seu Curso Básico, Curso Médio e Faculdade de Teologia.</p>
                <a class="btn btn-warning  btn-xl js-scroll-trigger" target="_blank" href="{{route('aluno.register')}}">
                     <i class="fas fa-address-card text-black"></i> <span class="text-primary">&nbsp;&nbsp;&nbsp;Matricule-se já!</span> 
                     <br> <small class="text-bold">Formulário de pré matrícula.</small>
                </a>
                <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Conheça Mais <br>nossa Escola</a>
                <a class="btn btn-warning  btn-xl js-scroll-trigger" href="{{route('quizbiblico')}}">
                    <i class="fas fa-question text-primary"></i> <i class="fas fa-question "></i><i class="fas fa-question text-primary"></i>  <span class="text-primary">Quiz Bíblico</span> <i class="fas fa-question text-primary"></i><i class="fas fa-question "></i><i class="fas fa-question text-primary"></i>
                    <br> Teste seus conhecimento!
                </a>
            </div>
        </div>
    </div>
</header>

<!-- About Section -->
<section class="page-section-sm bg-primary" id="about">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-12 col-md-12 text-center">
              <h3 class="text-white mt-0">
                    @if($config->nmescola != null && $config->nmescola != "")
                    {{$config->nmescola}}    
                    @else
                    Sobre
                    @endif
                </h3>
              <hr class="divider light my-4">
            </div>
        </div>
         <div class="row ">
            <!--<div class="col-lg-6 col-md-6 text-center">-->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
              
              
                @if(isset($config) && $config->count() > 0)
                @if($config->txtsobre != null && $config->txtsobre != "")
                <p class="text-white-75 ">{!! $config->txtsobre !!}</p>
                @endif
                @endif
              
            </div>
               <!--Start slider news-->
                <div class="col-6x col-md-6 col-sm-12 col-xs-12 text-center">
<!--                     <h3 class="text-white mt-0">
                   Postagens
                </h3>
                <hr class="divider light my-4">-->
<p></p>
                    <div id="featured" class="carousel slide carousel  " data-ride="carousel">
                        <!--dots navigate-->
<!--                        <ol class="carousel-indicators top-indicator">
                            <li data-target="#featured" data-slide-to="0" class="active"></li>
                            <li data-target="#featured" data-slide-to="1"></li>
                            <li data-target="#featured" data-slide-to="2"></li>
                            <li data-target="#featured" data-slide-to="3"></li>
                        </ol>-->
                        
                        <!--carousel inner-->
                        <div class="carousel-inner">
                            <!--Item slider-->
                            <div class="carousel-item active">
                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                    <div class="position-relative">
                                        <!--thumbnail img-->
                                        <div class="ratio_left-cover-1 image-wrapper">
                                            <!--<a href="#">-->
                                                <img class="img-fluid w-100"
                                                     src="{{ asset('img/diretoria2.jpg')}}"
                                                     alt="Diretoria EETAD Nacional">
                                            <!--</a>-->
                                        </div>
                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                            <!--title-->
                                            <!--<a href="#">-->
                                                <h2 class="h3 post-title text-white my-1">Pastor Jùnior Calais é o 3º secretario na Diretoria da EEATD Nacional</h2>
                                            <!--</a>-->
                                            <!-- meta title -->
<!--                                            <div class="news-meta">
                                                <span class="news-author">by <a class="text-white font-weight-bold" href="../category/author.html">Jennifer</a></span>
                                                <span class="news-date">Oct 22, 2019</span>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
<!--                            Item slider
                            <div class="carousel-item">
                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                    <div class="position-relative">
                                        thumbnail img
                                        <div class="ratio_left-cover-1 image-wrapper">
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <img class="img-fluid w-100"
                                                     src="https://bootstrap.news/source/img2.jpg"
                                                     alt="Bootstrap news theme">
                                            </a>
                                        </div>
                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                            title
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <h2 class="h3 post-title text-white my-1">Walmart shares up 10% on online sales lift</h2>
                                            </a>
                                             meta title 
                                            <div class="news-meta">
                                                <span class="news-author">by <a class="text-white font-weight-bold" href="../category/author.html">Jennifer</a></span>
                                                <span class="news-date">Oct 22, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            Item slider
                            <div class="carousel-item">
                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                    <div class="position-relative">
                                        thumbnail img
                                        <div class="ratio_left-cover-1 image-wrapper">
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <img class="img-fluid w-100"
                                                     src="https://bootstrap.news/source/img3.jpg"
                                                     alt="Bootstrap blog template">
                                            </a>
                                        </div>
                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                            title
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <h2 class="h3 post-title text-white my-1">Bank chief warns on Brexit staff moves to other company</h2>
                                            </a>
                                             meta title 
                                            <div class="news-meta">
                                                <span class="news-author">by <a class="text-white font-weight-bold" href="../category/author.html">Jennifer</a></span>
                                                <span class="news-date">Oct 22, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            Item slider
                            <div class="carousel-item">
                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                    <div class="position-relative">
                                        thumbnail img
                                        <div class="ratio_left-cover-1 image-wrapper">
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <img class="img-fluid w-100"
                                                     src="https://bootstrap.news/source/img4.jpg"
                                                     alt="Bootstrap portal template">
                                            </a>
                                        </div>
                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                            title
                                            <a href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">
                                                <h2 class="h3 post-title text-white my-1">The world's first floating farm making waves in Rotterdam</h2>
                                            </a>
                                             meta title 
                                            <div class="news-meta">
                                                <span class="news-author">by <a class="text-white font-weight-bold" href="../category/author.html">Jennifer</a></span>
                                                <span class="news-date">Oct 22, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            end item slider-->
                        </div>
                        <!--end carousel inner-->
                    </div>
                    
                    <!--navigation-->
<!--                    <a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#featured" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>-->
                </div>
                <!--End slider news-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 text-center">
            <br class="visible-xs visible-sm">
              <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Conheça nossa Equipe..</a>
        </div>
    </div>
</section>

<!-- Services Section -->
<section class="page-section-sm" id="services">
    <div class="container">
        <h2 class="text-center mt-0">Nossa Diretoria <!--e Titulares--></h2>
        <!--<p class="text-center mb-4">Conheça aqui alguns deles.</p>-->

        <hr class="divider my-4">
        <div id="meet-the-team" >
            <div class="row">
                @php
                if(isset($diretoria) && $diretoria->count() > 0){
                $col = 12/ count($diretoria);
                //dd($col );

                }
                @endphp

                @forelse($diretoria as $dir) 
                <div class="col-md-{{$col}} col-xs-12">
                    <div class="center text-center">
                        <p><img class="img-responsive img-thumbnail img-circle" src="{{$dir->foto}}" alt="" ></p>
                        <h5>{{$dir->nome}}<small class="designation muted">{{$dir->titulo}}</small></h5>
                        <p class="text-center">{{$dir->curriculo}}</p>
                        <!--<a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                    </div>
                    <br class="visible-xs">
                </div>
                @empty
                <!--                <div class="col-md-3 col-xs-12">
                                    <div class="center">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                                        <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                        <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                                    </div>
                                </div>
                
                                <div class="col-md-3 col-xs-6">
                                    <div class="center">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                                        <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                        <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                                    </div>
                                </div>        
                                <div class="col-md-3 col-xs-6">
                                    <div class="center">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                                        <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                        <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                                    </div>
                                </div>        
                                <div class="col-md-3 col-xs-6">
                                    <div class="center">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                                        <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                        <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                                    </div>
                                </div>-->
                @endforelse
            </div>
            <hr>
            <div class="row">
                @php
                if(isset($titular) && $titular->count() > 0){
                $col = 12/ count($titular);
                //dd($col );

                }
                @endphp

                @forelse($titular as $dir) 

                <div class="col-md-{{$col}} col-xs-12">
                    <div class="center text-center">
                        <p><img class="img-responsive img-thumbnail img-circle" src="{{$dir->foto}}" alt="" ></p>
                        <h5>{{$dir->nome}}<small class="designation muted">{{$dir->titulo}}</small></h5>
                        <p class="text-center">{{$dir->curriculo}}</p>
                        <!--<a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                    </div>
                    <br class="visible-xs">
                </div>
                @empty
                @endforelse
            </div>
        </div>

        <p class="text-right mb-4"><a  href="{{route('monitores')}}" >Conheça os Monitores..</a></p>
        <!--/#meet-the-team-->
        <!--      <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                  <div class="mt-5">
                    <i class="fas fa-4x fa-gem text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Sturdy Themes</h3>
                    <p class="text-muted mb-0">Our themes are updated regularly to keep them bug free!</p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                  <div class="mt-5">
                    <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Up to Date</h3>
                    <p class="text-muted mb-0">All dependencies are kept current to keep things fresh.</p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                  <div class="mt-5">
                    <i class="fas fa-4x fa-globe text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Ready to Publish</h3>
                    <p class="text-muted mb-0">You can use this design as is, or you can make changes!</p>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                  <div class="mt-5">
                    <i class="fas fa-4x fa-heart text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Made with Love</h3>
                    <p class="text-muted mb-0">Is it really open source if it's not made with love?</p>
                  </div>
                </div>
              </div>-->
    </div>
</section>

<!-- Portfolio Section -->
<section id="portfolio" class="">

    <div class="container-fluid p-0  ">

        <div class="row no-gutters">

            @forelse($galeria as $foto) 
            <div class="col-lg-4 col-sm-6">

                <a class="portfolio-box" href="{{$foto->imagem}}" title="{{$foto->nmfoto}}">

                    <img class="img-fluid" src="{{$foto->imagemp}}" alt=" {{$foto->nmfoto}}">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            <!--Category-->
                        </div>
                        <div class="project-name">

                            {{$foto->nmfoto}}

                        </div>
                    </div>
                </a>
            </div>

            @empty
            <!--            <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/3.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/3.jpg" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
                                <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
                                <div class="portfolio-box-caption p-3">
                                    <div class="project-category text-white-50">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </a>
                        </div>-->
            @endforelse


        </div>

    </div>

    <!--<a href="{{route('monitores')}}" alt="Mais Fotos" title="Mais Fotos"><div class="fotos" >+</div></a>-->
</section>
<div class="bg-primary text-right page-link text-white"> <a href="{{route('fotos')}}" class=" link-fotos"><i class="fas fa-plus"></i> Confira todas nossas fotos ..</a></div>

<!-- Call to Action Section -->
<section class="page-section bg-dark text-white">
    <div class="container text-center">
        <h2 class="mb-4">Conheça também o site da EETAD Nacional!</h2>
        <a class="btn btn-light btn-xl" target="_blank" href="https://www.eetad.com.br/">Entre Aqui!</a>
    </div>
</section>

<!-- Contact Section -->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="mt-0">Contate-nos!</h2>
                <hr class="divider my-4">
                @if(isset($config) && $config->count() > 0)
                @if($config->endereco != null && $config->endereco != "")
                <p class="text-muted mb-5">{{$config->endereco}}</p>
                @endif
                @endif
            </div>
        </div>
        <div class="row">
            @if(isset($config) && $config->count() > 0)
            @if($config->telefone != null && $config->telefone != "")

            <div class="col-lg-3 ml-auto text-center mb-5 mb-lg-0">
                <a href="tel:+55{{substr($config->telefone, 0, 11)}}" target="_blank">
                    <i class="fas fa-phone fa-3x mb-3 text-muted"></i>

                    <div>{{"(".substr($config->telefone, 0, 2).") ".substr($config->telefone, 2, 4)."-".substr($config->telefone,6) }}</div>
                </a>
            </div>

            @endif

            @if($config->whatsapp!= null && $config->whatsapp != "")

            <div class="col-lg-3 ml-auto text-center mb-5 mb-lg-0">
                <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$config->whatsapp}}" target="_blank">

                    <i class="fab fa-whatsapp-square fa-3x mb-3 text-muted"></i>

                    <div>{{"(".substr($config->whatsapp, 0, 2).") ".substr($config->whatsapp, 2, 4)."-".substr($config->whatsapp,6) }}</div>
                </a>
            </div>

            @endif
            @if($config->facebook!= null && $config->facebook != "")

            <div class="col-lg-3 ml-auto text-center mb-5 mb-lg-0">
                <a href="http://{{$config->facebook}}" target="_blank" >

                    <i class="fab fa-facebook-square fa-3x mb-3 text-muted"></i>

                    <div>{{$config->facebook}}</div>
                </a>
            </div>

            @endif
            @if($config->email != null && $config->email != "")
            <div class="col-lg-3 ml-auto text-center mb-5 mb-lg-0">

                <!-- Make sure to change the email address in anchor text AND the link below! -->


                <!--<a class="d-block" href="mailto:{{$config->email}}">{{$config->email}}</a>-->
                <a  href="{{route("contato")}}" >
                    <i class="fas fa-envelope fa-3x mb-3 text-muted "></i>
                    <div>{{$config->email}}</div></a>

            </div>
            @endif
            @endif
        </div>
    </div>
</section>

@endsection

