@extends('layouts.website')
@section('titulo', 'EETAD Núcleo 0325')
@section('subtitle')
@if($config->nmescola != null && $config->nmescola != "")
{{$config->nmescola}}
@endif
@endsection

@section('styles') 
 <link href="css/interno.css" rel="stylesheet">       
@endsection

@section('conteudo') 
   <section class="container page-section " id="contact">
   
     
      @if (Session::has('success'))
	<div class="col-md-12 col-lg-12 alert alert-success" role="alert">
		<strong> {{ Session::get('success') }}</strong>
	</div>
@elseif (Session::has('error'))
	<div class=" col-md-12 col-lg-12 alert alert-danger" role="alert">
		<strong>Erro:</strong> {{ Session::get('error') }}
	</div>
@endif
   
       
             <h2 class="text-center mt-0">       
                 Fale Cosnosco <br>
    
           </h2>
@if(isset($config) && $config->count() > 0)
          @if($config->txtcontato != null && $config->txtcontato != "")
          <p class="text-center text-muted mb-5">{{$config->txtcontato}}</p>
          @endif
          @endif
            
                <!--<div class="status alert alert-success" style="display: none"></div>-->
                <!--id="main-contact-form" class="contact-form"-->
                <form  class="row " name="contact-form" action="{{ route('contato.send') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-lg-6 col-md-6 col-sm-5 ">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Telefone</label>
                            <input type="text" class="form-control tel" maxlength="15">
                        </div>
<!--                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control">
                        </div>                        -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-5">
                        <div class="form-group">
                            <label>Assunto *</label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Menssagem *</label>
                            <textarea name="menssagem" id="message" required="required" class="form-control" rows="5"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Enviar</button>
                        </div>
                    </div>
                </form> <!--/.row-->
     
    </section><!--/#contact-page-->
      

@endsection

