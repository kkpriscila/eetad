@extends('layouts.website')
@section('titulo', 'EETAD Núcleo 0325')
@section('subtitle')
@if($config->nmescola != null && $config->nmescola != "")
{{$config->nmescola}}
@endif
@endsection

@section('styles') 
 <link href="css/interno.css" rel="stylesheet">       
@endsection

@section('conteudo') 
   <!-- About Section -->
<!--  <section class="page-section-header text-grey-darker" id="about">
   
  </section>-->


  <!-- Services Section -->
  <section class=" page-section " id="services">
    <div class="container">
      <h2 class="text-center mt-0">Nossos Monitores</h2>
      <!--<p class="text-center mb-4">Conheça aqui alguns deles.</p>-->
        
      <hr class="divider my-4">
       <div id="meet-the-team" class="row">
            @forelse($monitores as $monitor) 
            <div class="col-md-3 col-xs-6">
                <div class="center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="{{url($monitor->foto)}}" alt="" ></p>
                    <h5>{{$monitor->nome}}<small class="designation muted">{{$monitor->titulo}}</small></h5>
                    <p class="text-justify curriculo">{{$monitor->curriculo}}</p>
                    <!--<a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
            </div>
            @empty
<!--            <div class="col-md-3 col-xs-12">
                <div class="center text-center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                    <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>

            <div class="col-md-3 col-xs-6">
                <div class="center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                    <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>        
            <div class="col-md-3 col-xs-6">
                <div class="center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                    <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>        
            <div class="col-md-3 col-xs-6">
                <div class="center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="img/team-member.jpg" alt="" ></p>
                    <h5>David J. Robbins<small class="designation muted">Senior Vice President</small></h5>
                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>-->
            @endforelse
            
        </div>
      <!-- o componente vue, o local que ele fica é importante para definir
            onde o será detectado a rolagem da página para disparar o carregamento
            de novos items da paginação -->
      <infinite-scroll src="{{route('monitores')}}?page=" pagination-box=".paginated-items">
          
      </infinite-scroll>
      {{ $monitores->links() }}
     
    </div>
  </section>



 

@endsection

