<h1>Formulário de Matrícula - Dados Editado</h1>
<p>EDIÇÃO de Matrícula para o subnúcleo <b>{{ $subnucleo }}</b> , responsável {{ $resp_subnucleo }}, email {{$email_subnucleo}}:</p>

<ul>
    <h3>Dados do Curso</h3>
    <hr>
    <li><b>Ciclo:</b> {{ $ciclo }}</li>
    <li><b>Cidade em que fará eetad:</b> {{ $cidade_eetad }}/{{ $estado }}</li>
    <li><b>Núcleo:</b> {{ $nucleo }}</li>
    
    <h3>Dados do Aluno</h3>
    <hr>
    <li><b>Nome:</b> {{ $nome }}</li>
    <li><b>Endereço:</b> {{$end}},{{ $num }}, Bairro {{ $bairro }} - {{ $cidade }}/{{$uf}}</li>
   
    <li><b>Cep:</b> {{ $cep }}</li>
    <li><b>Telefone:</b> {{ $tel }}</li>
    <li><b>Dt. Nascimento:</b> {{ date("d/m/Y", strtotime($dtnasc))  }}</li>
    <li><b>Local de Nascimento:</b> {{ $cidnasc }}/{{ $ufnasc }}</li>
    <li><b>Nacionalidade:</b> {{ $nacionalidade }}</li>
    <li><b>Estado Civil:</b> {{ $estcivil }}</li>
    <li><b>Email:</b> {{ $email }}</li>
    <li><b>Identidade:</b> {{ $identidade }}</li>
    <li><b>Cpf:</b> {{ $cpf }}</li>
    <li><b>Sexo:</b> {{ $sexo }}</li>
    <li><b>Igreja:</b> {{ $igreja }}</li>
    <li><b>Começar em qual livro?</b> {{ $livro }}</li>
    <li><b>Cargo:</b> {{ $cargo }} - Qual: {{$qualcargo}}</li>
    <li><b>Escolaridade:</b> {{ $escolaridade }}</li>
    <li><b>Profissão:</b> {{ $profissao }}</li>
    <li><b>Formação Teológica?:</b> {{ $formacaoteologica }} - Escola: {{$escola}}</li>
    <li><b>Dt. Pagamento Taxa de Matrícila:</b> {{ date("d/m/Y", strtotime($dttaxamatricula))  }}</li>
    <li><b>Motivo não pagamento Matrícula:</b> {{ $motivotxmatricula }}</li>
  
       

</ul>

<br>

<small>Email automático enviado através de preenchimento do formulário de matrícula pelo SubNucleo no site 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}">http://{{$_SERVER['HTTP_HOST']}}</a></b>.</small>