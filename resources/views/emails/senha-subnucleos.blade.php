<h1>Senha para Portal do site da EETAD - Núcleo 0325</h1>
<p>
    <b>Olá Sr(a) {{ $responsável }},</b> responsável do subnúcleo {{ $nome }}
    <br>para você enviar matrículas através de nosso site acesse 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}/login">http://{{$_SERVER['HTTP_HOST']}}/login</a></b>.
    <br><br>

</p>

<b>Dados para acesso:</b>  
<ul>
   
    <li><b>Endereço:</b> <a href="http://{{$_SERVER['HTTP_HOST']}}/login">http://{{$_SERVER['HTTP_HOST']}}/login</a></li>
    <li><b>Usuário/Email:</b> {{ $email }}</li>
    <li><b>Senha:</b> {{ $senha }}</li>
</ul>
<br>
Obs.: Caso tenha alguma dúvida entre em contato com a Secretaria do Núcleo.
<br>

<small>Não responder. Email automático enviado através de preenchimento do formulário de contato do site 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}">http://{{$_SERVER['HTTP_HOST']}}</a></b>.</small>