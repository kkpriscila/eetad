<h1>Fale Conosco via Website</h1>
<p>Novo email preenchido via formulário de contato do Fale Conosco</p>

<p>
    <b>{{ $name }}</b> escreveu uma mensagem para <b>{{ $emaildestino }}</b>: 
    <br><b>Assunto:</b> {{$subject}}<br>

</p>
<pre>
         {{ $menssagem }}
</pre>

<br>
<b>Dados do Contato:</b>  
<ul>
    <li><b>Nome:</b> {{ $name }}</li>
    <li><b>Email:</b> {{ $email }}</li>
    <li><b>Telefone:</b> {{ $number }}</li>
</ul>
<br>

<small>Não responder. Email automático enviado através de preenchimento do formulário de contato do site 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}">http://{{$_SERVER['HTTP_HOST']}}</a></b>
   .</small>