<h1>Senha para envio de matrículas de alunos pelo site da EETAD - Núcleo 0325</h1>
<p>
    <b>Olá Sr(a){{ $name }},
    <br>para você acessar o Portal Administrativo do nosso site acesse 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}/admin">http://{{$_SERVER['HTTP_HOST']}}/admin</a></b>, 
    ou clique no link Admin no rodapé do site.
    <br><br>

</p>

<b>Dados para acesso:</b>  
<ul>
   
    <li><b>Endereço:</b> <a href="http://{{$_SERVER['HTTP_HOST']}}/admin">http://{{$_SERVER['HTTP_HOST']}}/admin</a></li>
    <li><b>Usuário/Email:</b> {{ $email }}</li>
    <li><b>Senha:</b> {{ $senha }}</li>
</ul>
<br>
Obs.: Caso tenha alguma dúvida entre em contato com a Secretaria do Núcleo.
<br>

<small>Não responder. Email automático enviado através de preenchimento do formulário de contato do site 
    <b><a href="http://{{$_SERVER['HTTP_HOST']}}">http://{{$_SERVER['HTTP_HOST']}}</a></b>.</small>