<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
            <style>

                #logo{
                    width: 100px;
                }
                #tb-result{
                    border: 1px solid #ffc107;

                }
                #result-of-question th{
                    /*text-align: center;*/
                    background: #ffc107;
                    color: #fff;
                    /*padding: 18px;*/
                    /*font-size: 18px;*/
                    /*border: none;*/
                }
                #result-of-question  td{
                    /*  text-align: center;
                        color: #222;
                        background-color: #fff;
                        padding: 18px;
                        font-size: 15px;
                        font-weight: 600;
                        border: 1px solid #ffc107;*/
                }

                #totalCorrect-title{
                    color: #333;
                    padding: 5px 20px;
                    /*    background: #ffc107;
                        padding: 5px 20px;
                        border-radius: 1px;
                        font-stretch: expanded;*/
                    font-size: 22px;
                    font-weight: bold;
                    /*    border-top-right-radius: 25px;
                        border-top-left-radius: 25px;  */
                }
                #totalCorrect{
                    color: #FFF;
                    background: #ffc107;
                    padding: 5px 20px;
                    border-radius: 1px;
                    font-stretch: expanded;
                    font-size: 32px;
                    font-weight: bold;
                    border-top-right-radius: 25px;
                    border-top-left-radius: 25px;  
                } 
                .text-danger{
                    color:#e3342f!important
                }
                .text-success{
                    color:#38c172!important
                }
                @media only screen and (max-width: 650px) {
                    #logo{
                        width: 60px;
                    }
                }
                
            </style>

    </head>
    <body>

        
        <h2>
         @if($logo != null && $logo != "")
<a href="{{ env('APP_URL') }}">
        <img src="http://eetad0325.com.br/{{($logo)}}" alt="EETAD" id="logo"/>
        @endif    
        @if( $nmescola != null &&  $nmescola != "")
        - <small>{{ $nmescola}}</small>
        @endif
</a>
        </h2>
        
        <h1><b>{{$name}}</b> , parabéns pelo seu Teste de Conhecimento Bíblico</h1>
        Venha conhecer nossos sub-núcleos em todo Vale do Aço.
            <br/>
                 Você foi contemplado com uma aula inaugural em qualquer um de nossos sub-nucleo. 
                <br/>Entre em contato com nossa secretaria e agende pelo telefone 
            @if($telefone_escola != null && $telefone_escola != "")
                <a href="tel:+55{{substr($telefone_escola, 0, 11)}}" target="_blank">
                    <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                    {{"(".substr($telefone_escola, 0, 2).") ".substr($telefone_escola, 2, 4)."-".substr($telefone_escola,6) }}
                </a>
            @endif
            ou no WhatsApp
             @if($whatsapp!= null && $whatsapp != "")
                <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$whatsapp}}" target="_blank">
                    <i class="fab fa-whatsapp-square fa-3x mb-3 text-muted"></i>
                    {{"(".substr($whatsapp, 0, 2).") ".substr($whatsapp, 2, 4)."-".substr($whatsapp,6) }}
                </a>          
            @endif
                

                    <h3>Resultado do Quiz <small>Nível: {{$nivel}} - Categoria: {{$categoria}}</small></h3>
                    <hr>
                        <div class="row" >
                            <div class="col-sm-12 col-md-12 col-lg-12 " >
                                <div id="result-of-question" class="pulse animated" >
                                    <span id="totalCorrect-title" class="pull-right">Total de acertos:</span>
                                    <span id="totalCorrect" class="pull-right">{{$totalCorrect}}</span>
                                    <table id="tb-result" width="100%" >
                                        <thead>
                                            <tr>
                                                <th>Questão</th>
                                                <th>Sua resposta</th>
                                                <th>Resultado</th>
                                            </tr>
                                        </thead>
                                        <tbody id="quizResult">
                                            {!!$toAppend!!}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <br>

                            <small>Email enviado através de preenchimento do Quiz Bíblico no site 
                                <b><a href="{{ env('APP_URL') }}">{{ env('APP_URL') }}</a></b>.</small>


                            </body>
                            </html>