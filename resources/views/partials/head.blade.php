<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Site da EETAD Núcleo 0325 - Coronel Fabriciano e Ipatinga">
  <meta name="author" content="">
  <meta property="og:site_name" content="EETAD 0325"/>
  <meta property="og:title" content="@yield('titulo')" />
  <meta property="og:image" content="{{ asset('img/logo.png') }}" />
  <meta property="og:image:width" content="244">
  <meta property="og:image:height" content="200">
  
  <meta property="og:type" content="website" />

  <meta property="og:url" content="http://www.eetad0325.com.br/" />
  <meta property="og:description" content="Site da EETAD Núcleo 0325 - Coronel Fabriciano e Ipatinga">
  
        <title>@yield('titulo')</title>

       
        <link rel="shortcut icon" href="img/eetad.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/eetad_128x128.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/eetad_128x128.ico">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="eetad_64x64.ico">
        <link rel="apple-touch-icon-precomposed" href="eetad_64x64.ico">
@yield('metafacebook')


@include('partials.styles')
