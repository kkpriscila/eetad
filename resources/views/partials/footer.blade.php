<!-- Footer -->
  <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2020 - EETAD    
          @if($config->nmescola != null && $config->nmescola != "")
            {{$config->nmescola}}    
            @endif
          &nbsp;&nbsp;-&nbsp;&nbsp;
          <a href="{{ url('/admin') }}" target="_blank"><i class="fa fa-lock"></i> Admin</a>
          
      </div> 
      
      
    </div>
  </footer>