 <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
       @if(isset($config) && $config->count() > 0)
       <a href="{{route('index')}}" class="links" >
         @if($config->logo != null && $config->logo != "")
         
          <img src="{{asset($config->logo)}}" alt="Logo EETAD" id="logo"/>
         @endif
         @if($config->nmescola != null && $config->nmescola != "")
         
         <span class="titulo"  >{{$config->nmescola}}</span>
         
         @endif
         </a>
      @endif
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>&nbsp;Menu
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{route('index')}}#about">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{route('index')}}#services">Diretoria/Monitores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{route('index')}}#portfolio">Fotos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{route('quizbiblico')}}">Quiz Bíblico</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{route('index')}}#contact">Contato</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('aluno.index')}}">Aluno</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>