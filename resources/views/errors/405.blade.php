@extends('errors.layout')

@php
  $error_number = 405;
@endphp

@section('title')
  Método não permitido.
@endsection

@section('description')
  @php
    $default_error_message = "Por favor <a href='javascript:history.back()''>volte</a> ou retorne a <a href='".url('')."'>página principal</a>.";
  @endphp
  {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
@endsection