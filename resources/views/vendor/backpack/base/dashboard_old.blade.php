@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>
        {{ trans('backpack::base.dashboard') }}
        <!--<small>{{ trans('backpack::base.first_page_you_see') }}</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">

            <div class="box-header with-border">
                <!--<div class="box-title">{{ trans('backpack::base.login_status') }}</div>-->
                @if(backpack_auth()->user()->tipo=="ADMINISTRADOR" or backpack_auth()->user()->tipo=="SECRETARIA")
                <div class="box-title">Como Utilizar o Painel Administrativo do Site</div>
                @else
                <div class="box-title">Seja Bem Vindo ao Portal Administrativo do Site da EETAD 0325</div>
                @endif
            </div>
            
            <!--<div class="box-body">{{ trans('backpack::base.logged_in') }}</div>-->
            <div class="box-body">
                @if(backpack_auth()->user()->tipo=="ADMINISTRADOR" or backpack_auth()->user()->tipo=="SECRETARIA")
                <ul >
                    <li> Para editar as configurações iniciais do Site abra no menu lateral esquerdo<strong>(Imagem 1 - opção 1)</strong> a opção 
                        <strong>Configurações</strong> e Clique no botão Editar<strong>(Imagem 1 - opção 2)</strong>. Nessa opção você precisa cadastrar
                        as informações da escola, bem como os textos que aparecem no site como Sobre e Contato. 
                        <br><br>
                        <figure>
                            <img class="img-responsive img-bordered" src="{{asset("img/tutorial_1.png")}}" alt="Figura 1"/>
                            <figcaption class="text-bold">Imagem 1</figcaption>
                        </figure>
                        <br>
                    </li>
                    <li>Você pode também alterar sua senha ou email de acesso através do espaço Minha conta no canto superior esquerdo<strong>(Imagem 2 - opção 1)</strong>.
                        <br>
                        Para cadastrar outros itens do site utilize uma das opções do Menu lateral esquerdo como no mostra na <strong>Imagem 2 - opção 2</strong>. Na parte central
                        aparecerá a listagem de todos os itens daquele tipo cadastrado com um botão superior para Adicionar <strong>(Imagem 2 - opção 3)</strong>,
                        e botões de Editar e Excluir a cada registro já cadastrado <strong>(Imagem 2 - opção 4)</strong>.

                        <br><br>
                        <figure>
                            <img class="img-responsive img-bordered" src="{{asset("img/tutorial_2.png")}}" alt="Figura 2"/>
                            <figcaption class="text-bold">Imagem 2</figcaption>
                        </figure>
                    </li>

                </ul>
                 @endif
            </div>

           

        </div>
    </div>
</div>
@endsection
