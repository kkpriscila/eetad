@php
$matriculas = App\Models\Matricula::getMatriculasPendente();
//dd($matriculas->count());
//dd(backpack_auth()->user()->tipo);
@endphp
<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fas fa-tachometer-alt"></i> <span> &nbsp;&nbsp;Início</span></a></li>
@if(backpack_auth()->user()->tipo=="ADMINISTRADOR")
<li><a href="{{backpack_url('configuracao') }}"><i class="fa fa-gears"></i> <span>Configurações</span></a></li>
<li><a href="{{backpack_url('user') }}"><i class="fa fa-users"></i> <span>Usuários</span></a></li>
@endif
@if(backpack_auth()->user()->tipo=="ADMINISTRADOR" or backpack_auth()->user()->tipo=="SECRETARIA")
<li><a href="{{backpack_url('monitores') }}"><i class="fas fa-user-tie"></i> <span> &nbsp;&nbsp;Diretoria/Monitores</span></a></li>
<li><a href="{{backpack_url('subnucleos') }}"><i class="fa fa-home"></i> <span>Sub-Núcleos</span></a></li>
<li><a href="{{backpack_url('fotos') }}"><i class="fa fa-image"></i> <span>Fotos</span></a></li>
<li><a href="{{backpack_url('aluno') }}"><i class="fa fa-user-circle"></i> <span>Alunos</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-address-book "></i><span>Matrícula 
            @if($matriculas->count()>0)
            &nbsp;
            <span class="badge badge-danger">{{$matriculas->count()}}</span>
            @endif
        </span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{backpack_url('matricula') }}"><i class="fa fa-address-card"></i> <span>Solicitações  
                    @if($matriculas->count()>0)
                    &nbsp;<span class="badge badge-danger">{{$matriculas->count()}}</span>
                    @endif
                </span></a></li>
        <li><a href="{{backpack_url('matricula-aprovada') }}"><i class="fa fa-check-circle"></i> <span>Efetivada</span></a></li>
        <li><a href="{{backpack_url('matricula-reprovada') }}"><i class="fa fa-exclamation-circle"></i> <span>Não Efetivada</span></a></li>
    </ul>
</li>
@endif
@if(backpack_auth()->user()->tipo=="ADMINISTRADOR" or backpack_auth()->user()->tipo=="SECRETARIA" or backpack_auth()->user()->tipo=="QUIZ REVISOR")
<li class="treeview">
    <a href="#"><i class="fa fa-tasks "></i><span>Quiz Bíblico</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{backpack_url('categoria') }}"><i class="fa fa-th-list"></i> <span>Categorias</span></a></li>
        <li><a href="{{backpack_url('pergunta') }}"><i class="fa fa-question-circle"></i> <span>Perguntas</span></a></li>
    </ul>
</li>
@endif
@if(backpack_auth()->user()->tipo=="QUIZ")
<li class="treeview">
    <a href="#"><i class="fa fa-tasks "></i><span>Quiz Bíblico</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{backpack_url('categoria') }}"><i class="fa fa-th-list"></i> <span>Categorias</span></a></li>
        <li><a href="{{backpack_url('pergunta_usuario') }}"><i class="fa fa-question-circle"></i> <span>Perguntas</span></a></li>
    </ul>
</li>
@endif
<!--<li><a href="{{ backpack_url('user') }}"><i class="fa fa-users"></i> <span>Usuários </span></a></li>-->
