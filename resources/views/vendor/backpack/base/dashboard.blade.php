@extends('backpack::layout')
@section('after_scripts')
<!-- Morris.js charts -->
<!--<script src="{{asset('vendor/adminlte/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('vendor/adminlte/bower_components/morris.js/morris.min.js')}}"></script>-->
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('vendor/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!--Resolve conflict in jQuery UI tooltip with Bootstrap tooltip--> 
<!--<script>
 $.widget.bridge('uibutton', $.ui.button);
</script>-->

<!-- Bootstrap WYSIHTML5 -->
<!--<script src="{{asset('vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>-->

<!-- jQuery Knob Chart -->
<!--<script src="{{asset('vendor/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>-->
<!-- daterangepicker -->
<!--<script src="{{asset('vendor/adminlte/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendor/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>-->
<!-- ChartJS -->
<script src="{{asset('vendor/adminlte/bower_components/chart.js/Chart.js')}}"></script>
<!-- Slimscroll -->
<!--<script src="{{asset('vendor/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>-->

<script type="text/javascript">
{{-- dd(json_encode($tpprop))  --}}
@if (backpack_auth() -> user() -> tipo == "ADMINISTRADOR" or backpack_auth() -> user() -> tipo == "SECRETARIA")
        var PieSexo = [
                @foreach ($alunos_sexo as $i)
                @if ($i -> sexo == 'Feminino')

        {value: {{ $i -> qtd }}, label: "{{ $i->sexo }}", color: '#f012be' },
                @endif
                @if ($i -> sexo == 'Masculino')
        {value: {{ $i -> qtd }}, label: "{{ $i->sexo }}", color: '#3c8dbc' },
                @endif

                @endforeach
                ];
var PieIdade = [
        @php
        // dd(($alunos_idade[0]->qtd));
        $menor20 = 0;
$de21a30 = 0;
$de31a40 = 0;
$de41a50 = 0;
$maior50 = 0;
foreach ($alunos_idade as $i){
if ($i -> idade <= 20)
        $menor20 += $i -> qtd;
elseif ($i -> idade > 20 and $i -> idade <= 30)
        $de21a30 += $i -> qtd;
elseif ($i -> idade > 30 and $i -> idade <= 40)
        $de31a40 += $i -> qtd;
elseif ($i -> idade > 40 and $i -> idade <= 50)
        $de41a50 += $i -> qtd;
elseif ($i -> idade >= 50)
        $maior50 += $i -> qtd;
}
@endphp
{value: {{ $menor20 }}, label: "Até 20 anos", color: '#00c0ef' },
{value: {{ $de21a30 }}, label: "De 21 a 30 anos", color: '#f39c12' },
{value: {{ $de31a40 }}, label: "De 31 a 40 anos", color: '#00a65a' },
{value: {{ $de41a50 }}, label: "De 41 a 50 anos", color: '#dd4b39' },
{value: {{ $maior50 }}, label: "Acima de 50 anos", color: '#3c8dbc' },
        ];
var barChartData = {
//             'Fevereiro', 'Março', 'Abril', 'Maio'
labels: [
        @foreach ($meses_matricula as $m)
        @switch ($m -> mes)
        @case('1')
        "JAN/{{$m->ano}}",
        @break
        @case('2')
        "FEV/{{$m->ano}}",
        @break
        @case('3')
        "MAR/{{$m->ano}}",
        @break
        @case('4')
        "ABR/{{$m->ano}}",
        @break
        @case('5')
        "MAI/{{$m->ano}}",
        @break
        @case('6')
        "JUN/{{$m->ano}}",
        @break
        @case('7')
        "JUL/{{$m->ano}}",
        @break
        @case('8')
        "AGO/{{$m->ano}}",
        @break
        @case('9')
        "SET/{{$m->ano}}",
        @break
        @case('10')
        "OUT/{{$m->ano}}",
        @break
        @case('11')
        "NOV/{{$m->ano}}",
        @break
        @case('12')
        "DEZ/{{$m->ano}}",
        @break
        @endswitch

        @endforeach
],
//             console.log("oi");
        datasets: [
        {
        label: 'Pré Matrículas',
                fillColor: '#3b8bba',
                data: [
                        @foreach ($meses_matricula as $m)
                        @php
                        $count = 0;
                @endphp
                        @foreach ($pre_matriculas as $qtd)
                        @if (($m -> mes == $qtd -> mes) && ($m -> ano == $qtd -> ano))
                {{$qtd -> qtd}},
                        @php
                        $count = 1;
                @endphp
                        @endif
                        @endforeach
                        @if ($count == 0)
                        '',
                        @endif
                        @endforeach
                ]
        },
        {
        label: 'Matrículas Efetivadas',
                fillColor: '#00ff30',
                data: [
                        @foreach ($meses_matricula as $m)
                        @php
                        $count = 0;
                @endphp
                        @foreach ($matriculas_aprov as $qtd)
                        @if (($m -> mes == $qtd -> mes) && ($m -> ano == $qtd -> ano))
                {{$qtd -> qtd}},
                        @php
                        $count = 1;
                @endphp
                        @endif
                        @endforeach
                        @if ($count == 0)
                        '',
                        @endif
                        @endforeach
                ]
        },
        ]
}

@endif
//    console.log("oi dashboard");

</script>    
<script src="{{asset('js/dashboard.js')}}"></script> 
@endsection

@section('header')
<!--<section class="content-header">
    <h1>
        {{-- trans('backpack::base.dashboard') --}}
        <small>{{ trans('backpack::base.first_page_you_see') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>-->
@endsection


@section('content')
@if(backpack_auth()->user()->tipo=="ADMINISTRADOR" or backpack_auth()->user()->tipo=="SECRETARIA")
<div class="box">
    <div class="box-header with-border">
        <!--<div class="box-title">{{ trans('backpack::base.login_status') }} </div>-->
        <div class="box-title">GESTÃO À VISTA </div>
    </div>

    <div class="box-body">
        {{--trans('backpack::base.logged_in') --}} 



        <!--GESTÃO A VISTA-->
        <!--                <div id="carousel-example-generic" class="carousel slide " data-ride="carousel" data-interval="7000">
        
                            <div class="carousel-inner">
                                <div class="item active">-->
        <div class="box">
            <div class="box-header with-border">
                <div class="box-title">Totalizadores</div>

            </div>
            <div class="box-body">


                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <a href="{{backpack_url('aluno') }}">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>
                                    @if($matriculados->count()>0)
                                    {{$matriculados[0]->qtd}}                                    
                                    @endif
                                        <!--<sup style="font-size: 20px">%</sup>-->
                                </h3>

                                <p>ALUNOS MATRICULADOS</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-user-circle"></i>
                            </div>
                            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                        </div></a>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <a href="{{backpack_url('subnucleos') }}">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>
                                    @if($subnucleos->count()>0)
                                    {{$subnucleos[0]->qtd}}                                    
                                    @endif
                                        <!--<sup style="font-size: 20px">%</sup>-->
                                </h3>

                                <p>SUB-NÚCLEOS</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                        </div>
                    </a>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <a href="{{backpack_url('monitores') }}">
                        <div class="small-box bg-blue-active">
                            <div class="inner">
                                <h3>
                                    @if($monitores->count()>0)
                                    {{$monitores[0]->qtd}}                                    
                                    @endif
                                        <!--<sup style="font-size: 20px">%</sup>-->
                                </h3>

                                <p>MONITORES</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-tie"></i>
                            </div>
                            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                        </div>
                    </a>
                </div>
                <!-- ./col -->


            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <div class="box-title">Gráficos</div>

            </div>
            <div class="box-body">
                <!-- BAR CHART -->
                <div class="row">
                    <div class="col-md-6 col-lg-6">

                        <p class="text-center">
                            <strong>Pré-Matrículas X Matrículas Efetivadas por mês (Útimos 6 meses)</strong>
                        </p>
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="chart">
                                    <canvas id="barChart" style="height:230px"></canvas>
                                </div>

                                <ul class="chart-legend ">


                                    <li><i class="fa fa-circle text-light-blue"></i> Pré Matrículas</li>
                                    <li><i class="fa fa-circle text-lime"></i> Efetivadas</li>

                                </ul>
                            </div><!-- /.col -->
                        </div>

                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="text-center">
                            <strong>Matriculados por Sexo</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chart-responsive">
                                    <canvas id="pieChart" height="200"></canvas>
                                </div>
                                <!-- ./chart-responsive -->

                                <ul class="chart-legend clearfix">

                                    <li><i class="fa fa-circle text-light-blue"></i> Masculino</li>
                                    <li><i class="fa fa-circle text-fuchsia"></i> Feminino</li>                                         

                                </ul>
                            </div><!-- /.col -->
                        </div>

                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="text-center">
                            <strong>Matriculados por Idade</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chart-responsive">
                                    <canvas id="pieChart2" height="200"></canvas>
                                </div>
                                <!-- ./chart-responsive -->

                                <ul class="chart-legend clearfix">
                                    <li><i class="fa fa-circle text-aqua"></i> Até 20 anos</li>
                                    <li><i class="fa fa-circle text-yellow"></i> De 21 a 30 anos</li>
                                    <li><i class="fa fa-circle text-green"></i> De 31 a 40 anos</li>
                                    <li><i class="fa fa-circle text-red"></i> De 41 a 50 anos</li>
                                    <li><i class="fa fa-circle text-light-blue"></i> Acima de 50 anos</li>

                                </ul>
                            </div><!-- /.col -->
                        </div>

                    </div>
                    <!-- /.box -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->
        </div>






        <!--                    </div>
        
        
                        </div>-->

        <!--                <a class="left carousel-control prev" href="#carousel-example-generic" data-slide="prev">
                            <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control next" href="#carousel-example-generic" data-slide="next">
                            <span class="fa fa-angle-right"></span>
                        </a>-->
        <!--</div>-->
        <!--FIM GESTÃO A VISTA-->





    </div>
</div>
@else
<div class="box">
    <div class="box-header with-border">
        <!--<div class="box-title">{{ trans('backpack::base.login_status') }} </div>-->
        <div class="box-title">PORTAL ADMINISTRATIVO DO SITE EETAD-0325</div>
    </div>

    <div class="box-body">
        Seja Bem Vindo ao Portal Administrativo do site.
<!--        <div class="box-header with-border">
            <div class="box-title">Totalizadores</div>

        </div>-->
        <div class="box-body">


            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <a href="{{backpack_url('categoria') }}">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                @if($categorias->count()>0)
                                {{$categorias[0]->qtd}}                                    
                                @endif
                                    <!--<sup style="font-size: 20px">%</sup>-->
                            </h3>

                            <p>CATEGORIAS QUIZ</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div></a>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <a href="{{backpack_url('pergunta') }}">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>
                                @if($perguntas->count()>0)
                                {{$perguntas[0]->qtd}}                                    
                                @endif
                                    <!--<sup style="font-size: 20px">%</sup>-->
                            </h3>

                            <p>PERGUNTAS QUIZ</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-question-circle"></i>
                        </div>
                        <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@endif
@endsection
