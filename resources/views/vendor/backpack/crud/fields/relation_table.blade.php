<?php
//dd($field);
if (!isset($field['value']) || $field['value'] == "") {
    echo "<div class='clearfix'></div><div class='alert alert-success' role='alert'>";

    echo "<strong>(*)</strong>Salve esse {$crud->entity_name} para adicionar  {$field['name']}.</span>";
    echo "</div>";
    return;
}

$max = isset($field['max']) && (int) $field['max'] > 0 ? $field['max'] : 999;
//dd(count($field['value']),$max);

//$crud->route = $field['route'];
?>
<div @include('crud::inc.field_wrapper_attributes') >
      <label>{!! $field['label'] !!}</label>

    <div class="array-container form-group ">
<!--<table id="crudTable" class="table table-striped table-hover display responsive nowrap" cellspacing="0">-->
        <table id="crudTableChild" class="table table-bordered table-striped m-b-0" >
            <thead>
                <tr>
                    @foreach( $field['columns'] as $column )
                    <th>
                        {{ __($column['label']) }}
                    </th>
                    @endforeach
                    <th data-orderable="false">{{ trans('backpack::crud.actions') }}</th>
                </tr>
            </thead>
            <tbody class="table-striped">


                @foreach( $field['value'] as $item)
                <tr class="array-row">
                    @foreach( $field['columns'] as $key => $column)
                    <td >

                        @if (isset($column['type']) && $column['type']=='date')
                            @if (!empty($item->{$column['name']}))
                            {{ Date::parse($item->{$column['name']})->format(($column['format'] ?? config('backpack.base.default_date_format'))) }}
                            @endif
                        @elseif (isset($column['type']) && $column['type']=='image')
                            <img class="img-responsive img-thumbnail img-vereador-perfil " src="{{ url('/').$item->{$column['name']} }}"   />
                        @elseif (isset($column['type']) && $column['type']=='select')
                        <span>
                            <?php
                            //dd($crud->entity_name);
                            $attributes = $crud->getModelAttributeFromRelation($item,$column['entity'] ,$column['attribute']);
                            if (count($attributes)) {
                                echo e(implode(', ', $attributes));
                            } else {
                                echo '-';
                            }
                            ?>
                        </span>
                        @else
                        {!! $item->{$column['name']} !!}
                        @endif


                    </td>
                    @endforeach
                    <td >
                        
                        @include('crud::buttons.update_child', ['entry' => $item])
                        <!--@include('crud::buttons.delete', ['entry' => $item])-->
                        @include('crud::buttons.delete_child', ['entry' => $item])
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        @php
        // Add relation entity
        $reflection = new ReflectionClass(get_class($crud->getModel()));
        $classname = strtolower($reflection->getShortName());

        // $url = "{$crud->route}/create?{$field['key_master']}={$crud->entry[$field['key_master']]}";
        $url = "{$field['route']}/create";
        @endphp

        <div class="array-controls btn-group m-t-10">
            <!--            <a href="{{ $url }}">
                            <button class="btn btn-sm btn-default" type="button"><i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} $field['name']}}</button>
                        </a>-->
            
           @if(count($field['value'])< $max)
            <a href="javascript:void(0)"  class="btn btn-sm btn-default" onclick="addEdit(this)" data-entity="{{$crud->entity_name}}" data-label="{{$field['name']}}" data-route="{{$url}}"  data-button-type="post"><i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} {{$field['name']}}</a>
            @else
            <button  class="btn btn-sm btn-default" type="button" disabled="" title="Máximo excedido"><i class="fa fa-plus"></i> Máximo excedido</button>
            @endif
        </div>


    </div>
</div>
{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
@push('crud_fields_styles')

{{-- YOUR CSS HERE --}}
<!--<link rel="stylesheet" href="{{ asset('vendor/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">-->
@endpush

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
<script  type="text/javascript">
    if (typeof deleteEntry != 'function') {
        $("[data-button-type=delete]").unbind('click');

        function deleteEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var row = $("#crudTableChild a[data-route='" + route + "']").closest('tr');

            if (confirm("{{ trans('backpack::crud.delete_confirm') }}") == true) {
                $.ajax({
                    url: route,
                    type: 'DELETE',
                    success: function (result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                            type: "success"
                        });

                        // Hide the modal, if any
                        $('.modal').modal('hide');

                        // Remove the details row, if it is open
                        if (row.hasClass("shown")) {
                            row.next().remove();
                        }

                        // Remove the row from the datatable
                        row.remove();
                    },
                    error: function (result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                            type: "warning"
                        });
                    }
                });
            } else {
                // Show an alert telling the user we don't know what went wrong
                new PNotify({
                    title: "{{ trans('backpack::crud.delete_confirmation_not_deleted_title') }}",
                    text: "{{ trans('backpack::crud.delete_confirmation_not_deleted_message') }}",
                    type: "info"
                });
            }
        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');

    if (typeof addEdit != 'function') {
        $("[data-button-type=post]").unbind('click');

        function addEdit(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var entity = button.attr('data-entity');
            var label = button.attr('data-label');
//	      var row = $("#crudTableChild a[data-route='"+route+"']").closest('tr');

            if (confirm("Certifique que você tenha salvo os dados do "+ entity +" antes de adicionar/editar "+label) == true) {
              window.location.href = route;
            } else {
                // Show an alert telling the user we don't know what went wrong
                new PNotify({
                    title: "Salvar",
                    text: "Salve esse registro antes de processeguir com adição do detalhe",
                    type: "warning"

                });
            }
        }
    }
</script>
@endpush

@endif