<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <!-- Theme CSS - Includes Bootstrap -->
  <!--<link href="{{ asset('css/creative.css')}}" rel="stylesheet">-->
  <link href="{{ asset('css/main.css')}}" rel="stylesheet">   
  @yield('styles')

</head>
<body>
   
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                   <a class="navbar-brand js-scroll-trigger" href="#page-top">
                    @if(isset($config) && $config->count() > 0)
                    <a href="{{route('index')}}" class="links" >
                      @if($config->logo != null && $config->logo != "")

                       <img src="{{asset($config->logo)}}" alt="Logo EETAD" id="logo"/>
                      @endif
                      @if($config->nmescola != null && $config->nmescola != "")

                      <span class="titulo"  >{{$config->nmescola}}</span>

                      @endif
                      </a>
                   @endif
                   </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('aluno.login') }}">Entrar</a>
                            </li>
                            @if (Route::has('aluno.register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('aluno.register') }}">Cadastrar</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->nome }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('aluno.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ trans('auth.Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('aluno.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
    <div class="row justify-content-center">
          @if (Auth::guard('aluno')->check())
        <div class="col-md-12">
            <div class="card">
              
                <div class="card-header bg-warning ">
                    <h4>Seja Bem-Vindo(a),<span  class="text-white "> {{Auth::user()->nome}}</span></h4>
                </div>
                @php
                //dd(Auth::user());
                @endphp
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                   
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item {{$menu=='Principal'?'active':''}}">
                                <a class="nav-link" href="{{route('aluno.home')}}">Principal <span class="sr-only">(current)</span></a>
                            </li>
                            @if(Auth::user()->status!=null or Auth::user()->status!='')
                            <li class="nav-item {{$menu=='Dados do Aluno'?'active':''}}">
                                <a class="nav-link" href="{{route('aluno.dados')}}">Dados do Aluno</a>
                            </li>
                            
                            <li class="nav-item {{($menu=='Financeiro')?'active':''}}">
                                <a class="nav-link" href="#">Financeiro</a>
                            </li>
                            @endif
                            <li class="nav-item {{($menu=='Quiz')?'active':''}}">
                                <a class="nav-link" href="{{route('quiz.gerar',['_token' => csrf_token(),
                                            'name' =>Auth::user()->nome,
                                            'email' =>Auth::user()->email,
                                            'telefone' =>Auth::user()->tel,
                                            'nivel' =>'FÁCIL',
                                            'categoria' =>'TODAS',                                            
                                            ])}}">Quiz</a>
                            </li>
<!--                            <li class="nav-item">
                                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                            </li>-->
                        </ul>
                    </div>
                </nav>
              
            @yield('content')
              </div>
        </div>
            
            @else
            @yield('content')
            @endif
    </div>
</div>
        </main>

    </div>
<!-- Scripts -->
    <noscript>Seu navegador não tem suporte a JavaScript ou está desativado!</noscript>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    
    <script  src="{{ asset('js/jquery.mask.min.js') }}"></script>
<script  src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}" ></script>
 <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '3079261772203555',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v8.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/pt_BR/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!--NÃO FUNCIONOU
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v8.0&appId=3079261772203555&autoLogAppEvents=1" nonce="0hNFN6Y0"></script>-->
<!--NÃO FUNCIONOU
<script>
    function fblogin(){
        FB.login(function(response) {
            if (response.authResponse) {
             console.log('Welcome!  Fetching your information.... ');
             FB.api('/me',{locale:'pt-BR', fields: 'id, first_name, last_name, email'},
//             curl -i -X GET \
//  "https://graph.facebook.com/{your-user-id}
//    ?fields=birthday,email,hometown
//    &access_token={your-user-access-token}"
             function(response) {
               console.log( response);
               $.ajax({
                  method:'post',
                  url:"{{URL::to('/aluno/login/facebook/fblogin')}}",
                  data:{
                      email: response.email,
                      id: response.id,
                      name:response.first_name+ " "+ response.last_name,
                      _token: $('#login-token').val();
                  },
                  success: function (result) {
                        window.location.href="{{URL::to('/aluno/home')}}";
                    }
               });
             });
            } else {
             console.log('User cancelled login or did not fully authorize.');
            }
        });
   
    }
</script>-->
</body>
</html>
