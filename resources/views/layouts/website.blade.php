﻿<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        @include('partials.head')
    </head>
<body id="page-top">

        
        @include("partials.header")

        
        
       
        @yield('conteudo')

        @include('partials.footer')
   
        @include('partials.scripts')
       
    </body>

</html>

