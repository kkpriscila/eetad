@extends('layouts.app_aluno')
@section('styles') 

<link href="{{asset('css/status_matricula.css')}}" rel="stylesheet">  
@endsection
@section('content')

<div class="card-body">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>

            @endif

            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong>Sucesso:</strong> {{ Session::get('success') }}
            </div>
            @elseif (Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> {{ Session::get('warning') }}
                    </div>
            @elseif (Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <strong>Erro:</strong> {{ Session::get('error') }}
            </div>
            @endif

            @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
            @endif
            <div class="col-lg-12 col-md-12">
               
                <form class="form" action="{{ route('aluno.editar') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!--<form>-->
@php
//dd($user);
@endphp

                    <div class="col-lg-12 col-md-12 col-sm-12">
<!--                        <h4>Complete seus dados abaixo para fazer sua pré-Matrícula:</h4>
                        <hr>-->
                    </div>
                    <label class="text-danger" >* Campos Obrigatórios</label>
                    <div class='form-row'>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                @if($user->avatar!=null or $user->avatar!='')
                                <img src='{{url($user->avatar)}}' style="max-width: 80%"  class="rounded-circle">

                                @endif

                                <!--                    <label for="exampleFormControlFile1">Sua Foto</label>
                                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">-->

                            </div></div>
                        <div class=" col-md-4 col-lg-4">
                            <div class="input-group input-file" name="avatar">
                                <span class="input-group-btn">
                                    <button class="btn btn-block btn-choose" type="button">Atualizar Foto do Perfil</button>
                                    <input type="text" name="avatar" class="form-control " value="" />
                                </span>

<!--<span class="input-group-btn">
         <button class="btn btn-warning btn-reset" type="button">Reset</button>
</span>-->
                            </div>
                        </div>

                        <div class=" col-md-4 col-lg-4">
                            <strong>Aluno matriculado em:</strong>
                            {{$user->ciclo}}
                            <input type="hidden" name="ciclo" value="{{old('ciclo')?old('ciclo'):$user->ciclo}}"  >
                            <br>
                            {{$user->cidade_eetad}}/{{$user->estado}}
                            <input type="hidden" name="id" value="{{$user->id}}"  >
                            <input type="hidden" name="cidade_eetad" value="{{old('cidade_eetad')?old('cidade_eetad'):$user->cidade_eetad}}"  >
                            <input type="hidden" name="estado" value="{{old('estado')?old('estado'):$user->estado}}"  >
                            <input type="hidden" name="subnucleo_id" value="{{old('subnucleo_id')?old('subnucleo_id'):$subnucleo->id}}"  >
                            <input type="hidden" name="subnucleo_resp" value="{{old('subnucleo_resp')?old('subnucleo_resp'):$subnucleo->responsável}}"  >
                            <input type="hidden" name="subnucleo" value="{{old('subnucleo')?old('subnucleo'):$subnucleo->nome}}"  >
                            <input type="hidden" name="nucleo" value="{{old('nucleo')?old('nucleo'):$user->nucleo}}"  >
                            <br>
                            Subnúcleo:{{$subnucleo->nome}}
                            
                        </div>
                    </div>

                    <hr>
                    <h4>Dados do Candidato</h4>

                    <div class="form-row">
                        <div class="col-md-12 col-lg-12">
                            <label for="nome">Nome Completo*</label>
                            
                            <input type="text" value="{{old('nome')?old('nome'):$user->nome}}"  class="form-control" required id="nome" name="nome" placeholder="">
                        </div>
                        <!--                            <div class="col-md-2 col-lg-2">
                                                        <label for="matricula">Matrícula</label>
                                                        <input type="text" class="form-control" value="{{old('matricula')}}" id="matricula" name="matricula" placeholder="">
                                                    </div>-->
                    </div>
                    <div class="form-row">
                        <div class="col-md-8 col-lg-8">
                            <label for="end">Endereço*</label>
                            <input type="text"  class="form-control" id="end" name="end" required value="{{old('end')?old('end'):$user->end}}" placeholder="">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="num">Num.*</label>
                            <input type="text"  class="form-control" id="num" name="num" required placeholder="" value="{{old('num')?old('num'):$user->num}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="bairro">Bairro*</label>
                            <input type="text"  class="form-control" id="bairro" name="bairro" required placeholder="" value="{{old('bairro')?old('bairro'):$user->bairro}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-5 col-lg-5">
                            <label for="cidade">Cidade*</label>
                            <input type="text"  class="form-control" id="cidade" name="cidade" required placeholder="" value="{{old('cidade')?old('cidade'):$user->cidade}}">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="uf">UF*</label>
                            <input type="text"  class="form-control" id="uf" maxlength="2" size="2" name="uf" placeholder="" required value="{{old('uf')?old('uf'):$user->uf}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="cep">CEP*</label>
                            <input type="text"  class="form-control form cep" id="cep" name="cep" placeholder="" required value="{{old('cep')?old('cep'):$user->cep}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="tel">Telefone</label>
                            <input type="text"  class="form-control form tel" id="tel" name="tel" placeholder=""  value="{{old('tel')?old('tel'):$user->tel}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 col-lg-4">
                            <label for="dtnasc">Dt. Nascimento*</label>
                            <input type="date"  class="form-control" id="dtnasc" name="dtnasc" placeholder="" required value="{{old('dtnasc')?old('dtnasc'):$user->dtnasc->format('Y-m-d')}}">
                        </div>
                        <div class="col-md-7 col-lg-7">
                            <label for="cidnasc">Cidade Nascimento*</label>
                            <input type="text"  class="form-control" id="cidnasc" name="cidnasc" placeholder="" required value="{{old('cidnasc')?old('cidnasc'):$user->cidnasc}}">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="ufnasc">UF*</label>
                            <input type="text"  class="form-control" id="ufnasc" maxlength="2" size="2" name="ufnasc" placeholder="" required value="{{old('ufnasc')?old('ufnasc'):$user->ufnasc}}">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-3 col-lg-3">
                            <label for="nacionalidade">Nacionalidade*</label>
                            <input type="text"  class="form-control" id="nacionalidade" name="nacionalidade" required placeholder="" value="{{old('nacionalidade')?old('nacionalidade'):$user->nacionalidade}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="estcivil">Est. Civil*</label>
                            <input type="text"  class="form-control" id="estcivil" name="estcivil" placeholder="" required value="{{old('estcivil')?old('estcivil'):$user->estcivil}}">
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <label for="email">Email</label>
                            <input type="email"  class="form-control" id="email" name="email" placeholder=""  value="{{old('email')?old('email'):$user->email}}" readonly="readonly">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-5 col-lg-5">
                            <label for="identidade">RG*</label>
                            <input type="text"  class="form-control" id="identidade" name="identidade" required placeholder="" value="{{old('identidade')?old('identidade'):$user->identidade}}">
                        </div>
                        <div class="col-md-5 col-lg-5">
                            <label for="cpf">CPF*</label>
                            <input type="text"  class="form-control form cpf" id="cpf" name="cpf" placeholder="" required value="{{old('cpf')?old('cpf'):$user->cpf}}">
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="sexo">Sexo*</label>
                            <div id='sexo'>
                                @php
                                $sexo = old('sexo')?old('sexo'):$user->sexo;
                                @endphp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="inlineRadio1" required value="Feminino" {{($sexo=='Feminino')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio1">F</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="inlineRadio2" required value="Masculino"  {{($sexo=='Masculino')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio2">M</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-12 col-lg-6">
                            <label for="igreja">Igreja onde é congregado*</label>
                            <input type="text" class="form-control" id="igreja" name="igreja" placeholder="" required  value="{{old('igreja')?old('igreja'):$user->igreja}}">
                        </div>


                        <div class="col-md-12 col-lg-6">
                            <label for="igreja">Começar em qual livro?</label>
                            <input type="text" class="form-control" id="livro" name="livro" placeholder=""   value="{{old('livro')?old('livro'):$user->livro}}">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-3 col-lg-3">
                            <label for="cargo">Exerce cargo na igreja?*</label>
                            <div id='cargo'>
                                 @php
                                $cargo = old('cargo')?old('cargo'):$user->cargo;
                                @endphp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="cargo" id="inlineRadio1" required value="Sim" {{($cargo=='Sim')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="cargo" id="inlineRadio2" required value="Não" {{($cargo=='Não')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio2">Não</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9">
                            <label for="igreja">Qual?</label>
                            <input type="text"  class="form-control" id="igreja" name="qualcargo" placeholder="" value="{{old('qualcargo')?old('qualcargo'):$user->qualcargo}}">
                        </div>

                    </div>
                    <div class="form-row">

                        <div class="col-md-6 col-lg-6">
                            <label for="escolaridade">Escolaridade*</label>
                            <input type="text" class="form-control" id="escolaridade" name="escolaridade" required value="{{old('escolaridade')?old('escolaridade'):$user->escolaridade}}" placeholder="" >
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <label for="profissao">Profissão*</label>
                            <input type="text" class="form-control" id="profissao" name="profissao" required placeholder="" value="{{old('profissao')?old('profissao'):$user->profissao}}" >
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-4 col-lg-4">
                               @php
                                $formacaoteologica = old('formacaoteologica')?old('formacaoteologica'):$user->formacaoteologica;
                                @endphp
                            <label for="formacaoteologica">Possui formação teológica?*</label>
                            <div id='formacaoteologica'>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio1" value="Sim" {{($formacaoteologica=='Sim')? 'checked':''}}  >
                                    <label class="form-check-label" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio2" value="Não" {{($formacaoteologica=='Não')? 'checked':''}}  >
                                    <label class="form-check-label" for="inlineRadio2">Não</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <label for="escola">Escola</label>
                            <input type="text" class="form-control" id="escola" name="escola" placeholder="" value="{{old('escola')?old('escola'):$user->escola}}" >
                        </div>

                    </div>

                    <br>
                    <div class="form-inline">
                        <div class="form-check col-md-8 col-lg-8">
        <!--                                <input class="form-check-input" type="checkbox" value="Sim" name="imprimir" id="imprimir" {{old('imprimir') ? 'checked' :''}} >
                            <label class="form-check-label" for="invalidCheck">
                                Imprimir Ficha, Normas e Orientações.
                            </label>-->

                        </div>
                        <button class="btn btn-warning col-md-4 col-lg-4" type="submit">Editar Dados</button>
                    </div>

                </form>
              
            </div>
        </div>

        @endsection
