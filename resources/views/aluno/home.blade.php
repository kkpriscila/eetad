@extends('layouts.app_aluno')
@section('styles') 

<link href="{{asset('css/status_matricula.css')}}" rel="stylesheet">  
@endsection
@section('content')

<div class="card-body">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>

            @endif

            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong>Sucesso:</strong> {{ Session::get('success') }}
            </div>
            @elseif (Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> {{ Session::get('warning') }}
                    </div>
            @elseif (Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <strong>Erro:</strong> {{ Session::get('error') }}
            </div>
            @endif

            @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
            @endif
            <div class="col-lg-12 col-md-12">
                @if($user->status== null or $user->status=="")
                <form class="form" action="{{ route('aluno.matricula') }}" method="post">
                    {{ csrf_field() }}
                    <!--<form>-->


                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h4>Complete seus dados abaixo para fazer sua pré-Matrícula:</h4>
                        <hr>
                    </div>
                    <label class="text-danger" >* Campos Obrigatórios</label>
                    <div class='form-row'>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                @if(Auth::user()->avatar!=null or Auth::user()->avatar!='')
                                <img src='{{Auth::user()->avatar}}' style="max-width: 80%"  class="rounded-circle">

                                @endif

                                <!--                    <label for="exampleFormControlFile1">Sua Foto</label>
                                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">-->

                            </div></div>
                        <div class=" col-md-4 col-lg-4">
                            <div class="input-group input-file" name="avatar">
                                <span class="input-group-btn">
                                    <button class="btn btn-block btn-choose" type="button">Atualizar Foto do Perfil</button>
                                    <input type="text" name="avatar" class="form-control " value="" />
                                </span>

<!--<span class="input-group-btn">
         <button class="btn btn-warning btn-reset" type="button">Reset</button>
</span>-->
                            </div>
                        </div>

                        <div class=" col-md-4 col-lg-4">
                             @php
                            $ciclo= old("ciclo")?old("ciclo"):$user->ciclo
                            @endphp
                            <div class="form-check">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio1" value="1º Ciclo Básico" {{($ciclo=='1º Ciclo Básico')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio1">1º Ciclo Básico</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio2" value="2º Ciclo Médio I" {{($ciclo=='2º Ciclo Médio I')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio2">2º Ciclo Médio I </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio3" value="2º Ciclo Médio II" {{($ciclo=='2º Ciclo Médio II')? 'checked':''}}  >
                                       <label class="form-check-label" for="inlineRadio3">2º Ciclo Médio II</label>
                            </div>

                        </div>
                    </div>

                    <br>
                    <div class='form-row'>
                        <div class="col-md-3 col-lg-3">
                             @php
                            $cidade_eetad = old("cidade_eetad")?old("cidade_eetad"):$user->cidade_eetad
                            @endphp
                            
                            <label for="cidade_eetad">Cidade do curso*</label>
                            <select name="cidade_eetad" class="form-control" required id="cidade_eetad" >
                                <option {{($cidade_eetad == "" ? "selected":"") }}></option>
                                <option {{($cidade_eetad == "Coronel Fabriciano" ? "selected":"") }}>Coronel Fabriciano</option>
                                <option {{($cidade_eetad == "Ipatinga" ? "selected":"") }}>Ipatinga</option>
                                <option {{($cidade_eetad == "Santana do Paraíso" ? "selected":"") }}>Santana do Paraíso</option>
                                <!--                                    <option>4</option>
                                                                    <option>5</option>-->
                            </select>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="estado">Estado</label>
                            <input name="estado" id='estado'  class="form-control" required type="text" placeholder="MG" value="MG" readonly>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="nucleo">Núcleo nº</label>
                            <input name='nucleo' id='nucleo'  class="form-control" type="text" placeholder="0325" value="0325" readonly>
                        </div>
                        <div class="col-md-5 col-lg-5">
                            <label for="subnucleo_id">Selecione o Subnúcleo*</label>
                            @php
                            $subnucleo = old('subnucleo_id')?old('subnucleo_id'):$user->subnucleo_id
                            @endphp
                            <select name="subnucleo_id" class="form-control" id="subnucleo_id" >
                                <option value=""  >Selecione o Subnucleo que deseja cursar</option>
                                    @foreach($subnucleos as $a)


                                    <option value="{{ $a->id }}" {{($subnucleo== $a->id?'selected':'')}}> {{ $a->nome }}</option>

                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h4>Dados do Candidato</h4>

                    <div class="form-row">
                        <div class="col-md-12 col-lg-12">
                            <label for="nome">Nome Completo*</label>
                            
                            <input type="text" value="{{old('nome')?old('nome'):$user->nome}}"  class="form-control" required id="nome" name="nome" placeholder="">
                        </div>
                        <!--                            <div class="col-md-2 col-lg-2">
                                                        <label for="matricula">Matrícula</label>
                                                        <input type="text" class="form-control" value="{{old('matricula')}}" id="matricula" name="matricula" placeholder="">
                                                    </div>-->
                    </div>
                    <div class="form-row">
                        <div class="col-md-8 col-lg-8">
                            <label for="end">Endereço*</label>
                            <input type="text"  class="form-control" id="end" name="end" required value="{{old('end')?old('end'):$user->end}}" placeholder="">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="num">Num.*</label>
                            <input type="text"  class="form-control" id="num" name="num" required placeholder="" value="{{old('num')?old('num'):$user->num}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="bairro">Bairro*</label>
                            <input type="text"  class="form-control" id="bairro" name="bairro" required placeholder="" value="{{old('bairro')?old('bairro'):$user->bairro}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-5 col-lg-5">
                            <label for="cidade">Cidade*</label>
                            <input type="text"  class="form-control" id="cidade" name="cidade" required placeholder="" value="{{old('cidade')?old('cidade'):$user->cidade}}">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="uf">UF*</label>
                            <input type="text"  class="form-control" id="uf" name="uf" maxlength="2" size="2" placeholder=""  required value="{{old('uf')?old('uf'):$user->uf}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="cep">CEP*</label>
                            <input type="text"  class="form-control form cep" id="cep" name="cep" placeholder="" required value="{{old('cep')?old('cep'):$user->cep}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="tel">Telefone</label>
                            <input type="text"  class="form-control form tel" id="tel" name="tel" placeholder=""  value="{{old('tel')?old('tel'):$user->tel}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 col-lg-4">
                            <label for="dtnasc">Dt. Nascimento*</label>
                            <input type="date"  class="form-control" id="dtnasc" name="dtnasc" placeholder="" required value="{{old('dtnasc')?old('dtnasc'):($user->dtnasc!=null or $user->dtnasc!=""? $user->dtnasc->format('Y-m-d'):"")}}">
                        </div>
                        <div class="col-md-7 col-lg-7">
                            <label for="cidnasc">Cidade Nascimento*</label>
                            <input type="text"  class="form-control" id="cidnasc" name="cidnasc" placeholder="" required value="{{old('cidnasc')?old('cidnasc'):$user->cidnasc}}">
                        </div>
                        <div class="col-md-1 col-lg-1">
                            <label for="ufnasc">UF*</label>
                            <input type="text"  class="form-control" id="ufnasc" name="ufnasc" maxlength="2" size="2" placeholder="" required value="{{old('ufnasc')?old('ufnasc'):$user->ufnasc}}">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-3 col-lg-3">
                            <label for="nacionalidade">Nacionalidade*</label>
                            <input type="text"  class="form-control" id="nacionalidade" name="nacionalidade" required placeholder="" value="{{old('nacionalidade')?old('nacionalidade'):$user->nacionalidade}}">
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="estcivil">Est. Civil*</label>
                            <input type="text"  class="form-control" id="estcivil" name="estcivil" placeholder="" required value="{{old('estcivil')?old('estcivil'):$user->estcivil}}">
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <label for="email">Email</label>
                            <input type="email"  class="form-control" id="email" name="email" placeholder=""  value="{{old('email')?old('email'):$user->email}}" readonly="readonly">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-5 col-lg-5">
                            <label for="identidade">RG*</label>
                            <input type="text"  class="form-control" id="identidade" name="identidade" required placeholder="" value="{{old('identidade')?old('identidade'):$user->identidade}}">
                        </div>
                        <div class="col-md-5 col-lg-5">
                            <label for="cpf">CPF*</label>
                            <input type="text"  class="form-control form cpf" id="cpf" name="cpf" placeholder="" required value="{{old('cpf')?old('cpf'):$user->cpf}}">
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="sexo">Sexo*</label>
                            <div id='sexo'>
                            @php
                            $sexo = old("sexo")?old("sexo"):$user->sexo
                            @endphp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="inlineRadio1" required value="Feminino" {{($sexo=='Feminino')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio1">F</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="inlineRadio2" required value="Masculino"  {{($sexo=='Masculino')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio2">M</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-12 col-lg-6">
                            <label for="igreja">Igreja onde é congregado*</label>
                            <input type="text" class="form-control" id="igreja" name="igreja" placeholder="" required  value="{{old('igreja')?old('igreja'):$user->igreja}}">
                        </div>


                        <div class="col-md-12 col-lg-6">
                            <label for="igreja">Começar em qual livro?</label>
                            <input type="text" class="form-control" id="livro" name="livro" placeholder=""   value="{{old('livro')?old('livro'):$user->livro}}">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-3 col-lg-3">
                            <label for="cargo">Exerce cargo na igreja?*</label>
                            <div id='cargo'>
                            @php
                            $cargo = old("cargo")?old("cargo"):$user->cargo
                            @endphp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="cargo" id="inlineRadio1" required value="Sim" {{($cargo=='Sim')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="cargo" id="inlineRadio2" required value="Não" {{($cargo=='Não')? 'checked':''}} >
                                    <label class="form-check-label" for="inlineRadio2">Não</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9">
                            <label for="igreja">Qual?</label>
                            <input type="text"  class="form-control" id="igreja" name="qualcargo" placeholder="" value="{{old('qualcargo')?old('qualcargo'):$user->qualcargo}}">
                        </div>

                    </div>
                    <div class="form-row">

                        <div class="col-md-6 col-lg-6">
                            <label for="escolaridade">Escolaridade*</label>
                            <input type="text" class="form-control" id="escolaridade" name="escolaridade" required value="{{old('escolaridade')?old('escolaridade'):$user->escolaridade}}" placeholder="" >
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <label for="profissao">Profissão*</label>
                            <input type="text" class="form-control" id="profissao" name="profissao" required placeholder="" value="{{old('profissao')?old('profissao'):$user->profissao}}" >
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-4 col-lg-4">
                            @php
                            $formacaoteologica = old("formacaoteologica")?old("formacaoteologica"):$user->formacaoteologica
                            @endphp
                            <label for="formacaoteologica">Possui formação teológica?*</label>
                            <div id='formacaoteologica'>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio1" value="Sim" {{($formacaoteologica=='Sim')? 'checked':''}}  >
                                    <label class="form-check-label" for="inlineRadio1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio2" value="Não" {{($formacaoteologica=='Não')? 'checked':''}}  >
                                    <label class="form-check-label" for="inlineRadio2">Não</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <label for="escola">Escola</label>
                            <input type="text" class="form-control" id="escola" name="escola" placeholder="" value="{{old('escola')?old('escola'):$user->escola}}" >
                        </div>

                    </div>

                    <br>
                    <div class="form-inline">
                        <div class="form-check col-md-8 col-lg-8">
        <!--                                <input class="form-check-input" type="checkbox" value="Sim" name="imprimir" id="imprimir" {{old('imprimir') ? 'checked' :''}} >
                            <label class="form-check-label" for="invalidCheck">
                                Imprimir Ficha, Normas e Orientações.
                            </label>-->

                        </div>
                        <button class="btn btn-warning col-md-4 col-lg-4" type="submit">Solicitar Matrícula</button>
                    </div>

                </form>
                @else
                <div class="col-lg-12 col-md-12">
                    Acompanhe aqui o status da sua Matrícula:
                    @if($user->status == "APROVADA" )    
                    <div class="row">                    
                        <div class="col-md-12 success" id="messagebox" >
                            <div class="row">
                                <div class="col-md-2" id="messagebox-icon">
                                    <span class="fa fa-2x fa-check text-success"></span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row h5" id="messagebox-header">Matrícula Efetivada. Segue abaixo dados do seu subnúcleo:</div>
                                    <div class="row">{{$user->subnucleo}}<br>
                                        @if($user->resp_subnucleo!=null or $user->resp_subnucleo!="")
                                        Responsável:&nbsp;{{$user->resp_subnucleo}}<br>
                                        @endif
                                        @if($user->resp_subnucleo!=null or $user->resp_subnucleo!="")
                                        Email:&nbsp;{{$user->email_subnucleo}}
                                        @endif</div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-check-circle text-success"></span>
                                        </div>
                                        <div class="col-md-10">
                                            Cadastro de Matrícula - OK
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-check-circle text-success"></span>
                                        </div>
                                        <div class="col-md-10">
                                            {{$user->justifica_status}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pull-right" id="messagebox-detail-link">
                                    <!--<a href="#">More Information</a>-->
                                    Entre em contato com nossa secretaria por um de nossos canais:
                                    <br/>
                                    @if($config->telefone_escola != null && $config->telefone_escola != "")
                                    Telefone: 
                                    <a href="tel:+55{{substr($config->telefone_escola, 0, 11)}}" target="_blank">

                                        {{"(".substr($config->telefone_escola, 0, 2).") ".substr($config->telefone_escola, 2, 4)."-".substr($config->telefone_escola,6) }}
                                    </a>
                                    <br/>
                                    @endif

                                    @if($config->whatsapp!= null && $config->whatsapp != "")
                                    WhatsApp: 
                                    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$config->whatsapp}}" target="_blank">

                                        {{"(".substr($config->whatsapp, 0, 2).") ".substr($config->whatsapp, 2, 4)."-".substr($config->whatsapp,6) }}
                                    </a><br/>

                                    @endif

                                    @if($config->email!= null && $config->email != "")
                                    Email: 
                                    <a href="mailto:{{$config->email}}" target="_blank">

                                        {{$config->email}}
                                    </a><br/>         
                                    @endif
                                </div>
                            </div> 
                        </div>

                    </div>
                    @elseif($user->status == "REPROVADA") 
                    <div class="row" >

                        <div class="col-md-12 fail" id="messagebox" >
                            <div class="row">
                                <div class="col-md-2" id="messagebox-icon">
                                    <span class="fa fa-2x fa-times text-danger"></span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row h5" id="messagebox-header">Sua matrícula Não foi Efetivada</div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-times-circle text-danger"></span>
                                        </div>
                                        <div class="col-md-10">
                                              Cadastro de Matrícula - OK
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-times-circle text-danger"></span>
                                        </div>
                                        <div class="col-md-10">
                                            {{$user->justifica_status}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pull-right" id="messagebox-detail-link">
                                    <!--<a href="#">More Information</a>-->
                                    Entre em contato com nossa secretaria por um de nossos canais:
                                    <br/>
                                    @if($config->telefone_escola != null && $config->telefone_escola != "")
                                    Telefone: 
                                    <a href="tel:+55{{substr($config->telefone_escola, 0, 11)}}" target="_blank">

                                        {{"(".substr($config->telefone_escola, 0, 2).") ".substr($config->telefone_escola, 2, 4)."-".substr($config->telefone_escola,6) }}
                                    </a>
                                    <br/>
                                    @endif

                                    @if($config->whatsapp!= null && $config->whatsapp != "")
                                    WhatsApp: &nbsp;
                                    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$config->whatsapp}}" target="_blank">

                                        {{"(".substr($config->whatsapp, 0, 2).") ".substr($config->whatsapp, 2, 4)."-".substr($config->whatsapp,6) }}
                                    </a><br/>

                                    @endif

                                    @if($config->email!= null && $config->email != "")
                                    Email: &nbsp;
                                    <a href="mailto:{{$config->email}}" target="_blank">

                                        {{$config->email}}
                                    </a><br/>         
                                    @endif
                                </div>
                            </div> 
                        </div>

                    </div>
                    @elseif($user->status == "PENDENTE")
                    <div class="row" >

                        <div class="col-md-12 warning" id="messagebox" >
                            <div class="row">
                                <div class="col-md-1" id="messagebox-icon">
                                    <span class="fa fa-2x fa-exclamation-triangle text-warning"></span>
                                </div>
                                <div class="col-md-11">
                                    <div class="row h5" id="messagebox-header">
                                        Matrícula enviada, entre em contato com a secretaria para efetivação.
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-check-circle text-success"></span>
                                        </div>
                                        <div class="col-md-10">
                                            Cadastro de Matrícula - OK
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-times-circle text-danger"></span>
                                        </div>
                                        <div class="col-md-10">
                                            {{$user->justifica_status}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pull-right" id="messagebox-detail-link">
                                    <!--<a href="#">More Information</a>-->
                                    Entre em contato com nossa secretaria por um de nossos canais:
                                    <br/>
                                    @if($config->telefone_escola != null && $config->telefone_escola != "")
                                    Telefone: &nbsp;
                                    <a href="tel:+55{{substr($config->telefone_escola, 0, 11)}}" target="_blank">

                                        {{"(".substr($config->telefone_escola, 0, 2).") ".substr($config->telefone_escola, 2, 4)."-".substr($config->telefone_escola,6) }}
                                    </a>
                                    <br/>
                                    @endif

                                    @if($config->whatsapp!= null && $config->whatsapp != "")
                                    WhatsApp: &nbsp;
                                    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$config->whatsapp}}" target="_blank">

                                        {{"(".substr($config->whatsapp, 0, 2).") ".substr($config->whatsapp, 2, 4)."-".substr($config->whatsapp,6) }}
                                    </a><br/>

                                    @endif

                                    @if($config->email!= null && $config->email != "")
                                    Email: &nbsp;
                                    <a href="mailto:{{$config->email}}" target="_blank">

                                        {{$config->email}}
                                    </a><br/>         
                                    @endif
                                </div>
                            </div> 
                        </div>

                    </div>
                    @elseif($user->status == "ERRO")
                    <div class="row" >

                        <div class="col-md-12 warning" id="messagebox" >
                            <div class="row">
                                <div class="col-md-2" id="messagebox-icon">
                                    <span class="fa fa-2x fa-exclamation-triangle text-warning"></span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row h5" id="messagebox-header">
                                        Sua matrícula está com alguma pendência. Verifique!
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-check-circle text-success"></span>
                                        </div>
                                        <div class="col-md-10">
                                              Cadastro de Matrícula - OK
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <span class="fa fa-times-circle text-danger"></span>
                                        </div>
                                        <div class="col-md-10">
                                            {{$user->justifica_status}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pull-right" id="messagebox-detail-link">
                                    <!--<a href="#">More Information</a>-->
                                    Entre em contato com nossa secretaria por um de nossos canais:
                                    <br/>
                                    @if($config->telefone_escola != null && $config->telefone_escola != "")
                                    Telefone: 
                                    <a href="tel:+55{{substr($config->telefone_escola, 0, 11)}}" target="_blank">

                                        {{"(".substr($config->telefone_escola, 0, 2).") ".substr($config->telefone_escola, 2, 4)."-".substr($config->telefone_escola,6) }}
                                    </a>
                                    <br/>
                                    @endif

                                    @if($config->whatsapp!= null && $config->whatsapp != "")
                                    WhatsApp: 
                                    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55{{$config->whatsapp}}" target="_blank">

                                        {{"(".substr($config->whatsapp, 0, 2).") ".substr($config->whatsapp, 2, 4)."-".substr($config->whatsapp,6) }}
                                    </a><br/>

                                    @endif

                                    @if($config->email!= null && $config->email != "")
                                    Email: 
                                    <a href="mailto:{{$config->email}}" target="_blank">

                                        {{$config->email}}
                                    </a><br/>         
                                    @endif
                                </div>
                            </div> 
                        </div>

                    </div>
                    @endif
                </div>
                @endif
            </div>
        </div>

        @endsection
