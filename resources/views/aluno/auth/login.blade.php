@extends('layouts.app_aluno')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white bg-warning mb-3">
                    <h4>Acesso restrito à Alunos e Pré-Matrícula</h4>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('aluno.login') }}">
                         @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ trans('auth.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{trans('auth.Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                           <label class="form-check-label" for="remember">
                                        {{ trans('auth.Remember') }}
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-warning">
                                    {{ trans('auth.Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('aluno.password.request') }}">
                                    {{ trans('auth.Forgot_password') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-12 col-lg-12 h5">Ainda não fez seu CADASTRO? Escolha uma das opções abaixo:
                         <hr/></div>
                       
<!--                         <div class=" offset-md-3 d-none d-md-none d-lg-block">
                        </div>-->
                        <div class="col-sm-12 col-md-4 col-lg-4  ">
                            
                             <a class="btn btn-warning" href="{{ route('aluno.register') }}">Cadastre seu Usuário</a>   
                           
                        </div>
 
                        <div class="col-sm-12 col-md-4 col-lg-4  ">
                            <!--<button  class="btn btn-info" onclick="fblogin()">Login com Facebook</button>-->
                               <a href="{{route('aluno.facebook')}}"><div  class="btn btn-facebook" > <i class="fab fa-facebook"></i>  Entrar com Facebook</div></a>
                            <!--<div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="default" data-auto-logout-link="true" data-use-continue-as="true" data-width=""></div>-->
                            
                        </div>
                
                        <div class="col-sm-12 col-md-4 col-lg-4  ">
                           <a href="{{route('aluno.google')}}"> <div  class="btn btn-default" > <i class="fab fa-google"></i> Entrar com Google</div></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
