@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><a href="{{route('home')}}">FORMULÁRIO DE MATRÍCULA</a>
                        <br><h3>Programa de Formação em Teologia</h3></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>

                    @endif

                    @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong>Sucesso:</strong> {{ Session::get('success') }}
                    </div>
                    @elseif (Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> {{ Session::get('warning') }}
                    </div>
                    @elseif (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Erro:</strong> {{ Session::get('error') }}
                    </div>
                    @endif

                    @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                     <a href="{{route('home')}}" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left"></i> Voltar</a>
                    <form class="form" action="{{ route('matricula') }}" method="post">
                        {{ csrf_field() }}
                        <!--<form>-->
                        <input type="hidden" name="subnucleo_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="subnucleo" value="{{Auth::user()->nome}}">
                        <input type="hidden" name="resp_subnucleo" value="{{Auth::user()->responsável}}">
                        <input type="hidden" name="email_subnucleo" value="{{Auth::user()->email}}">
                        <label class="text-danger" >* Campos Obrigatórios</label>
                        <div class="form-group ">
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio1" value="1º Ciclo Básico" {{(old('ciclo')=='1º Ciclo Básico')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio1">1º Ciclo Básico</label>
                            </div>
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio2" value="2º Ciclo Médio I" {{(old('ciclo')=='2º Ciclo Médio I')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio2">2º Ciclo Médio I </label>
                            </div>
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" required type="radio" name="ciclo" id="inlineRadio3" value="2º Ciclo Médio II" {{(old('ciclo')=='2º Ciclo Médio II')? 'checked':''}}  >
                                       <label class="form-check-label" for="inlineRadio3">2º Ciclo Médio II</label>
                            </div>
                        </div>


                        <div class='form-row'>
                            <div class="col-md-6 col-lg-6">
                                <label for="cidade_eetad">Cidade*</label>
                                <select name="cidade_eetad" class="form-control" required id="cidade_eetad" >
                                    <option {{(old("cidade_eetad") == "" ? "selected":"") }}></option>
                                    <option {{(old("cidade_eetad") == "Coronel Fabriciano" ? "selected":"") }}>Coronel Fabriciano</option>
                                    <option {{(old("cidade_eetad") == "Ipatinga" ? "selected":"") }}>Ipatinga</option>
                                    <option {{(old("cidade_eetad") == "Santana do Paraíso" ? "selected":"") }}>Santana do Paraíso</option>
                                    <!--                                    <option>4</option>
                                                                        <option>5</option>-->
                                </select>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="estado">Estado</label>
                                <input name="estado" id='estado'  class="form-control" required type="text" placeholder="MG" value="MG" readonly>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="nucleo">Núcleo nº</label>
                                <input name='nucleo' id='nucleo'  class="form-control" type="text" placeholder="0325" value="0325" readonly>
                            </div>
<!--                            <div class="col-md-2 col-lg-2">
                                <label for="turma">Turma/Subnucleo</label>
                                <input name='turma' id='turma' class="form-control" type="text" placeholder="" value="{{old('turma')}}"  >
                            </div>-->
                        </div>
                        <hr>
                        <h4>Dados do Candidato</h4>

                        <div class="form-row">
                            <div class="col-md-12 col-lg-12">
                                <label for="nome">Nome Completo*</label>
                                <input type="text" value="{{old('nome')}}"  class="form-control" required id="nome" name="nome" placeholder="">
                            </div>
    <!--                            <div class="col-md-2 col-lg-2">
                                    <label for="matricula">Matrícula</label>
                                    <input type="text" class="form-control" value="{{old('matricula')}}" id="matricula" name="matricula" placeholder="">
                                </div>-->
                        </div>
                        <div class="form-row">
                            <div class="col-md-8 col-lg-8">
                                <label for="end">Endereço*</label>
                                <input type="text"  class="form-control" id="end" name="end" required value="{{old('end')}}" placeholder="">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="num">Num.*</label>
                                <input type="text"  class="form-control" id="num" name="num" required placeholder="" value="{{old('num')}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="bairro">Bairro*</label>
                                <input type="text"  class="form-control" id="bairro" name="bairro" required placeholder="" value="{{old('bairro')}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-5 col-lg-5">
                                <label for="cidade">Cidade*</label>
                                <input type="text"  class="form-control" id="cidade" name="cidade" required placeholder="" value="{{old('cidade')}}">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="uf">UF*</label>
                                <input type="text"  size="2" maxlength="2" size="2" class="form-control" id="uf" name="uf" placeholder="" required value="{{old('uf')}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="cep">CEP*</label>
                                <input type="text"  class="form-control form cep" id="cep" name="cep" placeholder="" required value="{{old('cep')}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="tel">Telefone</label>
                                <input type="text"  class="form-control form tel" id="tel" name="tel" placeholder=""  value="{{old('tel')}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="dtnasc">Dt. Nascimento*</label>
                                <input type="date"  class="form-control" id="dtnasc" name="dtnasc" placeholder="" required value="{{old('dtnasc')}}">
                            </div>
                            <div class="col-md-7 col-lg-7">
                                <label for="cidnasc">Cidade Nascimento*</label>
                                <input type="text"  class="form-control" id="cidnasc" name="cidnasc" placeholder="" required value="{{old('cidnasc')}}">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="ufnasc">UF*</label>
                                <input type="text"  class="form-control" maxlength="2" size="2" id="ufnasc" name="ufnasc" placeholder="" required value="{{old('ufnasc')}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-3 col-lg-3">
                                <label for="nacionalidade">Nacionalidade*</label>
                                <input type="text"  class="form-control" id="nacionalidade" name="nacionalidade" required placeholder="" value="{{old('nacionalidade')}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="estcivil">Est. Civil*</label>
                                <input type="text"  class="form-control" id="estcivil" name="estcivil" placeholder="" required value="{{old('estcivil')}}">
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <label for="email">Email</label>
                                <input type="email"  class="form-control" id="email" name="email" placeholder=""  value="{{old('email')}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-5 col-lg-5">
                                <label for="identidade">RG*</label>
                                <input type="text"  class="form-control" id="identidade" name="identidade" required placeholder="" value="{{old('identidade')}}">
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <label for="cpf">CPF*</label>
                                <input type="text"  class="form-control form cpf" id="cpf" name="cpf" placeholder="" required value="{{old('cpf')}}">
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <label for="sexo">Sexo*</label>
                                <div id='sexo'>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" id="inlineRadio1" required value="Feminino" {{(old('sexo')=='Feminino')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio1">F</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" id="inlineRadio2" required value="Masculino"  {{(old('sexo')=='Masculino')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio2">M</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-12 col-lg-6">
                                <label for="igreja">Igreja onde é congregado*</label>
                                <input type="text" class="form-control" id="igreja" name="igreja" placeholder="" required  value="{{old('igreja')}}">
                            </div>

                        
                            <div class="col-md-12 col-lg-6">
                                <label for="igreja">Começar em qual livro?</label>
                                <input type="text" class="form-control" id="livro" name="livro" placeholder=""   value="{{old('livro')}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-3 col-lg-3">
                                <label for="cargo">Exerce cargo na igreja?*</label>
                                <div id='cargo'>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cargo" id="inlineRadio1" required value="Sim" {{(old('cargo')=='Sim')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cargo" id="inlineRadio2" required value="Não" {{(old('cargo')=='Não')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <label for="igreja">Qual?</label>
                                <input type="text"  class="form-control" id="igreja" name="qualcargo" placeholder="" value="{{old('qualcargo')}}">
                            </div>

                        </div>
                        <div class="form-row">

                            <div class="col-md-6 col-lg-6">
                                <label for="escolaridade">Escolaridade*</label>
                                <input type="text" class="form-control" id="escolaridade" name="escolaridade" required value="{{old('escolaridade')}}" placeholder="" >
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <label for="profissao">Profissão*</label>
                                <input type="text" class="form-control" id="profissao" name="profissao" required placeholder="" value="{{old('profissao')}}" >
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="formacaoteologica">Possui formação teológica?*</label>
                                <div id='formacaoteologica'>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio1" value="Sim" {{(old('formacaoteologica')=='Sim')? 'checked':''}}  >
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="formacaoteologica" required id="inlineRadio2" value="Não" {{(old('formacaoteologica')=='Não')? 'checked':''}}  >
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-8">
                                <label for="escola">Escola</label>
                                <input type="text" class="form-control" id="escola" name="escola" placeholder="" value="{{old('escola')}}" >
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="taxamatricula">Pagamento taxa de Matrícula? </label>
                                <div id='taxamatricula'>
                                    <div class="form-check ">
                                        <input class="form-check-input" type="radio" name="txmatricula" id="inlineRadio1" value="Sim" {{(old('txmatricula')=='Sim')? 'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <br><br>
                                    <div class="form-check ">
                                        <input class="form-check-input" type="radio" name="txmatricula" id="inlineRadio2" value="Não" {{(old('txmatricula')=='Não')? 'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-8 col-lg-8">
                                <label class="my-1 mr-2" for="dttaxamatricula">Data: </label>
                                <input type="date" class="form-control form-control-sm  my-1 mr-sm-2" id="dttaxamatricula" name="dttaxamatricula" placeholder="" value="{{old('dttaxamatricula')}}" >

                                <label class="my-1 mr-2" for="motivotxmatricula">Motivo: </label>
                                <input type="text" class="form-control form-control-sm my-1 mr-sm-2" id="motivotxmatricula" name="motivotxmatricula" placeholder="" value="{{old('motivotxmatricula')}}" >
                            </div>
                        </div>
                        <br>
                        <div class="form-inline">
                            <div class="form-check col-md-7 col-lg-7">
<!--                                <input class="form-check-input" type="checkbox" value="Sim" name="imprimir" id="imprimir" {{old('imprimir') ? 'checked' :''}} >
                                <label class="form-check-label" for="invalidCheck">
                                    Imprimir Ficha, Normas e Orientações.
                                </label>-->

                            </div>
                            <button class="btn btn-primary col-md-4 col-lg-4" type="submit">Enviar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
