@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2><a href="{{route('home')}}">Área do secretário de Subnúcleo</a>
                        <br><small>Programa de Formação em Teologia</small></h2></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>

                    @endif

                    @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong>Sucesso:</strong> {{ Session::get('success') }}
                    </div>
                    @elseif (Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> {{ Session::get('warning') }}
                    </div>
                    @elseif (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Erro:</strong> {{ Session::get('error') }}
                    </div>
                    @endif

                    @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <div class="container-fluid">
                        <h4>Acompanhe suas matrículas abaixo:
                            <a href="{{route('form.matricula')}}" class="btn btn-primary float-right"> <i class="fa fa-address-card "></i> Envio de Matrícula</a>
                        </h4>
                        <hr>
                        <table class="table table-striped custab  table-hover table-responsive">
                            <thead>
                            
                            <tr>
                                <th >Dt. Envio</th>
                                <th >Nome</th>
                                <th >Email</th>
                                <th >Cpf</th>
                                <th class="">Status/Secretaria</th>
                                <th class="text-center "></th>
                            </tr>
                            </thead>
                            @forelse ($matriculas as $m)
                            <tr>
                                <td>{{$m->created_at->format('d/m/Y')}}</td>
                                <td>{{$m->nome}}</td>
                                <td>{{$m->email}}</td>
                                <td>{{$m->cpf}}</td>
                                <td>
                                    @if($m->status == "PENDENTE")
                                    <span class="label label-warning">{{$m->status}}</span>
                                    @elseif($m->status == "ERRO")
                                    <span class="label label-danger">{{$m->status}}</span>
                                    @elseif($m->status == "REPROVADA")
                                    <span class="label label-danger">{{$m->status}}</span>
                                    @elseif($m->status == "APROVADA")
                                    <span class="label label-success">{{$m->status}}</span>
                                    @endif
                                </td>
                                @if($m->status != "APROVADA")
                                <td class="text-center"><a class='btn btn-info btn-xs  col-md-12 col-lg-12' href="{{route('editar.matricula',['id' => $m->id])}}"><span class="glyphicon glyphicon-edit"></span> Editar Dados </a> </td>
                                @else
                                <td class="text-center "><a class='btn btn-default btn-xs  col-md-12 col-lg-12' href="{{route('editar.matricula',['id' => $m->id])}}"><span class="glyphicon glyphicon-edit"></span>Visualizar Dados</a> </td>
                                @endif
                            </tr>
                            @empty
                            @endforelse
                            
                        </table>
                        {{ $matriculas->links() }}	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
