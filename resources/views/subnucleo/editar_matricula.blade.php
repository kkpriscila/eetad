@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><a href="{{route('home')}}">FORMULÁRIO DE MATRÍCULA</a>
                        <br><h3>Programa de Formação em Teologia</h3></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>

                    @endif

                    @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong>Sucesso:</strong> {{ Session::get('success') }}
                    </div>
                    @elseif (Session::has('warning'))
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> {{ Session::get('warning') }}
                    </div>
                    @elseif (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Erro:</strong> {{ Session::get('error') }}
                    </div>
                    @endif

                    @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    @if($matricula->justifica_status!="" or $matricula->justifica_status!=null)
                    <div class="alert alert-danger" role="alert">
                        {{ $matricula->justifica_status }}
                    </div>
                    @endif
                    <a href="{{route('home')}}" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left"></i> Voltar</a>
                    <form class="form" action="{{ route('update.matricula') }}" method="post">
                        {{ csrf_field() }}
                        <!--<form>-->
                        <input type="hidden" name="id" value="{{$matricula->id}}">
                        <input type="hidden" name="subnucleo_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="subnucleo" value="{{Auth::user()->nome}}">
                        <input type="hidden" name="resp_subnucleo" value="{{Auth::user()->responsável}}">
                        <input type="hidden" name="email_subnucleo" value="{{Auth::user()->email}}">
                        <label class="text-danger" >* Campos Obrigatórios</label>
                        @php
                        $ciclo = old('ciclo')?old('ciclo'):$matricula->ciclo;
                        @endphp
                        <div class="form-group ">
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} required type="radio" name="ciclo" id="inlineRadio1" value="1º Ciclo Básico" {{($ciclo=='1º Ciclo Básico')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio1">1º Ciclo Básico</label>
                            </div>
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} required type="radio" name="ciclo" id="inlineRadio2" value="2º Ciclo Médio I" {{($ciclo=='2º Ciclo Médio I')? 'checked':''}} >
                                       <label class="form-check-label" for="inlineRadio2">2º Ciclo Médio I </label>
                            </div>
                            <div class="form-check form-check-inline col-md-3 col-lg-3">
                                <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} required type="radio" name="ciclo" id="inlineRadio3" value="2º Ciclo Médio II" {{($ciclo=='2º Ciclo Médio II')? 'checked':''}}  >
                                       <label class="form-check-label" for="inlineRadio3">2º Ciclo Médio II</label>
                            </div>
                        </div>


                        <div class='form-row'>
                            <div class="col-md-6 col-lg-6">
                                <label for="cidade_eetad">Cidade*</label>
                                @php
                                $cidade_eetad = old('cidade_eetad')?old('cidade_eetad'):$matricula->cidade_eetad;
                                @endphp
                                <select name="cidade_eetad" class="form-control" required id="cidade_eetad" {{($matricula->status == "APROVADA")?"disabled":""}} >
                                    <option {{($cidade_eetad == "" ? "selected":"") }}></option>
                                    <option {{($cidade_eetad == "Coronel Fabriciano" ? "selected":"") }}>Coronel Fabriciano</option>
                                    <option {{($cidade_eetad == "Ipatinga" ? "selected":"") }}>Ipatinga</option>
                                    <option {{($cidade_eetad == "Santana do Paraíso" ? "selected":"") }}>Santana do Paraíso</option>
                                    <!--                                    <option>4</option>
                                                                        <option>5</option>-->
                                </select>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="estado">Estado</label>
                                <input name="estado" id='estado'  class="form-control" required type="text" placeholder="MG" value="MG" readonly>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="nucleo">Núcleo nº</label>
                                <input name='nucleo' id='nucleo'  class="form-control" type="text" placeholder="0325" value="0325" readonly>
                            </div>
                            <!--                            <div class="col-md-2 col-lg-2">
                                                            <label for="turma">Turma/Subnucleo</label>
                                                            <input name='turma' id='turma' class="form-control" type="text" placeholder="" value="{{old('turma')}}"  >
                                                        </div>-->
                        </div>
                        <hr>
                        <h4>Dados do Candidato</h4>

                        <div class="form-row">
                            <div class="col-md-12 col-lg-12">
                                <label for="nome">Nome Completo*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}} value="{{old('nome')?old('nome'):$matricula->nome}}"  class="form-control" required id="nome" name="nome" placeholder="">
                            </div>
                            <!--                            <div class="col-md-2 col-lg-2">
                                                            <label for="matricula">Matrícula</label>
                                                            <input type="text" class="form-control" value="{{old('matricula')}}" id="matricula" name="matricula" placeholder="">
                                                        </div>-->
                        </div>
                        <div class="form-row">
                            <div class="col-md-8 col-lg-8">
                                <label for="end">Endereço*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="end" name="end" required value="{{old('end')?old('end'):$matricula->end}}" placeholder="">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="num">Num.*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="num" name="num" required placeholder="" value="{{old('num')?old('num'):$matricula->num}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="bairro">Bairro*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="bairro" name="bairro" required placeholder="" value="{{old('bairro')?old('bairro'):$matricula->bairro}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-5 col-lg-5">
                                <label for="cidade">Cidade*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="cidade" name="cidade" required placeholder="" value="{{old('cidade')?old('cidade'):$matricula->cidade}}">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="uf">UF*</label>
                                <input type="text" maxlength="2" size="2" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="uf" name="uf" placeholder="" required value="{{old('uf')?old('uf'):$matricula->uf}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="cep">CEP*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control form cep" id="cep" name="cep" placeholder="" required value="{{old('cep')?old('cep'):$matricula->cep}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="tel">Telefone</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control form tel" id="tel" name="tel" placeholder=""  value="{{old('tel')?old('tel'):$matricula->tel}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="dtnasc">Dt. Nascimento*</label>
                                <input type="date" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="dtnasc" name="dtnasc" placeholder="" required value="{{old('dtnasc')?old('dtnasc'):$matricula->dtnasc->format('Y-m-d')}}">
                            </div>
                            <div class="col-md-7 col-lg-7">
                                <label for="cidnasc">Cidade Nascimento*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="cidnasc" name="cidnasc" placeholder="" required value="{{old('cidnasc')?old('cidnasc'):$matricula->cidnasc}}">
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <label for="ufnasc">UF*</label>
                                <input type="text" maxlength="2" size="2" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="ufnasc" name="ufnasc" placeholder="" required value="{{old('ufnasc')?old('ufnasc'):$matricula->ufnasc}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-3 col-lg-3">
                                <label for="nacionalidade">Nacionalidade*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="nacionalidade" name="nacionalidade" required placeholder="" value="{{old('nacionalidade')?old('nacionalidade'):$matricula->nacionalidade}}">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="estcivil">Est. Civil*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="estcivil" name="estcivil" placeholder="" required value="{{old('estcivil')?old('estcivil'):$matricula->estcivil}}">
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <label for="email">Email</label>
                                <input type="email" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="email" name="email" placeholder=""  value="{{old('email')?old('email'):$matricula->email}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-5 col-lg-5">
                                <label for="identidade">RG*</label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}}  class="form-control" id="identidade" name="identidade" required placeholder="" value="{{old('identidade')?old('identidade'):$matricula->identidade}}">
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <label for="cpf">CPF*</label>
                                <input type="text"  {{($matricula->status == "APROVADA")?"disabled":""}} class="form-control form cpf" id="cpf" name="cpf" placeholder="" required value="{{old('cpf')?old('cpf'):$matricula->cpf}}">
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <label for="sexo">Sexo*</label>
                                @php
                                $sexo = old('sexo')?old('sexo'):$matricula->sexo;
                                @endphp
                                <div id='sexo'>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="sexo" id="inlineRadio1" required value="Feminino" {{($sexo=='Feminino')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio1">F</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="sexo" id="inlineRadio2" required value="Masculino"  {{($sexo=='Masculino')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio2">M</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-12 col-lg-6">
                                <label for="igreja">Igreja onde é congregado*</label>
                                <input type="text" class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="igreja" name="igreja" placeholder="" required  value="{{old('igreja')?old('igreja'):$matricula->igreja}}">
                            </div>


                            <div class="col-md-12 col-lg-6">
                                <label for="igreja">Começar em qual livro?</label>
                                <input type="text" class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="livro" name="livro" placeholder=""   value="{{old('livro')?old('livro'):$matricula->livro}}">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-3 col-lg-3">
                                <label for="cargo">Exerce cargo na igreja?*</label>
                                <div id='cargo'>
                                    @php
                                    $cargo = old('cargo')?old('cargo'):$matricula->cargo;
                                    @endphp
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="cargo" id="inlineRadio1" required value="Sim" {{($cargo=='Sim')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="cargo" id="inlineRadio2" required value="Não" {{($cargo=='Não')? 'checked':''}} >
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <label for="igreja">Qual?</label>
                                <input type="text"  class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="igreja" name="qualcargo" placeholder="" value="{{old('qualcargo')?old('qualcargo'):$matricula->qualcargo}}">
                            </div>

                        </div>
                        <div class="form-row">

                            <div class="col-md-6 col-lg-6">
                                <label for="escolaridade">Escolaridade*</label>
                                <input type="text" class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="escolaridade" name="escolaridade" required value="{{old('escolaridade')?old('escolaridade'):$matricula->escolaridade}}" placeholder="" >
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <label for="profissao">Profissão*</label>
                                <input type="text" class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="profissao" name="profissao" required placeholder="" value="{{old('profissao')?old('profissao'):$matricula->profissao}}" >
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="formacaoteologica">Possui formação teológica?*</label>
                                <div id='formacaoteologica'>
                                    @php
                                    $formacaoteologica = old('formacaoteologica')?old('formacaoteologica'):$matricula->formacaoteologica;
                                    @endphp
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="formacaoteologica" required id="inlineRadio1" value="Sim" {{($formacaoteologica=='Sim')? 'checked':''}}  >
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="formacaoteologica" required id="inlineRadio2" value="Não" {{($formacaoteologica=='Não')? 'checked':''}}  >
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-8">
                                <label for="escola">Escola</label>
                                <input type="text" class="form-control" {{($matricula->status == "APROVADA")?"disabled":""}} id="escola" name="escola" placeholder="" value="{{old('escola')?old('escola'):$matricula->escola}}" >
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                                <label for="taxamatricula">Pagamento taxa de Matrícula? </label>
                                <div id='taxamatricula'>
                                    @php
                                    $txmatricula = old('txmatricula')?old('txmatricula'):$matricula->txmatricula;
                                    @endphp
                                    <div class="form-check ">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="txmatricula" id="inlineRadio1" value="Sim" {{($txmatricula=='Sim')? 'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio1">Sim</label>
                                    </div>
                                    <br><br>
                                    <div class="form-check ">
                                        <input class="form-check-input" {{($matricula->status == "APROVADA")?"disabled":""}} type="radio" name="txmatricula" id="inlineRadio2" value="Não" {{($txmatricula=='Não')? 'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio2">Não</label>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-8 col-lg-8">
                                <label class="my-1 mr-2" for="dttaxamatricula">Data: </label>
                                @php
                               // dd($matricula->dttaxamatricula);
                                @endphp
                                <input type="date" {{($matricula->status == "APROVADA")?"disabled":""}} class="form-control form-control-sm  my-1 mr-sm-2" id="dttaxamatricula" name="dttaxamatricula" placeholder="" value="{{old('dttaxamatricula')?old('dttaxamatricula'):($matricula->dttaxamatricula!=null?$matricula->dttaxamatricula->format('Y-m-d'):"")}}" >

                                <label class="my-1 mr-2" for="motivotxmatricula">Motivo: </label>
                                <input type="text" {{($matricula->status == "APROVADA")?"disabled":""}} class="form-control form-control-sm my-1 mr-sm-2" id="motivotxmatricula" name="motivotxmatricula" placeholder="" value="{{old('motivotxmatricula')?old('motivotxmatricula'):$matricula->motivotxmatricula}}" >
                            </div>
                        </div>
                        <br>
                        <div class="form-inline">
                            <div class="form-check col-md-7 col-lg-7">
<!--                                <input class="form-check-input" type="checkbox" value="Sim" name="imprimir" id="imprimir" {{old('imprimir') ? 'checked' :''}} >
                                <label class="form-check-label" for="invalidCheck">
                                    Imprimir Ficha, Normas e Orientações.
                                </label>-->

                            </div>
                             @if ($matricula->status == "APROVADA")
                             <button class="btn btn-secondary  col-md-8 col-lg-8 disabled " type="" disabled>Para alterar Dados desse Aluno entre em Contato com a Secretaria</button>
                            @else
                            <button class="btn btn-primary col-md-4 col-lg-4" type="submit">Editar e Enviar à Secretaria</button>
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
