<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Essas credenciais não correspondem aos nossos registros.',
    'throttle' => 'Muitas tentativas de login. Tente novamente em :seconds segundos.',
    'email' => 'E-mail',
    'Password' => 'Senha',
    'Remember' => 'Lembrar-me',
    'Login' => 'Entrar',
    'Forgot_password' => "Esqueceu sua Senha?",
    'Logout' => 'Sair'

];
