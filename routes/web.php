<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('website.home');
//});

Route::get('/', 'IndexController@getHome')->name('index');
Route::get('/monitores', 'IndexController@getMonitores')->name('monitores');
Route::get('/fotos', 'IndexController@getFotos')->name('fotos');
Route::get('/contato', 'IndexController@contato')->name('contato');
Route::post('/contato', 'IndexController@postContato')->name('contato.send');
Route::get('/quizbiblico', 'QuizBiblicoController@index')->name('quizbiblico');
Route::get('/gerar-quiz', 'QuizBiblicoController@gerar_quiz')->name('quiz.gerar');
Route::post('/quiz-enviar', 'QuizBiblicoController@enviar_quiz')->name('quiz.enviar');






//Auth::routes();
Auth::routes(['register' => false]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/matricula', 'HomeController@form_matricula')->name('form.matricula');
Route::get('/matricula/editar', 'HomeController@editar_matricula')->name('editar.matricula');
Route::post('/enviar-matricula', 'HomeController@enviar_matricula')->name('matricula');
Route::post('/editar-matricula', 'HomeController@update_matricula')->name('update.matricula');


Route::group([
    'prefix' => 'aluno', 'as' => 'aluno.',
   
    'namespace' => 'Aluno',
    ], function () {
    
//    Route::get('/redirect', 'SocialAuthFacebookController@redirect');
//Route::get('/callback', 'SocialAuthFacebookController@callback');
//
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
//Route::get('login/facebook/fblogin', 'Auth\LoginController@fblogin');
Route::get('login/google', 'Auth\LoginController@redirectToProviderGoogle')->name('google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');

    Route::get('/', 'Auth\LoginController@showLoginForm')->name('index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dados', 'HomeController@dados_aluno')->name('dados');
    Route::post('/matricula', 'HomeController@enviar_matricula')->name('matricula');
    Route::post('/matricula/editar', 'HomeController@editar_matricula')->name('editar');
// Authentication Routes…
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login')->name('login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');
    
// Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');    

// Password Reset Routes…
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
});





