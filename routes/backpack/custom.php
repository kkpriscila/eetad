<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.


Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace' => 'App\Http\Controllers\Admin',
        ], function () { // custom admin routes
    Route::get('/dashboard', 'DashboardController@index');
    CRUD::resource('configuracao', 'ConfiguracoesCrudController');
    CRUD::resource('fotos', 'FotosCrudController');
    CRUD::resource('monitores', 'MonitoresCrudController');
    CRUD::resource('user', 'UserCrudController');
    CRUD::resource('subnucleos', 'SubNucleoCrudController');
    CRUD::resource('categoria', 'CategoriaQuizCrudController');
    CRUD::resource('aluno', 'AlunoCrudController');
    CRUD::resource('matricula', 'MatriculaCrudController');
    CRUD::resource('matricula-reprovada', 'MatriculaNaoAprovadaCrudController');
    CRUD::resource('matricula-aprovada', 'MatriculaAprovadaCrudController');
    CRUD::resource('pergunta', 'PerguntaQuizCrudController');
    Route::group(['prefix' => 'pergunta/{pergunta}'], function() {
        CRUD::resource('resposta', 'PerguntaRespostaQuizCrudController');
    });
    CRUD::resource('pergunta_usuario', 'PerguntaQuizUsuarioCrudController');
    Route::group(['prefix' => 'pergunta_usuario/{pergunta}'], function() {
        CRUD::resource('resposta', 'PerguntaRespostaQuizCrudController');
    });
}); // this should be the absolute last line of this file
