/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

    'use strict';

//    console.log("passou aqui");



    // -------------
    // - PIE CHART -
    // -------------
    // Get context with jQuery - using jQuery's .get() method.
    //TIPOS PROPOSIÇÕES
//    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChartCanvas = document.getElementById('pieChart').getContext('2d');
    var pieChart = new Chart(pieChartCanvas);
//TIPOS DE  LICITAÇÕES
    var pieChartCanvas2 = $('#pieChart2').get(0).getContext('2d');
    var pieChart2 = new Chart(pieChartCanvas2);
    
    // var PieTpProposicoes //sendo carregada na view gestao_a_vista.blade
    
    var pieOptions = {
        // Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: false,
        // String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        // Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        // Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 30, // This is 0 for Pie charts
        // Number - Amount of animation steps
        animationSteps: 100,
        // String - Animation easing effect
        animationEasing: 'easeOutBounce',
        // Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        // Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: true,
        // Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        // String - A legend template
        legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
        // String - A tooltip template
        tooltipTemplate: '<%=value %> <%=label%>',
         
         
        
    };
    // Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    //TIPOS PROPOSIÇÕES
   
    pieChart.Doughnut(PieSexo, pieOptions);
    pieChart2.Doughnut(PieIdade, pieOptions);
    //TIPOS DE  LICITAÇÕES
//    pieChart2.Doughnut(PieTpProposicoes, pieOptions);
    // -----------------
    // - END PIE CHART -
    // -----------------


    //-------------
    //- BAR CHART -
    //-------------
    //DADO da variável abaixo vindo da view gestao-a-vista
   

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChart = new Chart(barChartCanvas)


    var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
//        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to make the chart responsive
       tooltipTemplate: '<%=value %> <%=label%>',
        responsive: true,
        maintainAspectRatio: true,
//        legend: {
//            display: true,
//            position: 'top'
//        }
    };
// barChartOptions.legend.display = true;
//    barChartOptions.datasetFill = false

    barChart.Bar(barChartData, barChartOptions)


   
});
