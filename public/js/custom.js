//﻿
///*=============================================================
//    Authour URI: www.binarytheme.com
//    License: Commons Attribution 3.0
//
//    http://creativecommons.org/licenses/by/3.0/
//
//    100% Free To use For Personal And Commercial Use.
//    IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US
//   
//    ========================================================  */
//

function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});


/* MÁSCARAS SITES */
jQuery(function($){
    
//    alert('teste');
    
   $(".data").mask("99/99/9999");
   $(".data-hora").mask("99/99/9999 99:99");    
   $(".tel").mask("(99) 99999-9999");
   $(".telfixo").mask("(99) 9999-9999");
   $(".cpf").mask("999.999.999-99"); 
   $(".cnpj").mask("99.999.999/9999-99"); 
   $(".cep").mask("99999-999");   
});


