
/*--------loader script-----------*/
$(function () {
    var loading = $('#loadbar').hide();
    $(document)
            .ajaxStart(function () {
                loading.show();
            }).ajaxStop(function () {
        loading.hide();
    });

    var questionNo = 0;
    var correctCount = 0;
//    var q = [
//        {'Q':'How do you write "Hello World" in an alert box?', 'A':2,'C':['msg("Hello World");','alert("Hello World");','alertBox("Hello World");']},
//        {'Q':'How do you create a function in JavaScript?', 'A':3,'C':['function:myFunction()','function = myFunction()','function myFunction()']},
//        {'Q':'How to write an IF statement in JavaScript?', 'A':1,'C':['if (i == 5)','if i = 5 then','if i == 5 then']},
//        {'Q':'How does a FOR loop start?', 'A':2,'C':['for (i = 0; i <= 5)','for (i = 0; i <= 5; i++)','for i = 1 to 5']},
//        {'Q':'What is the correct way to write a JavaScript array?', 'A':3,'C':['var colors = "red", "green", "blue"','var colors = (1:"red", 2:"green", 3:"blue")','var colors = ["red", "green", "blue"]']}
//    ];


    $(document.body).on('click', "label.element-animation", function (e) {
        //ripple start
        var parent, ink, d, x, y;
        parent = $(this);
        if (parent.find(".ink").length == 0)
            parent.prepend("<span class='ink'></span>");

        ink = parent.find(".ink");
        ink.removeClass("animate");

        if (!ink.height() && !ink.width())
        {
            d = Math.max(parent.outerWidth(), parent.outerHeight());
            ink.css({height: "100px", width: "100px"});
        }

        x = e.pageX - parent.offset().left - ink.width() / 2;
        y = e.pageY - parent.offset().top - ink.height() / 2;

        ink.css({top: y + 'px', left: x + 'px'}).addClass("animate");
        //ripple end

        var choice = $(this).parent().find('input:radio').val();
//        console.log(choice);
        var anscheck = $(this).checking(questionNo, choice);//$( "#answer" ).html(  );      
        q[questionNo].UC = $(this).parent().find('label').html();
        if (anscheck) {
            correctCount++;
            q[questionNo].result = "Certo";
        } else {
            q[questionNo].result = "Errado";
        }
//        console.log("CorrectCount:" + correctCount);
        setTimeout(function () {
            $('#loadbar').show();
            $('#quiz').fadeOut();
            questionNo++;
            if ((questionNo + 1) > q.length) {
//                alert("Quiz Finalizado, Veja seu resuldo e receba-o por email!");
                $('label.element-animation').unbind('click');
                setTimeout(function () {
                    var toAppend = '';
                    $.each(q, function (i, a) {
                        if (a.result === 'Certo') {
                            toAppend += '<tr>'
                            toAppend += '<td>' + a.Q + '</td>';
                            toAppend += '<td class="text-success">' + a.UC + '</td>';
                            toAppend += '<td class="text-success">' + a.result + '</td>';
                            toAppend += '</tr>'
                        } else {
                             toAppend += '<tr>'
                            toAppend += '<td>' + a.Q + '</td>';
                            toAppend += '<td class="text-danger">' + a.UC + '</td>';
                            toAppend += '<td class="text-danger">' + a.result + '</td>';
                            toAppend += '</tr>'
                        }
                    
                    });
                    $('#quizResult').html(toAppend);
                    $('#totalCorrect').html(+correctCount);
                    $('#quizResult').show();
                    $('#loadbar').fadeOut();
                    $('#result-of-question').show();
                    $('#graph-result').show();
//                    chartMake();
                    ///executar rota de envio por email no php

//                $.ajaxSetup({
//                    headers: {
//                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                    }
//                });

                    $.ajax({
                        type: 'POST',
                        url: '/quiz-enviar',
                        data: $("#form").serialize() + '&toAppend=' + toAppend + '&totalCorrect=' + correctCount,
//                        data: {
//                            name: $('#name').val(),
//                            email: $('#email').val(),
//                            telefone: $('#telefone').val(),
//                            nivel: $('#nivel').val(),
//                            categoria: $('#categoria').val(),
//                            toAppend: toAppend,
//                            totalCorrect: correctCount
//                        },
//                        contentType: "json",
//                        processData: false,
                        success: function (data) {
//                        alert("teste");
//                            alert(data.success);
                            console.log(data.success);
                        }
                    });



                }, 1000);


            } else {
                $('#qid').html(" Questão " + (questionNo + 1) + " de " + q.length + " ");
                $('input:radio').prop('checked', false);
                setTimeout(function () {
                    $('#quiz').show();
                    $('#loadbar').fadeOut();
                }, 1000);

                $('#question').html(q[questionNo].Q);
//                console.log(q[questionNo].C.length);

                $.each(q[questionNo].C, function (i, a) {
                    $('#' + (i + 1) + '-li').show();
                    $($('#' + (i + 1) + '-option').parent().find('label')).html(q[questionNo].C[(i)]);



                });
//                   console.log("I:"+q[questionNo].C.length);
                if (q[questionNo].C.length < 5) {
                    for (var i = q[questionNo].C.length; i < 5; i++) {
                        $('#' + (i + 1) + '-li').hide();
                    }
                }

//                $($('#1-option').parent().find('label')).html(q[questionNo].C[0]);
//                $($('#2-option').parent().find('label')).html(q[questionNo].C[1]);
//                $($('#3-option').parent().find('label')).html(q[questionNo].C[2]);
//                               
//                
//                $($('#4-option').parent().find('label')).html(q[questionNo].C[3]);
//                $($('#5-option').parent().find('label')).html(q[questionNo].C[4]);
            }
        }, 1000);
    });


    $.fn.checking = function (qstn, ck) {
        var ans = q[questionNo].A;
        if (ck != ans)
            return false;
        else
            return true;
    };

// chartMake();
//    function chartMake() {
//
//        var chart = AmCharts.makeChart("chartdiv",
//                {
//                    "type": "serial",
//                    "theme": "dark",
//                    "dataProvider": [{
//                            "name": "Correct",
//                            "points": correctCount,
//                            "color": "#00FF00",
//                            "bullet": "http://i2.wp.com/img2.wikia.nocookie.net/__cb20131006005440/strategy-empires/images/8/8e/Check_mark_green.png?w=250"
//                        }, {
//                            "name": "Incorrect",
//                            "points": q.length - correctCount,
//                            "color": "red",
//                            "bullet": "http://4vector.com/i/free-vector-x-wrong-cross-no-clip-art_103115_X_Wrong_Cross_No_clip_art_medium.png"
//                        }],
//                    "valueAxes": [{
//                            "maximum": q.length,
//                            "minimum": 0,
//                            "axisAlpha": 0,
//                            "dashLength": 4,
//                            "position": "left"
//                        }],
//                    "startDuration": 1,
//                    "graphs": [{
//                            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
//                            "bulletOffset": 10,
//                            "bulletSize": 52,
//                            "colorField": "color",
//                            "cornerRadiusTop": 8,
//                            "customBulletField": "bullet",
//                            "fillAlphas": 0.8,
//                            "lineAlpha": 0,
//                            "type": "column",
//                            "valueField": "points"
//                        }],
//                    "marginTop": 0,
//                    "marginRight": 0,
//                    "marginLeft": 0,
//                    "marginBottom": 0,
//                    "autoMargins": false,
//                    "categoryField": "name",
//                    "categoryAxis": {
//                        "axisAlpha": 0,
//                        "gridAlpha": 0,
//                        "inside": true,
//                        "tickLength": 0
//                    }
//                });
//    }
});	