<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\CRUD\CrudTrait;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;


class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = [
		'dtexpira'
	];

    protected $fillable = [
        'name', 'email', 'password','tipo','dtexpira'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new ResetPasswordNotification($token));
  }
   public function active_desactive($crud = false)
    {
                   
        //return '<a class="btn btn-xs btn-default" target="_blank" href="'.route("news.show",["id" => $this->id, "slug" => str_slug($this->subject)] ).'" data-toggle="tooltip" title="Visualizar Notícia no site."><i class="fa fa-search"></i> Ver no Site</a>';
//        return '<a href="'.route('news.exibir',['id' => $this->id, 'slug' => str_slug($this->subject)] ).'" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-search"></i> Ver no site</a> ';
//        return '<a href="#" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-user-check"></i> Ativar</a> ';
        return '<a href="#" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-user-slash"></i> Desativar</a> ';
        
    }
}
