<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubnucleoPasswordReset
 * 
 * @property string $email
 * @property string $token
 * @property Carbon $created_at
 *
 * @package App\Models
 */
class SubnucleoPasswordReset extends Model
{
	protected $table = 'subnucleo_password_resets';
	public $incrementing = false;
	public $timestamps = false;

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'email',
		'token'
	];
}
