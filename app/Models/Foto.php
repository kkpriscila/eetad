<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class Foto
 * 
 * @property int $id
 * @property string $imagem
 * @property string $imagemp
 * @property string $nmfoto
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Foto extends Model
{
    use CrudTrait;
	protected $table = 'fotos';

	protected $fillable = [
		'imagem',
		'imagemp',
		'nmfoto',
		'home',
	];
        
          public function setImagemAttribute($value) {
//        if (isset($value) && $value != "") {
        $attribute_name = "imagem";


        $disk = "uploads";
        $destination_path = "/uploads/fotos";
        //$destination_path_mobile = "/mobile/img_vereadores";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);



        //IMAGEMP REDIMENSIONADA
        $filenamewithextension = $value->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $value->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename . '_' . time() . '.' . $extension;

        //Upload File
        $value->storeAs($destination_path . '/thumbs/', $filenametostore, $disk);

        $this->imagemp = $destination_path . '/thumbs/' . $filenametostore;
        // dd($this->imagemp);
        //We will write TinyPNG Compress Image Code Here
        //TinyPNG Compress Image

        $filepath = public_path($this->imagem);
        $filepathp = public_path($this->imagemp);
        try {
            \Tinify\setKey("rXHP8HR5G8SB53zQrXGLJfZkHjVHJZ8T");
            //READ: 825Rgm2Xar2OlYMBM2Pf2QiteYPdAvjP
            $sourcep = \Tinify\fromFile($filepathp);
            $source = \Tinify\fromFile($filepath);
           
            $resized = $source->resize(array(
                "method" => "thumb",
                "width" => 650,
                "height" => 350
            ));
            $resized->toFile($filepathp);
            $source->toFile($filepath);
        } catch (\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ClientException $e) {
            // Check your source image and request options.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            return redirect('images/create')->with('error', $e->getMessage());
        }
    }
}
