<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class Configuraco
 * 
 * @property int $id
 * @property string $nmescola
 * @property string $logo
 * @property string $endereco
 * @property string $telefone
 * @property string $email
 * @property string $txtsobre
 * @property string $txtcontato
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Configuraco extends Model
{
    use CrudTrait;
	protected $table = 'configuracoes';

	protected $fillable = [
		'nmescola',
		'logo',
		'endereco',
		'telefone',
		'email',
		'whatsapp',
		'facebook',
		'txtsobre',
		'txtcontato'
	];
                public function setLogoAttribute($value) {
        //dd($value);
        $attribute_name = "logo";

        $disk = "uploads";
        $destination_path = "/uploads";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

   
//         public function getLogoAttribute($value) {
//        //VERIFICAR SE O ENDEREÇO DA IMAGEM ESTÁ COMPLETO, SENAO APONTAR PARA PASTA UPLOADS/IMAGES/VEREADOR
//
//        if (isset($value) && $value != "") {
//
//
//            $termo = 'uploads';
//
//            $pattern = '/' . $termo . '/'; //Padrão a ser encontrado na string $tags
//            if (!(preg_match($pattern, $value))) {
//                $value = "/uploads/config/" . $value;
//            } else {
//                $value = "/" . $value;
//            }
//
//
//          
//            //dd($value);
//        }
//        return $value;
//    }
}
