<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\CRUD\CrudTrait;
//use Backpack\Base\app\Notifications\ResetPasswordNotification;
use Illuminate\Auth\Notifications\ResetPassword;

//use Illuminate\Http\Request;

/**
 * Class Matricula
 * 
 * @property int $id
 * @property string $subnucleo
 * @property string $resp_subnucleo
 * @property string $email_subnucleo
 * @property string $ciclo
 * @property string $cidade_eetad
 * @property string $estado
 * @property string $nucleo
 * @property string $nome
 * @property string $end
 * @property string $num
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $cep
 * @property string $tel
 * @property Carbon $dtnasc
 * @property string $cidnasc
 * @property string $ufnasc
 * @property string $nacionalidade
 * @property string $estcivil
 * @property string $email
 * @property string $identidade
 * @property string $cpf
 * @property string $sexo
 * @property string $igreja
 * @property string $cargo
 * @property string $qualcargo
 * @property string $escolaridade
 * @property string $profissao
 * @property string $formacaoteologica
 * @property string $escola
 * @property string $dttaxamatricula
 * @property string $motivotxmatricula
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $livro
 *
 * @package App\Models
 */
class Matricula extends Authenticatable {

    use Notifiable;

use CrudTrait;

    protected $table = 'matricula';
    protected $dates = [
        'dtnasc',
        'dttaxamatricula'
    ];
    protected $fillable = [
        'subnucleo',
        'resp_subnucleo',
        'email_subnucleo',
        'ciclo',
        'cidade_eetad',
        'estado',
        'nucleo',
        'nome',
        'end',
        'num',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'tel',
        'dtnasc',
        'cidnasc',
        'ufnasc',
        'nacionalidade',
        'estcivil',
        'email',
        'identidade',
        'cpf',
        'sexo',
        'igreja',
        'cargo',
        'qualcargo',
        'escolaridade',
        'profissao',
        'formacaoteologica',
        'escola',
        'dttaxamatricula',
        'motivotxmatricula',
        'livro',
        'password',
        'provider_id',
        'avatar',
        'status',
        'provider',
        'subnucleo_id',
        'justifica_status',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPassword($token));
    }

    public function getFullSubnucleoAttribute() {
        return "{$this->id} - {$this->nome}";
    }

    public function subnucleo() {

        return $this->belongsTo(Subnucleo::class);
    }

    public function aluno() {

        return $this->belongsTo(Aluno::class);
    }

    public static function getMatriculasPendente() {

        $matriculas = self::where('status', 'PENDENTE')
                ->OrWhere('status', 'ERRO')
                ->get();

        return $matriculas;
    }

    public function setAvatarAttribute($value) {
         // dd($value);

        if (isset($value) && $value != "" && !is_null($value)) {

            if (is_file($value)) {
                $attribute_name = "avatar";


                $disk = "uploads";
                $destination_path = "/uploads/avatar";
                //$destination_path_mobile = "/mobile/img_vereadores";

                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

                //dd($value->getClientOriginalName());
                //IMAGEMP REDIMENSIONADA
                $filenamewithextension = $value->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $value->getClientOriginalExtension();

                //filename to store
                $filenametostore = $filename . '_' . time() . '.' . $extension;

//            $this->imagemp = $destination_path . '/thumbs/' . $filenametostore;
                //We will write TinyPNG Compress Image Code Here
                //TinyPNG Compress Image

                $filepath = public_path($this->avatar);

                try {
                    \Tinify\setKey("rXHP8HR5G8SB53zQrXGLJfZkHjVHJZ8T");
                    //READ: 825Rgm2Xar2OlYMBM2Pf2QiteYPdAvjP
//                $sourcep = \Tinify\fromFile($filepathp);
                    $source = \Tinify\fromFile($filepath);

                    $resized = $source->resize(array(
                        "method" => "thumb",
                        "width" => 500,
//                    "height" => 350
                    ));
                    $resized->toFile($filepath);
                    $source->toFile($filepath);
                } catch (\Tinify\AccountException $e) {
                    // Verify your API key and account limit.
                    return redirect('aluno/matricula')->with('error', $e->getMessage());
                } catch (\Tinify\ClientException $e) {
                    // Check your source image and request options.
                    return redirect('aluno/matricula')->with('error', $e->getMessage());
                } catch (\Tinify\ServerException $e) {
                    // Temporary issue with the Tinify API.
                    return redirect('aluno/matricula')->with('error', $e->getMessage());
                } catch (\Tinify\ConnectionException $e) {
                    // A network connection error occurred.
                    return redirect('aluno/matricula')->with('error', $e->getMessage());
                } catch (Exception $e) {
                    // Something else went wrong, unrelated to the Tinify API.
                    return redirect('images/create')->with('error', $e->getMessage());
                }
            } else {
                
//                $this->avatar = $value;
                $this->attributes['avatar'] = $value;
//                dd('else', $this->avatar);
            }
        }

//        dd($this->avatar);
    }

}
