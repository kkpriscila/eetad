<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Auth\Notifications\ResetPassword;

/**
 * Class Subnucleo
 * 
 * @property int $id
 * @property string $nome
 * @property string $responsável
 * @property string $txtsobre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Subnucleo extends Authenticatable {

     use Notifiable;
    use CrudTrait;

    protected $table = 'subnucleo';
    protected $fillable = [
        'nome',
        'responsável',
        'endereco',
        'email',
        'password',
        
    ];
     protected $hidden = [
        'password', 'remember_token',
    ];
     protected $casts = [
        'email_verified_at' => 'datetime',
    ];
      public function sendPasswordResetNotification($token)
  {
      $this->notify(new ResetPassword($token));
  }
  public function getFullSubnucleoAttribute() {
        return "{$this->id} - {$this->nome}";
    }
    public function matriculas() {
        return $this->hasMany(Matricula::class);
               
    }
    public function aunos() {
        return $this->hasMany(Aluno::class);
               
    }

}
