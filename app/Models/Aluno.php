<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use File;

/**
 * Class Aluno
 * 
 * @property int $id
 * @property string $nome
 * @property string $end
 * @property string $num
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $cep
 * @property string $tel
 * @property Carbon $dtnasc
 * @property string $cidnasc
 * @property string $ufnasc
 * @property string $nacionalidade
 * @property string $estcivil
 * @property string $email
 * @property string $identidade
 * @property string $cpf
 * @property string $sexo
 * @property string $igreja
 * @property string $cargo
 * @property string $qualcargo
 * @property string $escolaridade
 * @property string $profissao
 * @property string $formacaoteologica
 * @property string $escola
 * @property string $livro
 * @property string $status
 * @property string $cidade_eetad
 * @property int $subnucleo_id
 * @property int $matricula_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $ciclo
 * @property string $rematricula
 * @property Carbon $dtmatricula
 * 
 * @property Matricula $matricula
 * @property Subnucleo $subnucleo
 *
 * @package App\Models
 */
class Aluno extends Model {

    use CrudTrait;

    protected $table = 'aluno';
    protected $casts = [
        'subnucleo_id' => 'int',
        'matricula_id' => 'int'
    ];
    protected $dates = [
        'dtnasc',
        'dtmatricula'
    ];
    protected $fillable = [
        'nome',
        'end',
        'num',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'tel',
        'dtnasc',
        'cidnasc',
        'ufnasc',
        'nacionalidade',
        'estcivil',
        'email',
        'identidade',
        'cpf',
        'sexo',
        'igreja',
        'cargo',
        'qualcargo',
        'escolaridade',
        'profissao',
        'formacaoteologica',
        'escola',
        'livro',
        'status',
        'cidade_eetad',
        'subnucleo_id',
        'matricula_id',
        'ciclo',
        'rematricula',
        'foto',
        'created_at',
        'updated_at',
        'dtmatricula'
    ];

    public function matricula() {
        return $this->belongsTo(Matricula::class);
    }

    public function subnucleo() {
        return $this->belongsTo(Subnucleo::class);
    }

    public function setFotoAttribute($value) {
        //dd($value);
        $attribute_name = "foto";
        $disk = "uploads";
        $destination_path = "/uploads/alunos";

        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }
        if (is_file($value)) {

            // if a base64 was sent, store it in the db
            if (starts_with($value, 'data:image')) {
                // 0. Make the image
                $image = \Image::make($value)->encode('jpg', 90);
                // 1. Generate a filename.
                $filename = md5($value . time()) . '.jpg';
                // 2. Store the image on disk.
                \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
                // 3. Save the path to the database
                $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
            }
        } else {
               
            $this->attributes['foto'] = $value;
//            $fileContents = file_get_contents($value);
//            
//            $filename =  md5($this->id. time());
//           
//            File::put($destination_path .'/' . $filename . ".jpg", $fileContents);
//             dd($filename);
//            $this->attributes[$attribute_name] = $destination_path .'/' . $filename . ".jpg";
//                dd('else', $this->avatar);
        }
        //  dd($this->foto);
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($obj) {
            \Storage::disk('uploads')->delete($obj->image);
        });
    }

}
