<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class CategoriaQuiz
 * 
 * @property int $id
 * @property string $categoria
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class QuizCategoria extends Model {

    use CrudTrait;

    protected $table = 'quiz_categoria';
    protected $fillable = [
        'categoria'
    ];

    public function getFullCategoriaAttribute() {
        return "{$this->id} - {$this->categoria}";
    }
     public function perguntas() {
        return $this->hasMany(QuizPergunta::class, 'id','categoria_id');
               
    }

}
