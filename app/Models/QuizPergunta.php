<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class QuizPergunta
 * 
 * @property int $id
 * @property string $pergunta
 * @property string $nivel
 * @property int $categoria_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class QuizPergunta extends Model {

    use CrudTrait;

    protected $table = 'quiz_perguntas';
    protected $casts = [
        'categoria_id' => 'int'
    ];
    protected $fillable = [
        'pergunta',
        'nivel',
        'categoria_id',
        'created_user',
        'updated_user',
    ];

    public function categoria() {
       
        return $this->belongsTo(QuizCategoria::class);
    }
    public function usuario() {
       
        return $this->belongsTo(\App\User::class,'created_user','id');
    }
     public function respostas() {
        return $this->hasMany(QuizResposta::class, 'pergunta_id','id');
    }
     public function getRespostaAttribute() {
          $resp_certa = \App\Models\QuizResposta::where('pergunta_id',$this->id)
                  ->where('certa','S')
                  ->get();
          
          $resposta="";

        if ($resp_certa->count() > 0) {
            foreach ($resp_certa as $r) {

                if($resposta<>""){
                    $resposta.=" - ".$r->resposta;
                }else
                $resposta.=$r->resposta;
            }
        }else{
            $resposta = "";
        }


        return $resposta;
        
     }

}
