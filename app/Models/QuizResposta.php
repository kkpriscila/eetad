<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class QuizResposta
 * 
 * @property int $id
 * @property string $resposta
 * @property string $certa
 * @property int $pergunta_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class QuizResposta extends Model
{
    use CrudTrait;
	protected $table = 'quiz_respostas';

	protected $casts = [
		'pergunta_id' => 'int'
	];

	protected $fillable = [
		'resposta',
		'certa',
		'pergunta_id'
	];
}
