<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class Monitore
 * 
 * @property int $id
 * @property string $nome
 * @property string $titulo
 * @property string $foto
 * @property string $endereco
 * @property string $telefone
 * @property string $email
 * @property string $curricuo
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Monitore extends Model {

    use CrudTrait;

    protected $table = 'monitores';
    protected $fillable = [
        'nome',
        'titulo',
        'foto',
        'endereco',
        'telefone',
        'email',
        'curriculo',
        'dtnasc',
        'tipo',
        'subnucleo_id',
    ];
      public function subnucleo() {
        return $this->belongsTo(Subnucleo::class);
    }

    public function setFotoAttribute($value) {
//        if (isset($value) && $value != "") {
        $attribute_name = "foto";


        $disk = "uploads";
        $destination_path = "/uploads/monitores";
        //$destination_path_mobile = "/mobile/img_vereadores";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        $filepath = public_path($this->foto);
        try {
            \Tinify\setKey("rXHP8HR5G8SB53zQrXGLJfZkHjVHJZ8T");
            //READ: 825Rgm2Xar2OlYMBM2Pf2QiteYPdAvjP
            $source = \Tinify\fromFile($filepath);
            $resized = $source->resize(array(
                "method" => "thumb",
                "width" => 167,
                "height" => 167
            ));
            $resized->toFile($filepath);
        } catch (\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ClientException $e) {
            // Check your source image and request options.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            return redirect('images/create')->with('error', $e->getMessage());
        } catch (Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            return redirect('images/create')->with('error', $e->getMessage());
        }
    }

}
