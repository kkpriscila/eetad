<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Models\Configuraco;
use App\Models\Matricula;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Subnucleo;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:aluno');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $menu = 'Principal';
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
        }

        $user = Auth::user();

        $subnucleos = Subnucleo::get();
//dd($user->subnucleo_id);
        if (isset($user->subnucleo_id) or $user->subnucleo_id != null)
            $subnucleo = Subnucleo::findOrFail($user->subnucleo_id);
        else
            $subnucleo ="";

//          dd ($subnucleo);
        //  dd($user);
//        $matricula = Matricula::

        return view('aluno.home', compact('config', 'user', 'menu', 'subnucleos', 'subnucleo'));
    }

    public function dados_aluno() {
        $menu = 'Dados do Aluno';
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
        }
        $subnucleos = Subnucleo::get();

        $user = Auth::user();
        $subnucleo = Subnucleo::findOrFail($user->subnucleo_id);
//       dd($user);
//        $matricula = Matricula::

        return view('aluno.dados', compact('config', 'user', 'menu', 'subnucleos', 'subnucleo'));
    }

    public function enviar_matricula(Request $request) {
        $menu = 'Principal';

        $emaildestino = "";
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
            $emaildestino = $config->email;
        }

        $user = Auth::user();
        // dd($user, $request);
        $inputs = $request->validate([
            'ciclo' => 'required',
            'cidade_eetad' => 'required',
            'nome' => 'required|min:3',
            'end' => 'required|min:3',
            'num' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
//                'tel' => 'required',
            'dtnasc' => 'required',
            'cidnasc' => 'required',
            'ufnasc' => 'required',
            'nacionalidade' => 'required',
            'estcivil' => 'required',
//                'email' => 'required',
            'identidade' => 'required',
            'cpf' => 'required',
            'sexo' => 'required',
        ]);
        $subnucleos = Subnucleo::get();

        $matricula = Matricula::where('cpf', $request->cpf)->first();
        if ($matricula) {
//            Session::flash('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!');
//            $request->flash();
            
            return redirect()->route('aluno.home')->with('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!') ->withInput();;
//            return view('aluno.home', compact('config', 'user', 'menu', 'subnucleos'));
        } else {

            $user->subnucleo_id = $request->subnucleo_id;
            $subnucleo = Subnucleo::findOrFail($request->subnucleo_id);
            //dd($subnucleo);
            $request['subnucleo'] = $subnucleo->nome;


            $user->subnucleo = $subnucleo->nome;
            $user->resp_subnucleo = $subnucleo->responsável;
            $user->email_subnucleo = $subnucleo->email;
            $user->ciclo = $request->ciclo;
            $user->cidade_eetad = $request->cidade_eetad;
            $user->estado = $request->estado;
            $user->nucleo = $request->nucleo;
            $user->nome = $request->nome;
            $user->end = $request->end;
            $user->num = $request->num;
            $user->bairro = $request->bairro;
            $user->cidade = $request->cidade;
            $user->uf = $request->uf;
            $user->cep = $request->cep;
            $user->tel = $request->tel;
            $user->dtnasc = $request->dtnasc;
            $user->cidnasc = $request->cidnasc;
            $user->ufnasc = $request->ufnasc;
            $user->nacionalidade = $request->nacionalidade;
            $user->estcivil = $request->estcivil;
            $user->email = $request->email;
            $user->identidade = $request->identidade;
            $user->cpf = $request->cpf;
            $user->sexo = $request->sexo;
            $user->igreja = $request->igreja;
            $user->cargo = $request->cargo;
            $user->qualcargo = $request->qualcargo;
            $user->escolaridade = $request->escolaridade;
            $user->profissao = $request->profissao;
            $user->formacaoteologica = $request->formacaoteologica;
            $user->escola = $request->escola;
            $user->avatar = $request->avatar;
            $user->status = 'PENDENTE';
            $user->justifica_status = 'Pagamento - AGUARDANDO ';

            $user->save();


//            $emaildestino = "";
//            $config = Configuraco::limit(1)->get();

              if ($emaildestino != "") {
//                $config = $config[0];
//                $emaildestino = $config->email;


                Mail::send('emails.matricula_aluno', $request->all(), function ($m)use ($request, $emaildestino) {
                    $m->from('faleconosco@eetad0325.com.br', $request->nome . " - " . $request->email);
                    $m->to($emaildestino, 'EETAD Núcleo 0325')->subject('Matrícula Aluno - EETAD 0325 (' . $request->nome . ')');
                });
            }
            // Mensagem de sucesso para ser exibida
//            Session::flash('success', $request->nome . ', sua matrícula foi enviada à secretaria!');
//            $request->flash();
//            return view('aluno.home', compact('config', 'user', 'menu', 'subnucleos'));
            return redirect()->route('aluno.home')->with('success', $request->nome . ', sua matrícula foi enviada à secretaria!') ->withInput();;
        }
    }

    public function editar_matricula(Request $request) {
        $menu = 'Dados do Aluno';
        $user = Auth::user();

        $emaildestino = "";
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
            $emaildestino = $config->email;
        }
        // dd($user, $request);
        $inputs = $request->validate([
            'ciclo' => 'required',
            'cidade_eetad' => 'required',
            'nome' => 'required|min:3',
            'end' => 'required|min:3',
            'num' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
//                'tel' => 'required',
            'dtnasc' => 'required',
            'cidnasc' => 'required',
            'ufnasc' => 'required',
            'nacionalidade' => 'required',
            'estcivil' => 'required',
//                'email' => 'required',
            'identidade' => 'required',
            'cpf' => 'required',
            'sexo' => 'required',
        ]);
        $subnucleos = Subnucleo::get();
        $subnucleo = Subnucleo::findOrFail($user->subnucleo_id);

        $matricula = Matricula::where('cpf', $request->cpf)
                ->where('id', '<>', $request->id)
                ->first();
        //dd($matricula);
        if ($matricula) {
//            Session::flash('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!');
//            $request->flash();
//            return view('aluno.dados', compact('config', 'user', 'menu', 'subnucleos', 'subnucleo'));
            return redirect()->route('aluno.dados')->with('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!')->withInput();
        } else {
            $user->subnucleo_id = $request->subnucleo_id;
            $user->ciclo = $request->ciclo;
            $user->cidade_eetad = $request->cidade_eetad;
            $user->estado = $request->estado;
            $user->nucleo = $request->nucleo;
            $user->nome = $request->nome;
            $user->end = $request->end;
            $user->num = $request->num;
            $user->bairro = $request->bairro;
            $user->cidade = $request->cidade;
            $user->uf = $request->uf;
            $user->cep = $request->cep;
            $user->tel = $request->tel;
            $user->dtnasc = $request->dtnasc;
            $user->cidnasc = $request->cidnasc;
            $user->ufnasc = $request->ufnasc;
            $user->nacionalidade = $request->nacionalidade;
            $user->estcivil = $request->estcivil;
            $user->email = $request->email;
            $user->identidade = $request->identidade;
            $user->cpf = $request->cpf;
            $user->sexo = $request->sexo;
            $user->igreja = $request->igreja;
            $user->cargo = $request->cargo;
            $user->qualcargo = $request->qualcargo;
            $user->escolaridade = $request->escolaridade;
            $user->profissao = $request->profissao;
            $user->formacaoteologica = $request->formacaoteologica;
            $user->escola = $request->escola;
            $user->avatar = $request->avatar;
//        $user->status = 'PENDENTE';
            if ($user->isDirty()) {

                $user->save();


//                $emaildestino = "";
//                $config = Configuraco::limit(1)->get();
                if ($emaildestino != "") {
//                    $config = $config[0];
//                    $emaildestino = $config->email;


                    Mail::send('emails.aluno_dados_editado', $request->all(), function ($m)use ($request, $emaildestino) {
                        $m->from('faleconosco@eetad0325.com.br', $request->nome . " - " . $request->email);
                        $m->to($emaildestino, 'EETAD Núcleo 0325')->subject('Dados do Aluno editado - EETAD 0325 (' . $request->nome . ')');
                    });
                }
                // Mensagem de sucesso para ser exibida
                Session::flash('success', $request->nome . ', seus dados foi editado e enviado à secretaria!');
                return redirect()->route('aluno.home')->with('success', $request->nome . ', seus dados foi editado e enviado à secretaria!')->withInput();
            }
            return redirect()->route('aluno.home');
//            return view('aluno.dados', compact('config', 'user', 'menu', 'subnucleos', 'subnucleo'));
        }
    }

}
