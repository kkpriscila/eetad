<?php

namespace App\Http\Controllers\Aluno\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Matricula;
use App\Models\Configuraco;
use Socialite;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/aluno/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest:aluno')->except('logout');
    }

    public function showLoginForm() {
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
        }
        //dd($config);
        return view('aluno.auth.login', compact('config'));
    }
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request) {
        $this->guard()->logout();
        return redirect('/aluno/login');
    }

    protected function guard() {
        return Auth::guard('aluno');
    }

    public function redirectToProvider() {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToProviderGoogle() {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback() {
        // dd('oi');
        $userSocial = Socialite::driver('facebook')->user();
        // dd($userSocial->getId());

        $findUser = Matricula::where('email', $userSocial->email)->first();
        if ($findUser) {
            $user = $findUser;
        } else {
            $user = new Matricula;

//            $user->nome = $userSocial->getName();
            $user->email = $userSocial->getEmail();
           
            $user->password = bcrypt(123456);
           
//            $user->avatar = $userSocial->avatar;
        }
//        $user->provider_id = $userSocial->getId();
//        dd($user->avatar, $userSocial->avatar);
        if ($user->avatar == "" or $user->avatar == null) {
            $user->avatar = $userSocial->avatar;
        }
//        dd($user->avatar, $userSocial->avatar);
       
        if ($user->nome == "" or $user->nome == null) {
            $user->nome = $userSocial->getName();
        }
         $user->provider_id = $userSocial->getId();
         $user->provider = 'facebook';

        $user->save();

//        dd($user);


        Auth::guard('aluno')->login($user);
//        $this->login($user);

        return redirect('/aluno/home');
        // $user->token;
    }

    public function handleProviderCallbackGoogle() {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/aluno/login');
        }        // only allow people with @company.com to login
//        if(explode("@", $user->email)[1] !== 'company.com'){
//            return redirect()->to('/');
//        }        // check if they're an existing user
        $existingUser = Matricula::where('email', $user->email)->first();
        if ($existingUser) {
            // log them in
            
            if ($existingUser->avatar == "" or is_null($existingUser->avatar) or $existingUser->avatar=='null') {
                $existingUser->avatar = $user->avatar;
            }
           
            if ($existingUser->nome == "" or is_null($existingUser->nome)) {
                $existingUser->nome = $user->name;
            }
            $existingUser->provider_id = $user->id;
            
            $existingUser->provider = 'google';
//dd( $existingUser->avatar,'avatar');
//            dd(($existingUser->avatar));
            $existingUser->save();
            Auth::guard('aluno')->login($existingUser, true);
        } else {
            // create a new user
            $newUser = new Matricula;
            $newUser->nome = $user->name;
            $newUser->email = $user->email;
            $newUser->provider_id = $user->id;
            $newUser->avatar = $user->avatar;
            $newUser->provider = 'google';
            $newUser->save();
            Auth::guard('aluno')->login($newUser, true);
        }
        return redirect('/aluno/home');
    }

//    public function fblogin(Request $request) {
//        $email= $request->email;
//        $name= $request->name;
//        $id= $request->id;
//        
//        $user = Matricula::where('provider_id',$id)->first();
//        if(!$user){
//            $user = New Matricula;
//            $user->name = $name;
//            $user->email = $email;
//            $user->provider_id= $id;
//            $user->save();
//        }
//        Auth::guard('aluno')->login($user,true);
//    }
}
