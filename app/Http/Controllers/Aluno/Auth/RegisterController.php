<?php

namespace App\Http\Controllers\Aluno\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Matricula;
use Illuminate\Support\Facades\Auth;
use App\Models\Configuraco;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/aluno/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest:aluno');
    }

    protected function guard() {
        return Auth::guard('aluno');
    }

    public function showRegistrationForm() {
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
        }
        //dd($config);
        return view('aluno.auth.register', compact('config'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'nome' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:matricula'],
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        return Matricula::create([
                    'nome' => $data['nome'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
        ]);
    }

}
