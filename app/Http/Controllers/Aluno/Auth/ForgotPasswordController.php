<?php

namespace App\Http\Controllers\Aluno\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Models\Configuraco;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:aluno');
    }
    
    public function showLinkRequestForm()
    {
         $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $config = $config[0];
        }
    
        return view('aluno.auth.passwords.email', compact('config'));
    }    
    public function broker()
    {
        return Password::broker('aluno');
    }
    
}
