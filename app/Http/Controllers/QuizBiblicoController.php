<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuizCategoria;
use App\Models\QuizPergunta;
use App\Models\Configuraco;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;


class QuizBiblicoController extends Controller {

    public function __construct() {
        $this->middleware('web');
    }

    public function index() {

        $config = Configuraco::limit(1)->get();

          if (count($config)>0){
          $config = $config[0];
      }
//      dd($config);


        $categoria = QuizCategoria::get();



//      dd(public_path('uploads/monitores'));
        return view('website.quizbiblico', compact('categoria', 'config'));
    }

    public function gerar_quiz(Request $request) {

        // dd($request['categoria']);
        $config = Configuraco::limit(1)->get();

         if (count($config)>0){
          $config = $config[0];
      }
//      dd($config);

        if ($request['categoria'] == 'TODAS') {
            $perguntas = QuizPergunta::where('nivel', $request['nivel'])
                    ->with('respostas')
                    ->whereHas('respostas'
//                            , function ($query) {
////                        return $query->where(DB::raw('count(quiz_respostas.pergunta_id) as total'), '>', 0);
//                        return $query->where('pergunta_id', '=', 5);
//                         dd($query->toSql(), $query->getBindings());
//                    }
                    )
                    ->orderByRaw("RAND()")
                    ->limit(10)
                    ->get();
//                      dd($perguntas->toSql(), $perguntas->getBindings());
        } else {
            $perguntas = QuizPergunta::where('nivel', $request['nivel'])
                    ->Where('categoria_id', $request['categoria'])
                    ->with('respostas')
                     ->whereHas('respostas')
                    ->orderByRaw("RAND()")
                    ->limit(10)
                    ->get();
        }



        $categoria = QuizCategoria::get();

        $inputs = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required',
        ]);


//        dd($perguntas, $request['nivel']);
//        echo $perguntas->count() .' - '. $request['nivel'].' - '. $request['categoria'];



        $request->flash();


//      dd(public_path('uploads/monitores'));
        return view('website.quizbiblico', compact('categoria', 'config', 'perguntas'));
    }

    public function enviar_quiz(Request $request) {
// echo(json_decode($request->getContent(), true));
        //dd($request->all());
//        $input = json_decode($request, true);
        $input = $request->all();

        $emaildestino = "";
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $emaildestino = $config[0]->email;
            $input['nmescola'] = $config[0]->nmescola;
            $input['logo'] = $config[0]->logo;
            $input['telefone_escola'] = $config[0]->telefone;
            $input['whatsapp'] = $config[0]->whatsapp;
//        $emaildestino = 'eetad0325@gmail.com';
        }

        print_r($input);


        //dd($request->all());
//        if ($inputs->fails()) {
//               $request->flash();
//               
//        }
        // Envio do email utilizando o Mailer
        //   Mail::to($inputs['emaildestino'], 'Fale Conosco - Site Câmara Ipatinga')->send(new FaleConoscoMail($inputs));

        Mail::send('emails.email_quiz', $input, function ($m)use ($input) {
            $m->from('faleconosco@eetad0325.com.br', 'EETAD 0325');
            $m->to($input['email'], $input['name'])->subject('Resultado Quiz bíblico');
        });
        Mail::send('emails.email_quiz_sec', $input, function ($m)use ($input, $emaildestino) {
            $m->from('faleconosco@eetad0325.com.br', 'EETAD 0325');
            $m->to($emaildestino, 'Secretaria EETAD 0325')->subject('Quiz bíblico realizado no site');
        });
        // Mensagem de sucesso para ser exibida
//            Session::flash('success', 'Matrícula de ' . $request['nome'] . ' enviada para o email da secretaria!');

        return response()->json(['success' => 'Got Simple Ajax Request.']);
//            return $this->index();
    }

}
