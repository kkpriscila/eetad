<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoriaQuizCrudRequest as StoreRequest;
use App\Http\Requests\CategoriaQuizCrudRequest as UpdateRequest;
use App\Models\QuizCategoria;

//use cmi_v6\Models\News;

/**
 * Class CategoriaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoriaQuizCrudController extends CrudController {

    public function setup() {
        /*
          |--------------------------------------------------------------------------
          | BASIC CRUD INFORMATION
          |--------------------------------------------------------------------------
         */


        $this->crud->setModel('App\Models\QuizCategoria');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/categoria');
        $this->crud->setEntityNameStrings('categoria', 'categorias');
        
        /*
          |--------------------------------------------------------------------------
          | COLUMNS AND FIELDS
          |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS

        $this->crud->addColumn([
            'name' => 'id', // The db column name
            'label' => "ID", // Table column heading
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'categoria', // The db column name
            'label' => "Categoria", // Table column heading
            'type' => 'Text',
            'limit' => 80, // character limit; default is 50;
            'searchLogic' => 'text',
        ]);






        // ------ CRUD FIELDS
        //BOTH
        //TAB DADOS
//        $this->crud->addField([
//            'name' => 'id', // the name of the db column
//            'label' => 'ID', // the input label
//            'type' => 'custom_html',
//           'value' => '<b>ID: </b>'.$this->crud->getCurrentEntryId(),
//                ], 'update');


        $this->crud->addField([
            'name' => 'id',
            'label' => "ID",
            'attributes' => [
                'readonly' => 'readonly'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-lg-2 col-md-2 col-sm-4'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'categoria',
            'label' => "Categoria",
            'wrapperAttributes' => [
                'class' => 'form-group col-lg-10 col-md-10 col-sm-8'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'categoria',
            'label' => "Categoria",
            'wrapperAttributes' => [
                'class' => 'form-group col-lg-12 col-md-12 col-sm-8'
            ],
                ], 'create');

//        $this->crud->addField([
//            'name' => 'dsfoto',
//            'label' => "Breve Descrição da Foto",
//            
//          
//        ]);
//         $this->crud->addField([
//            'name' => 'home', // the name of the db column
//            'label' => 'Ir para página Principal?', // the input label
//            'type' => 'radio',
//            'options' => [ // the key will be stored in the db, the value will be shown as label; 
//                'S' => "Sim",
//                'N' => "Não"
//            ],
//            // optional
//            
//                ]);
//        
//        $this->crud->addField([       // Select_Multiple = n-n relationship
//            'label' => 'Marque as categorias relacionadas à foto:',
//            'type' => 'select2_multiple',
//            'name' => 'categorias', // the method that defines the relationship in your Model
//            'entity' => 'categorias', // the method that defines the relationship in your Model
//            'attribute' => 'categoria', // foreign key attribute that is shown to user
//            'model' => "App\Models\Categoria", // foreign key model
//            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
//           
////            'attributes' => [
////                'rows' => '5'
////            ],
//        ]);
//       
        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');
        // add asterisk for fields that are required in CategoriaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ------ CRUD BUTTONS
        //$this->crud->addButtonFromModelFunction('line', 'ver_site', 'verSite', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        //$this->crud->removeButton('show');
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete', 'details_row', 'show']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');
        // ------ CRUD DETAILS ROW
        //   $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php
        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');
        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        //$this->crud->enableAjaxTable();
        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        $this->crud->enableExportButtons();
        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '=', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
//        $this->crud->orderBy('created_at', 'desc');
        // $this->crud->groupBy();
        // $this->crud->limit();
        // ------ FILTERS
//        $this->crud->addFilter(
//                [ // simple filter
//            'type' => 'dropdown',
//            'name' => 'publicar',
//            'label' => 'Publicado?'
//                ], function() {
//            return [
//                'S' => 'Sim',
//                'N' => 'Não',
//            ];
//        }
//                , function($value) { // if the filter is active
//            $this->crud->addClause('where', 'publicar', '=', $value);
//        });
//        $this->crud->addFilter([ // daterange filter
//            'type' => 'date_range',
//            'name' => 'date',
//            'label' => 'Dt. Cadastro'
//                ], false, function($value) { // if the filter is active, apply these constraints
//            $dates = json_decode($value);
//            $this->crud->addClause('where', 'date', '>=', $dates->from);
//            $this->crud->addClause('where', 'date', '<=', $dates->to);
//        });
//        $this->crud->addFilter($options, $values, $filter_logic);
//        $this->crud->removeFilter($name);
//        $this->crud->removeAllFilters();
//
//         $this->crud->addFilter([ // daterange filter
//            'type' => 'select2',
//            'name' => 'legislaturas',
//            'label' => 'Legislaturas'
//                ], function() {
//            return \cmi_v6\Models\CmiLegislatura::orderBy('dfim','desc')->pluck('legislatura', 'idlegislatura')->toArray();
//        }, function($value) { // if the filter is active
//            //->join('cmi_vereadorlegislat', "cmi_vereador.idvereador", "=", "cmi_vereadorlegislat.idvereador")
////            $this->crud->addClause('join', 'cmi_vereadorlegislat',  'cmi_vereador.idvereador ',  'cmi_vereadorlegislat.idvereador');
////          
////            $this->crud->addClause('where', 'cmi_vereadorlegislat.idlegislatura', $value);
//            $this->crud->query = $this->crud->query->join('cmi_vereadorlegislat', "cmi_vereador.idvereador", "=", "cmi_vereadorlegislat.idvereador")
//                    ->where( 'cmi_vereadorlegislat.idlegislatura', $value);
//       //dd($this->crud->query->toSql());
//            });
//      
//        $this->crud->filters(); // gets all the filters
        // dd($this->crud->filters());
        // dd($this->crud->query->toSql(),$this->crud->query->getBindings());
    }

    public function store(StoreRequest $request) {
        // your additional operations before save here


        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request) {
        // your additional operations before save here
//        $data = $request->all();
        //  dd($this->crud->model);

        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

//    public function destroy($id) {
////        dd('oi');
//        $this->crud->hasAccessOrFail('delete');
//        $this->crud->setOperation('delete');
//
//        $categoria = QuizCategoria::findOrFail($id)
//                ->with('perguntas')
//                ->get();
////        return Alert::error("teste");
////        return \Alert::error($categoria->perguntas->count());
////        dd($categoria->perguntas->count());
//        if ($categoria[0]->perguntas->count() > 0) {
//            // Error, can't a class with students
////            dd($categoria->perguntas->count());
//           return Alert::error("O registro não pode ser exluido pois é utilizado em outro Cadastro.");
////            return false;
//        }else{        
//        $id = $this->crud->getCurrentEntryId() ?? $id;
//        return $this->crud->delete($id);
//        }
//    }
//    public function __construct() {
//        parent::__construct();
////       $categoria = QuizCategoria::findOrFail(1)
////                ->with('perguntas')
////               ->get();
////       dd($categoria[0]->perguntas->count());
//    }
    

//     public function showDetailsRow($id){
//         
//         $assunto = News::where('id', $id)->first();
//         return $assunto->subject;
//         
//         
//     }
}
