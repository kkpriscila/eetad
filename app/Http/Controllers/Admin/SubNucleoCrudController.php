<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SubnucleoCrudRequest as StoreRequest;
use App\Http\Requests\SubnucleoCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Mail;


/**
 * Class CategoriaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SubNucleoCrudController extends CrudController {

    public function setup() {
        /*
          |--------------------------------------------------------------------------
          | BASIC CRUD INFORMATION
          |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Subnucleo');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subnucleos');
        $this->crud->setEntityNameStrings('sub-núcleo', 'sub-núcleos');

        /*
          |--------------------------------------------------------------------------
          | COLUMNS AND FIELDS
          |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS

        $this->crud->addColumn([
            'name' => 'nome', // The db column name
            'label' => "Nome Sub-núcleo", // Table column heading
            'type' => 'Text',
            'limit' => 80, // character limit; default is 50;
            'searchLogic' => 'text',
        ]);





        // ------ CRUD FIELDS
        //BOTH
        //TAB DADOS

        $this->crud->addField([
            'name' => 'nome',
            'label' => "Nome do Sub-Núcleo",
          
        ]);
        $this->crud->addField([
            'name' => 'responsável',
            'label' => "Responsável",
            'tab' => 'Dados',
        ]);

        $this->crud->addField([
            'name' => 'endereco',
            'label' => "Endereço",
            'type' => 'textarea',
            'attributes' => [
                'rows' => '2'
            ],
            'tab' => 'Dados',
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => "Email(usado para login)",
            'type' => 'email',
            'tab' => 'Login',
        ]);
        $this->crud->addField([
            'name' => 'password',
            'label' => "Senha",
            'type' => 'password',
            'tab' => 'Login',
        ]);
        $this->crud->addField([
            'name' => 'password_confirmation',
            'label' => 'Confirme a Senha',
            'type' => 'password',
            'tab' => 'Login',
        ]);
        $this->crud->addField([
            'name' => 'enviar_email',
//            'fake' => true, // show the field, but don't store it in the database column above
            "default" => 0,
            'label' => 'Enviar senha para o email?',
            'type' => 'checkbox',
            'tab' => 'Login',
        ]);


        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');
        // add asterisk for fields that are required in CategoriaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ------ CRUD BUTTONS
        //$this->crud->addButtonFromModelFunction('line', 'ver_site', 'verSite', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
//        $this->crud->removeButton('create');
//         $this->crud->removeButtonFromStack('delete', 'line');
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
        // ------ CRUD ACCESS
//        $this->crud->allowAccess(['list',  'update', 'reorder', 'details_row', 'show']);
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete', 'details_row', 'show']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');
        // ------ CRUD DETAILS ROW
        //   $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php
        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');
        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        //$this->crud->enableAjaxTable();
        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
         $this->crud->enableExportButtons();
        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '=', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
//        $this->crud->orderBy('vereador', 'asc');
        // $this->crud->groupBy();
        // $this->crud->limit();
        // ------ FILTERS
//        $this->crud->addFilter(
//                [ // simple filter
//            'type' => 'dropdown',
//            'name' => 'publicar',
//            'label' => 'Publicado?'
//                ], function() {
//            return [
//                'S' => 'Sim',
//                'N' => 'Não',
//            ];
//        }
//                , function($value) { // if the filter is active
//            $this->crud->addClause('where', 'publicar', '=', $value);
//        });
//        $this->crud->addFilter([ // daterange filter
//            'type' => 'date_range',
//            'name' => 'date',
//            'label' => 'Dt. Cadastro'
//                ], false, function($value) { // if the filter is active, apply these constraints
//            $dates = json_decode($value);
//            $this->crud->addClause('where', 'date', '>=', $dates->from);
//            $this->crud->addClause('where', 'date', '<=', $dates->to);
//        });
//        $this->crud->addFilter($options, $values, $filter_logic);
//        $this->crud->removeFilter($name);
//        $this->crud->removeAllFilters();
//
//         $this->crud->addFilter([ // daterange filter
//            'type' => 'select2',
//            'name' => 'legislaturas',
//            'label' => 'Legislaturas'
//                ], function() {
//            return \cmi_v6\Models\CmiLegislatura::orderBy('dfim','desc')->pluck('legislatura', 'idlegislatura')->toArray();
//        }, function($value) { // if the filter is active
//            //->join('cmi_vereadorlegislat', "cmi_vereador.idvereador", "=", "cmi_vereadorlegislat.idvereador")
////            $this->crud->addClause('join', 'cmi_vereadorlegislat',  'cmi_vereador.idvereador ',  'cmi_vereadorlegislat.idvereador');
////          
////            $this->crud->addClause('where', 'cmi_vereadorlegislat.idlegislatura', $value);
//            $this->crud->query = $this->crud->query->join('cmi_vereadorlegislat', "cmi_vereador.idvereador", "=", "cmi_vereadorlegislat.idvereador")
//                    ->where( 'cmi_vereadorlegislat.idlegislatura', $value);
//       //dd($this->crud->query->toSql());
//            });
//      
//        $this->crud->filters(); // gets all the filters
        // dd($this->crud->filters());
        // dd($this->crud->query->toSql(),$this->crud->query->getBindings());
    }

    public function store(StoreRequest $request) {
        // your additional operations before save here

        $this->enviarEmail($request);

        $this->handlePasswordInput($request);

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request) {
        // your additional operations before save here
//        $data = $request->all();
        //  dd($this->crud->model);
        //  dd($request->input('enviar_email'), $request->input('password'));

        $this->enviarEmail($request);
        $this->handlePasswordInput($request);
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    protected
            function handlePasswordInput(UpdateRequest $request) {

        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }

    function enviarEmail(UpdateRequest $request) {
       
        if ($request['enviar_email'] == 1) {

            $inputs['nome'] = $request->input('nome');
            $inputs['responsável'] = $request->input('responsável');
            $inputs['email'] = $request->input('email');
            $inputs['senha'] = $request->input('password');

 //dd('teste');
            Mail::send('emails.senha-subnucleos', $inputs, function ($m)use ($inputs) {
                $m->from('faleconosco@eetad0325.com.br', 'EETAD - Nucleo 0325');
                $m->to($inputs['email'], $inputs['nome'])->subject('Senha para matricula de alunos - Site EETAD Núcleo 0325');
            });
        }
        $request->request->remove('enviar_email');
    }

//     public function showDetailsRow($id){
//         
//         $assunto = News::where('id', $id)->first();
//         return $assunto->subject;
//         
//         
//     }
}
