<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserStoreCrudRequest as StoreRequest;
use App\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


//use cmi_v6\User;

/**
 * Class CategoriaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UserCrudController extends CrudController {

    public function setup() {
        //dd("oi");
        /*
          |--------------------------------------------------------------------------
          | BASIC CRUD INFORMATION
          |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('usuário', 'usuários');

        /*
          |--------------------------------------------------------------------------
          | COLUMNS AND FIELDS
          |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Nome", // Table column heading
            'type' => 'Text',
              'searchLogic' => 'text'
        ]);
        $this->crud->addColumn([
            'name' => 'email', // The db column name
            'label' => "Email(login)", // Table column heading
            'type' => 'Text',
              'searchLogic' => 'text'
        ]);
        $this->crud->addColumn([
            'name' => 'tipo', // The db column name
            'label' => "Tipo", // Table column heading
            'type' => 'Text',
              'searchLogic' => 'text'
        ]);





        // ------ CRUD FIELDS
        //BOTH

        $this->crud->addField([
            'name' => 'name',
            'label' => "Nome do usuário",
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => "Email(usado para login)",
            'type' => 'email'
        ]);
        $this->crud->addField([
            'name' => 'password',
            'label' => "Senha",
            'type' => 'password',
        ]);
        $this->crud->addField([
            'name' => 'password_confirmation',
            'label' => 'Confirme a Senha',
            'type' => 'password',
        ]);
         $this->crud->addField([
            'name' => 'tipo',
            'label' => "Tipo",
            'type' => 'select_from_array',
            'options' => [
                '' => null,
                'ADMINISTRADOR' => 'ADMINISTRADOR',
                'SECRETARIA' => 'SECRETÁRIA',
                'QUIZ' => 'QUIZ',
                'QUIZ REVISOR' => 'QUIZ REVISOR',
               
            ],
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-sm-6 col-sm-12'
            ],
        ]);
          $this->crud->addField([
            'name' => 'dtexpira',
            'label' => "Expira em",
            'type' => 'date',
            'date_picker_options' => [
                'todayHighlight' => true,
                'format' => 'dd/mm/yyyy',
                'todayBtn' => "linked",
                'clearBtn' => true,
                'language' => "pt-BR"
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-lg-6 col-md-6 col-sm-12'
            ], // extra HTML attributes for the field wrapper - mostly for resizing fields 
        ]);
           $this->crud->addField([
            'name' => 'enviar_email',
//            'fake' => true, // show the field, but don't store it in the database column above
            "default" => 0,
            'label' => 'Enviar senha para o email?',
            'type' => 'checkbox',
           
        ]);
       


        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');
        // add asterisk for fields that are required in CategoriaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ------ CRUD BUTTONS
        $this->crud->addButtonFromModelFunction('line', 'active_desactive', 'active_desactive', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromModelFunction('line', 'ver_site', 'verSite', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        //$this->crud->removeButton('show');
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete', 'show']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');
        // ------ CRUD DETAILS ROW
        //   $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php
        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');
        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        $this->crud->enableAjaxTable();
        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();
        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '=', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        $this->crud->orderBy('name', 'asc');
        // $this->crud->groupBy();
        // $this->crud->limit();
        // ------ FILTERS
//        $this->crud->addFilter([ // select2 filter
//            'name' => 'IdMenu',
//            'type' => 'select2',
//            'label' => 'Menu Principal'
//                ], function() {
//            return \cmi_v6\Models\CmiMenu::all()->keyBy('IdMenu')->pluck('dsmenu', 'IdMenu')->toArray();
//        }, function($value) { // if the filter is active
//             $this->crud->addClause('where', 'IdMenu', $value);
//        });
//        $this->crud->addFilter($options, $values, $filter_logic);
//        $this->crud->removeFilter($name);
//        $this->crud->removeAllFilters();
//
        $this->crud->filters(); // gets all the filters
    }

    public function store(StoreRequest $request) {
        // your additional operations before save here
        $this->enviarEmail($request);
        $this->handlePasswordInput($request);

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request) {
        // your additional operations before save here
//        dd("http://$_SERVER[HTTP_HOST]");
        $this->enviarEmail($request);
        $this->handlePasswordInput($request);

        // dd($request['dtpublica']);
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    protected function handlePasswordInput(Request $request) {

        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }
    function enviarEmail($request) {
       
        if(isset($request['enviar_email'])){
        if ($request['enviar_email'] == 1) {

            $inputs['name'] = $request->input('name');          
            $inputs['email'] = $request->input('email');
            $inputs['senha'] = $request->input('password');

 //dd('teste');
            Mail::send('emails.senha-users', $inputs, function ($m)use ($inputs) {
                $m->from('faleconosco@eetad0325.com.br', 'EETAD - Nucleo 0325');
                $m->to($inputs['email'], $inputs['name'])->subject('Senha para Portal Admin - Site EETAD Núcleo 0325');
            });
        }
        $request->request->remove('enviar_email');
    }
    }

}
