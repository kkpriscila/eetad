<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Aluno;
use App\Models\Matricula;
use App\Models\Monitore;
use App\Models\Subnucleo;

class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    /* SELECT MES PARA O GRAFICO
      SELECT  month(created_at) as mes,  year(created_at) as ano,
      CONCAT (month(created_at),'/', year(created_at)) as mes_ano
      FROM dbeetad.matricula m
      group by  ano, mes,mes_ano;

      /*SELECT PRE-MATRICULAS / MES
      SELECT month(created_at) as mes,  year(created_at) as ano,
      count(*) as qtd
      FROM dbeetad.matricula m
      group by  ano, mes
      order by ano desc, mes desc;

      /*SELECT MATRICULAS APROVADAS/ MES
      SELECT month(created_at) as mes,  year(created_at) as ano,
      count(*) as qtd
      FROM dbeetad.matricula m
      WHERE status = 'APROVADA'
      group by  ano, mes
      order by ano desc, mes desc;
     * 
     */

    public function index() {
        $matriculados = Aluno::select(DB::raw('count(*) as qtd'))
                ->where('status', 'MATRICULADO')
                ->get();

        $monitores = Monitore::select(DB::raw('count(*) as qtd'))
                ->where('tipo', 'Monitor')
                ->get();

        $subnucleos = Subnucleo::select(DB::raw('count(*) as qtd'))
                ->get();
        $categorias = \App\Models\QuizCategoria::select(DB::raw('count(*) as qtd'))
                ->get();
        $perguntas = \App\Models\QuizPergunta::select(DB::raw('count(*) as qtd'))
                ->get();


        $meses_matricula = Matricula::select(DB::raw('month(created_at) as mes'), DB::raw('year(created_at) as ano')
                        , DB::raw("CONCAT (month(created_at),'/', year(created_at)) as mes_ano"))
                ->groupBy('ano', 'mes', 'mes_ano')
                ->orderBy('ano', 'asc')
                ->orderBy('mes', 'asc')
                ->limit(6)
                ->get();

        $pre_matriculas = Matricula::select(DB::raw('month(created_at) as mes'), DB::raw(' year(created_at) as ano')
                        , DB::raw("count(*) as qtd"))
                ->groupBy('ano', 'mes')
                ->orderBy('ano', 'asc')
                ->orderBy('mes', 'asc')
                ->limit(6)
                ->get();

        $matriculas_aprov = Matricula::select(DB::raw('month(created_at) as mes'), DB::raw(' year(created_at) as ano')
                        , DB::raw("count(*) as qtd"))
                 ->where('status', 'APROVADA')
                ->groupBy('ano', 'mes')
                ->orderBy('ano', 'asc')
                ->orderBy('mes', 'asc')
                ->limit(6)
                ->get();
        
        $alunos_sexo = Aluno::select(DB::raw('count(*) as qtd'),'sexo')
                ->groupBy('sexo')
                ->get();
        $alunos_idade = Aluno::select(DB::raw('TIMESTAMPDIFF(YEAR, dtnasc, NOW()) AS idade'),DB::raw('count(*) as qtd'))
                ->groupBy('idade')
                ->orderBy('idade')
                ->get();

//dd($pre_matriculas);
//        dd($matriculados->toSql());
        //$prop = json_encode($prop);
        //     dd($qtds);
//        return view('gestao_a_vista', compact('tpprop','prop','mes','qtds'));
        return view('vendor.backpack.base.dashboard', 
                compact('matriculados', 'subnucleos','categorias','perguntas' ,'monitores', 'meses_matricula', 'pre_matriculas','matriculas_aprov','alunos_sexo','alunos_idade'));
    }

}
