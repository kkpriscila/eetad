<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\MatriculaCrudRequest as StoreRequest;
use App\Http\Requests\MatriculaCrudRequest as UpdateRequest;
use Carbon\Carbon;
use App\Models\Aluno;
use Illuminate\Http\File;

/**
 * Class CategoriaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MatriculaCrudController extends CrudController {

    public function setup() {

//        parent::setup();

        /*
          |--------------------------------------------------------------------------
          | BASIC CRUD INFORMATION
          |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Matricula');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/matricula');
        $this->crud->setEntityNameStrings('solicitção de matricula', 'solicitções de matriculas');

        $this->crud->addClause('where', 'status', 'PENDENTE')
                ->OrWhere('status', 'ERRO');
        /*
          |--------------------------------------------------------------------------
          | COLUMNS AND FIELDS
          |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Data do Envio',
            'type' => 'datetime',
            'searchLogic' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Data da Atualização',
            'type' => 'datetime',
            'searchLogic' => 'text',
        ]);
        $this->crud->addColumn([
            'label' => "Ciclo", // Table column heading
            'type' => "text",
            'name' => 'ciclo', // the column that contains the ID of that connected entity;
            'searchLogic' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'nome', // The db column name
            'label' => "Nome", // Table column heading
            'type' => 'Text',
            'searchLogic' => 'text',
        ]);

//        $this->crud->addColumn([ // select_from_array
//            'name' => 'cpf',
//            'label' => "Cpf",
//            'type' => 'Text',
//            'searchLogic' => 'text',
//        ]);
        $this->crud->addColumn([ // select_from_array
            'name' => 'subnucleo',
            'label' => "Subnucleo",
            'type' => 'Text',
            'limit' => 30, // character limit; default is 50;
            'searchLogic' => 'text',
        ]);


        $this->crud->addColumn([ // select_from_array
            'name' => 'status',
            'label' => "Status",
            'type' => 'Text',
            'searchLogic' => 'text',
        ]);





        // ------ CRUD FIELDS
        // UPDATE
        $this->crud->addField([
            'name' => 'ciclo', // the name of the db column
            'label' => 'Ciclo', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update'
        );
        $this->crud->addField([
            'name' => 'cidade_eetad', // the name of the db column
            'label' => 'Cidade do Curso', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'estado', // the name of the db column
            'label' => 'Estado', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'subnucleo', // the name of the db column
            'label' => 'Subnuceo Solicitante', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'resp_subnucleo', // the name of the db column
            'label' => 'Responsável Subnucleo', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'email_subnucleo', // the name of the db column
            'label' => 'Email subnucleo', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
                ], 'update');
        $this->crud->addField(
                [// CustomHTML
                    'name' => 'separator',
                    'type' => 'custom_html',
                    'value' => '<hr><h3>Dados do Aluno</h3><hr>'
                ]
        );

        $this->crud->addField([
            'name' => 'nome', // the name of the db column
            'label' => 'Nome', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'end', // the name of the db column
            'label' => 'Endereço', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'num', // the name of the db column
            'label' => 'Número', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'bairro', // the name of the db column
            'label' => 'Bairro', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'cidade', // the name of the db column
            'label' => 'Cidade', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'cep', // the name of the db column
            'label' => 'CEP', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'uf', // the name of the db column
            'label' => 'UF', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'dtnasc', // the name of the db column
            'label' => 'Data de Nascimento:', // the input label        
            'type' => 'date',
//            'date_picker_options' => [
//
//                'format' => 'dd/mm/yyyy',
//                'language' => 'pt-br',
//            ],
            'attributes' => [
                'disabled' => 'disabled',
            ],
            // optional
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3 '
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'cidnasc', // the name of the db column
            'label' => 'Naturalidade', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'ufnasc', // the name of the db column
            'label' => 'Estado Nasc.', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'nacionalidade', // the name of the db column
            'label' => 'Nacionalidade', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'estcivil', // the name of the db column
            'label' => 'Est. Civil', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'tel', // the name of the db column
            'label' => 'Telefone', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'email', // the name of the db column
            'label' => 'Email', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'identidade', // the name of the db column
            'label' => 'Identidade', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');

        $this->crud->addField([
            'name' => 'cpf', // the name of the db column
            'label' => 'Cpf', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'sexo', // the name of the db column
            'label' => 'Sexo', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'igreja', // the name of the db column
            'label' => 'Igreja em que congrega', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'qualcargo', // the name of the db column
            'label' => 'Qual cargo', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'escolaridade', // the name of the db column
            'label' => 'Escolaridade', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'profissao', // the name of the db column
            'label' => 'Profissão', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');

        $this->crud->addField([
            'name' => 'escola', // the name of the db column
            'label' => 'Escola de Formação Teológica?', // the input label
            'type' => 'text',
            // optional
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
                ], 'update');


        $this->crud->addField([
            'name' => 'avatar',
            'label' => "Foto do Aluno",
            'type' => 'text',
            'attributes' => [
                'disabled' => 'disabled'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);
        $this->crud->addField([
            'name' => 'created_at', // the name of the db column
            'label' => 'Matrícula enviada em:', // the input label  
             'type' => 'datetime',      
//            'type' => 'datetime_picker',
//            'date_picker_options' => [
//
//                'format' => 'dd/mm/YYYY',
//                'language' => 'pt-br',
//            ],
            'attributes' => [
                'disabled' => 'disabled',
            ],
            // optional
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 '
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'updated_at', // the name of the db column
            'label' => 'Matricula atualizada em:', // the input label   
             'type' => 'datetime', 
//            'type' => 'datetime_picker',
//            'date_picker_options' => [
//
//                'format' => 'dd/mm/YYYY',
//                'language' => 'pt-br',
//            ],
            'attributes' => [
                'disabled' => 'disabled',
            ],
            // optional
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 '
            ],
                ], 'update');
        $this->crud->addField([
            'name' => 'livro', // the name of the db column
            'label' => 'Começar em qual Livro?', // the input label
            'type' => 'text',
            // optional
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
                ], 'update');


        $this->crud->addField([
            'name' => 'dttaxamatricula',
            'label' => "Dt. Tx. de Matrícula",
             'type' => 'date', 
//            'type' => 'date_picker',
//            'defaultDate' => date('d/m/Y'),
//            'date_picker_options' => [
//
//                'format' => 'dd/mm/yyyy',
//                'language' => "pt-BR",
//                'todayHighlight' => true,
////                'daysOfWeekDisabled' => '[0, 6]',
////                'defaultDate' => now()
//            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
                ], 'update');

        $this->crud->addField([
            'name' => 'motivotxmatricula',
            'label' => "Motivo Tx. de Matrícula",
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
        ]);
        $this->crud->addField([
            'name' => 'status', // the name of the db column
            'label' => 'Status', // the input label
            'type' => 'radio',
            'options' => [ // the key will be stored in the db, the value will be shown as label; 
                'PENDENTE' => "PENDENTE",
                'APROVADA' => "APROVADA",
                'REPROVADA' => "REPROVADA",
                'ERRO' => "ERRO",
            ],
            // optional
            'wrapperAttributes' => [
                'class' => 'form-group col-lg-4 col-md-4 col-sm-12'
            ],
        ]);
        $this->crud->addField([
            'name' => 'subnucleo_id',
            'label' => "Matricular no Subnucleo",
            'type' => 'select2',
            'entity' => 'subnucleo', // the method that defines the relationship in your Model
            'attribute' => 'nome', // foreign key attribute that is shown to user
            'model' => "App\Models\Subnucleo", // foreign key model
            'default' => '', // set the default value of the select2
            'allows_null' => true,
//            'options' => (function ($query) {
//                return $query->orderBy('categoria', 'ASC')->get();
//            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'wrapperAttributes' => [
                'class' => 'form-group col-sm-12 col-md-8 col-md-8'
            ],
        ]);
        $this->crud->addField([
            'name' => 'justifica_status',
            'label' => "Justificativa do Status(Exibido no Portal do Aluno e Subnúcleo)",
            'type' => 'textarea',
            'attributes' => [
                'rows' => '2'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-sm-12 col-md-8 col-md-8'
            ],
        ]);



        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');
        // add asterisk for fields that are required in CategoriaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ------ CRUD BUTTONS
        // $this->crud->addButtonFromModelFunction('line', 'ver_site', 'verSite', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->removeButton('show');
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'update', 'reorder', 'details_row', 'show']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');
        // ------ CRUD DETAILS ROW
        //   $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php
        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');
        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        //$this->crud->enableAjaxTable();
        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        $this->crud->enableExportButtons();
        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '=', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        $this->crud->orderBy('created_at');
        // $this->crud->groupBy();
        // $this->crud->limit();
//        $this->crud->addFilter(
//                [// simple filter
//            'type' => 'text',
//            'name' => 'tiposolicitacao',
//            'label' => 'Tipo'
//                ], false, function($value) { // if the filter is active
//            $this->crud->addClause('where', 'tiposolicitacao', 'LIKE', "$value");
//        });
//        $this->crud->addFilter(
//                [// simple filter
//            'type' => 'text',
//            'name' => 'protocolo',
//            'label' => 'Nº Protocolo'
//                ], false, function($value) { // if the filter is active
//            $this->crud->addClause('where', 'protocolo', 'LIKE', "$value");
//        }
//        );
//        $this->crud->addFilter(
//                [// simple filter
//            'type' => 'text',
//            'name' => 'nome',
//            'label' => 'Solicitante'
//                ], false, function($value) { // if the filter is active
//            $this->crud->addClause('where', 'nome', 'LIKE', "$value");
//        }
//        );
//        $this->crud->addFilter(
//                [// dataRange filter
//            'type' => 'date_range',
//            'name' => 'from_to',
//            'label' => 'Data Solicitação'
//                ], false, function($value) { // if the filter is active
//            $dates = json_decode($value);
//            $this->crud->addClause('where', 'dtsolicitacao', '>=', $dates->from);
//            $this->crud->addClause('where', 'dtsolicitacao', '<=', $dates->to);
//        }
//        );
//        $this->crud->filters(); // gets all the filters
    }

    public function store(StoreRequest $request) {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request) {
        // your additional operations before save here
        if ($request['justifica_status'] == "" and $request['status'] == "APROVADA") {
            $request['justifica_status'] = 'Pagamento - OK ';
        }

        $subnucleo = \App\Models\Subnucleo::findOrFail($request->subnucleo_id);
//        dd( $subnucleo);
        if ($subnucleo) {
//            dd($subnucleo->responsável);
            $request['resp_subnucleo'] = $subnucleo->responsável;
            $request['email_subnucleo'] = $subnucleo->email;
//              dd($request['resp_subnucleo']);
        }
//        dd($request->resp_subnucleo );
//        if ($this->crud->model->isDirty()) {
//
//        }
//         dd(parent::updateCrud($request));
        $redirect_location = parent::updateCrud($request);
        $matricula = \App\Models\Matricula::findOrFail($request->id);
        if ($request['status'] == "APROVADA") {
//            dd ($matricula );
//            CADASTRAR ALUNO OU ATUALIZAR CASO EXISTA
//            dd($matricula->cpf);
            $aluno = Aluno::where('cpf',$matricula->cpf)->get();
           // dd($aluno->count());
            if ($aluno->count()==0) {
               // dd($matricula->ciclo);
                $aluno = new Aluno();
                 $aluno->rematricula = 'N';
            }else{
                $aluno = $aluno[0];                
                $aluno->rematricula = 'S';
              
            }
            $aluno->matricula_id = $matricula->id;
            $aluno->subnucleo_id = $matricula->subnucleo_id;
            $aluno->ciclo = $matricula->ciclo;
            $aluno->cidade_eetad = $matricula->cidade_eetad;        
            $aluno->nome = $matricula->nome;
            $aluno->end = $matricula->end;
            $aluno->num = $matricula->num;
            $aluno->bairro = $matricula->bairro;
            $aluno->cidade = $matricula->cidade;
            $aluno->uf = $matricula->uf;
            $aluno->cep = $matricula->cep;
            $aluno->tel = $matricula->tel;
            $aluno->dtnasc = $matricula->dtnasc;
            $aluno->cidnasc = $matricula->cidnasc;
            $aluno->ufnasc = $matricula->ufnasc;
            $aluno->nacionalidade = $matricula->nacionalidade;
            $aluno->estcivil = $matricula->estcivil;
            $aluno->email = $matricula->email;
            $aluno->identidade = $matricula->identidade;
            $aluno->cpf = $matricula->cpf;
            $aluno->sexo = $matricula->sexo;
            $aluno->igreja = $matricula->igreja;
            $aluno->cargo = $matricula->cargo;
            $aluno->qualcargo = $matricula->qualcargo;
            $aluno->escolaridade = $matricula->escolaridade;
            $aluno->profissao = $matricula->profissao;
            $aluno->formacaoteologica = $matricula->formacaoteologica;
            $aluno->escola = $matricula->escola;
            $aluno->foto = $matricula->avatar;
//            $aluno->foto = 'MATRICULADO';
            $aluno->livro = $matricula->livro;
            $aluno->status = 'MATRICULADO';
            $aluno->dtmatricula = date("Y-m-d");
//        dd('Aqui: ', $aluno );
            $aluno->save();
            }
            
          //  dd($aluno);
        
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        return $redirect_location;
    }

//     public function showDetailsRow($id){
//         
//         $assunto = News::where('id', $id)->first();
//         return $assunto->subject;
//         
//         
//     }
}
