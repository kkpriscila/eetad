<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//
//use Jenssegers\Date\Date;
use App\Models\Configuraco;
use App\Models\Foto;
use App\Models\Monitore;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;



class IndexController extends Controller {

    public function getHome() {
        $menu = 'home';
    
      $diretoria = Monitore::limit(4)->where('tipo','Diretoria')
              ->get();
      $titular = Monitore::limit(3)->where('tipo','Titular')
              ->get();
      $config = Configuraco::limit(1)->get();
      $galeria = Foto::where('home',1)
              ->inRandomOrder()
//              ->orderby('created_at','desc')
              ->limit(6)->get();
      
      if (count($config)>0){
          $config = $config[0];
      }
    
    //  dd($config);
         return view('website.home', compact('diretoria','titular','config','galeria','menu'));
    }
    public function getFotos() {
        $config = Configuraco::limit(1)->get();
//        $galeria = Foto::paginate(15);
        $galeria = Foto::get();
       
         if (count($config)>0){
          $config = $config[0];
      }
//      dd(public_path('uploads/monitores'));
        return view('website.fotos', compact('galeria','config'));
    }
    public function getMonitores() {
        $config = Configuraco::limit(1)->get();
        $monitores = Monitore::where('tipo','<>','Diretoria')->paginate(16);//paginate() acho que o padrão é 15
      
         if (count($config)>0){
          $config = $config[0];
      }
      
//      dd($monitores);
//      dd(public_path('uploads/monitores'));
        return view('website.monitores', compact('monitores','config'));
    }
     public function contato() {
        $menu = 'contato';

         $config = Configuraco::limit(1)->get();

        if (count($config) > 0) {
            $config = $config[0];
        }

        //dd($config);
        return view('website.contato', compact('config', 'menu'));
    }
     public function postContato(Request $request) {
         $menu = 'contato';
      $emaildestino="";
          $config = Configuraco::limit(1)->get();
         if (count($config) > 0) {
            $emaildestino = $config[0]->email;
        
       
        //$emaildestino ='imprensacmi@camaraipatinga.mg.gov.br';
    
       
        $inputs = $request->validate([
            'name' => 'required|between:3,50',
            'email' => 'required|email|between:5,50',
            'number' => '',
//            'emaildestino' => 'required|between:3,50',
            'subject' => 'required|min:5',
            'menssagem' => 'required|min:5'
        ],[],[
            'name'  => 'Nome',
            'email' => 'Email',
            'number' => 'Telefone',
//            'emaildestino' => 'Destinatário',
            'subject' => 'Assunto',
            'menssagem' => 'Menssagem',
        ]);
        $inputs['emaildestino']=$emaildestino;
        if (!isset($inputs['number'])){
            $inputs['number']='';
        }
         
    //dd($inputs['message']);
        
        // Envio do email utilizando o Mailer
     //   Mail::to($inputs['emaildestino'], 'Fale Conosco - Site Câmara Ipatinga')->send(new FaleConoscoMail($inputs));
                
          Mail::send('emails.faleconosco', $inputs, function ($m)use ($inputs, $emaildestino){
            $m->from ('faleconosco@eetad0325.com.br', $inputs['name']." - ".$inputs['email']);
            $m->to($emaildestino, 'EETAD Núcleo 0325')->subject('Fale Conosco - EETAD 0325('.$inputs['subject'].')');
        });
        // Mensagem de sucesso para ser exibida
        Session::flash('success', 'Email enviado com sucesso!');
         }else{
             Session::flash('error', 'Email não pode ser enviado!');
         }
        return  $this->contato();
    }

}
