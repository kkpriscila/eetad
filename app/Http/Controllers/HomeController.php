<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Models\Configuraco;
use App\Models\Matricula;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:subnucleo');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $matriculas = Matricula::where('subnucleo_id', Auth::user()->id)
                ->paginate(10);
        //dd($matriculas);
        return view('subnucleo.home', compact('matriculas'));
    }

    public function form_matricula() {
        return view('subnucleo.matricula');
    }

    public function editar_matricula(Request $request) {

        $matricula = Matricula::where('id', $request->id)->first();
//         dd($matricula->subnucleo);
        return view('subnucleo.editar_matricula', compact('matricula'));
    }

    public function enviar_matricula(Request $request) {



        $emaildestino = "";
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $emaildestino = $config[0]->email;

//        $emaildestino = 'eetad0325@gmail.com';


            $inputs = $request->validate([
                'ciclo' => 'required',
                'cidade_eetad' => 'required',
                'nome' => 'required|min:3',
                'end' => 'required|min:3',
                'num' => 'required',
                'bairro' => 'required',
                'cidade' => 'required',
                'uf' => 'required',
                'cep' => 'required',
//                'tel' => 'required',
                'dtnasc' => 'required',
                'cidnasc' => 'required',
                'ufnasc' => 'required',
                'nacionalidade' => 'required',
                'estcivil' => 'required',
//                'email' => 'required',
                'identidade' => 'required',
                'cpf' => 'required',
                'sexo' => 'required',
            ]);

            $matricula = Matricula::where('cpf', $request->cpf)->first();
            if ($matricula) {
//                Session::flash('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!');
//                return view('subnucleo.matricula');
                return redirect()->route('form.matricula')->with('warning', 'O cpf ' . $request->cpf . ' já existe em nossa base. Verifique com a secretaria!')->withInput();
            } else {
                $matricula = new Matricula;
                $matricula->subnucleo_id = $request->subnucleo_id;
                $matricula->subnucleo = $request->subnucleo;
                $matricula->resp_subnucleo = $request->resp_subnucleo;
                $matricula->email_subnucleo = $request->email_subnucleo;
                $matricula->ciclo = $request->ciclo;
                $matricula->cidade_eetad = $request->cidade_eetad;
                $matricula->estado = $request->estado;
                $matricula->nucleo = $request->nucleo;
                $matricula->nome = $request->nome;
                $matricula->end = $request->end;
                $matricula->num = $request->num;
                $matricula->bairro = $request->bairro;
                $matricula->cidade = $request->cidade;
                $matricula->uf = $request->uf;
                $matricula->cep = $request->cep;
                $matricula->tel = $request->tel;
                $matricula->dtnasc = $request->dtnasc;
                $matricula->cidnasc = $request->cidnasc;
                $matricula->ufnasc = $request->ufnasc;
                $matricula->nacionalidade = $request->nacionalidade;
                $matricula->estcivil = $request->estcivil;
                $matricula->email = $request->email;
                $matricula->identidade = $request->identidade;
                $matricula->cpf = $request->cpf;
                $matricula->sexo = $request->sexo;
                $matricula->igreja = $request->igreja;
                $matricula->cargo = $request->cargo;
                $matricula->qualcargo = $request->qualcargo;
                $matricula->escolaridade = $request->escolaridade;
                $matricula->profissao = $request->profissao;
                $matricula->formacaoteologica = $request->formacaoteologica;
                $matricula->escola = $request->escola;
                $matricula->avatar = $request->avatar;
                $matricula->status = 'PENDENTE';
                $matricula->justifica_status = 'Pagamento - AGUARDANDO ';

                $matricula->save();
            }

            //dd($request->all());
//        if ($inputs->fails()) {
//               $request->flash();
//               
//        }
            // Envio do email utilizando o Mailer
            //   Mail::to($inputs['emaildestino'], 'Fale Conosco - Site Câmara Ipatinga')->send(new FaleConoscoMail($inputs));

            Mail::send('emails.matricula', $request->all(), function ($m)use ($request, $emaildestino) {
                $m->from('faleconosco@eetad0325.com.br', $request['subnucleo'] . " - " . $request['email_subnucleo']);
                $m->to($emaildestino, 'EETAD Núcleo 0325')->subject('Matrícula - EETAD 0325 (' . $request['nome'] . ')');
            });
            // Mensagem de sucesso para ser exibida
//            Session::flash('success', 'Matrícula de ' . $request['nome'] . ' enviada para o email da secretaria!');
//            $request->flash();
//            return view('subnucleo.matricula');
            return redirect()->route('form.matricula')->with('success', 'Matrícula de ' . $request['nome'] . ' enviada para o email da secretaria!')->withInput();
        }
    }

    public function update_matricula(Request $request) {

        $emaildestino = "";
        $config = Configuraco::limit(1)->get();
        if (count($config) > 0) {
            $emaildestino = $config[0]->email;

            $inputs = $request->validate([
                'ciclo' => 'required',
                'cidade_eetad' => 'required',
                'nome' => 'required|min:3',
                'end' => 'required|min:3',
                'num' => 'required',
                'bairro' => 'required',
                'cidade' => 'required',
                'uf' => 'required',
                'cep' => 'required',
//                'tel' => 'required',
                'dtnasc' => 'required',
                'cidnasc' => 'required',
                'ufnasc' => 'required',
                'nacionalidade' => 'required',
                'estcivil' => 'required',
//                'email' => 'required',
                'identidade' => 'required',
                'cpf' => 'required',
                'sexo' => 'required',
            ]);

            $matricula = Matricula::where('id', $request->id)->first();
            if ($matricula) {

                $matricula->subnucleo_id = $request->subnucleo_id;
                $matricula->subnucleo = $request->subnucleo;
                $matricula->resp_subnucleo = $request->resp_subnucleo;
                $matricula->email_subnucleo = $request->email_subnucleo;
                $matricula->ciclo = $request->ciclo;
                $matricula->cidade_eetad = $request->cidade_eetad;
                $matricula->estado = $request->estado;
                $matricula->nucleo = $request->nucleo;
                $matricula->nome = $request->nome;
                $matricula->end = $request->end;
                $matricula->num = $request->num;
                $matricula->bairro = $request->bairro;
                $matricula->cidade = $request->cidade;
                $matricula->uf = $request->uf;
                $matricula->cep = $request->cep;
                $matricula->tel = $request->tel;
                $matricula->dtnasc = $request->dtnasc;
                $matricula->cidnasc = $request->cidnasc;
                $matricula->ufnasc = $request->ufnasc;
                $matricula->nacionalidade = $request->nacionalidade;
                $matricula->estcivil = $request->estcivil;
                $matricula->email = $request->email;
                $matricula->identidade = $request->identidade;
                $matricula->cpf = $request->cpf;
                $matricula->sexo = $request->sexo;
                $matricula->igreja = $request->igreja;
                $matricula->cargo = $request->cargo;
                $matricula->qualcargo = $request->qualcargo;
                $matricula->escolaridade = $request->escolaridade;
                $matricula->profissao = $request->profissao;
                $matricula->formacaoteologica = $request->formacaoteologica;
                $matricula->escola = $request->escola;
                $matricula->avatar = $request->avatar;
                $matricula->status = 'PENDENTE';

                if ($matricula->isDirty()) {

                    $matricula->save();
                    Mail::send('emails.matricula_editada', $request->all(), function ($m)use ($request, $emaildestino) {
                        $m->from('faleconosco@eetad0325.com.br', $request['subnucleo'] . " - " . $request['email_subnucleo']);
                        $m->to($emaildestino, 'EETAD Núcleo 0325')->subject('Matrícula Editada - EETAD 0325 (' . $request['nome'] . ')');
                    });
                    // Mensagem de sucesso para ser exibida
//                    Session::flash('success', 'Matrícula de ' . $request['nome'] . ' foi EDITADA e enviada para o email da secretaria!');
                    return redirect()->route('home')->with('success', 'Matrícula de ' . $request['nome'] . ' foi EDITADA e enviada para o email da secretaria!')->withInput();
                }
//                $matriculas = Matricula::where('subnucleo_id', Auth::user()->id)
//                        ->paginate(10);
                //dd($matriculas);
                return redirect()->route('home');
//                return view('subnucleo.home', compact('matriculas'));
            }

            //dd($request->all());
//        if ($inputs->fails()) {
//               $request->flash();
//               
//        }


//            $request->flash();
//            return view('subnucleo.editar_matricula', compact('matricula'));
            return redirect()->route('editar.matricula')->withInput();
        }
    }

}
