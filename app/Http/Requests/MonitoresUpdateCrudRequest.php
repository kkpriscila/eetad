<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MonitoresUpdateCrudRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nome' => 'required|min:3',
            'titulo' => 'required|min:3',
            'tipo' => 'required|min:3',            
            'foto' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:3000',
//            'fotosobre' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:3000',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes() {
        return [
       
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
               'foto.max' => 'Tamanho máximo de 3Mb',
            'foto.image' => 'Permitido apenas arquivos de imagens',
            'foto.mimes' => 'Extensões permitidas: jpeg,png,jpg,gif,svg',
//            'fotosobre.max' => 'Tamanho máximo de 300Mb',
//            'fotosobre.image' => 'Permitido apenas arquivos de imagens',
//            'fotosobre.mimes' => 'Extensões permitidas: jpeg,png,jpg,gif,svg',
        ];
    }

}
