<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubnucleoCrudRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nome' => 'required|min:3',
            //  'email' => 'required|email|unique:users',
             'email' => [
//                'required',
                Rule::unique('subnucleo')->ignore($this->id),
                'email',
                 
            ],
            // 'password' => 'required'
            'password' => 'confirmed',
        
            ];
         
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes() {
        return [
            'nome' => 'Nome',
            'email' => 'Email',
            'password' => 'Senha',
          
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
                //
        ];
    }

}
