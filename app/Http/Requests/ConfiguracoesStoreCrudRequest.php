<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConfiguracoesStoreCrudRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nmescola' => 'required|min:3',
            'endereco' => 'required|min:3',
            'telefone' => 'required|min:3',
            'email' => 'email|required|min:3',
            'logo' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:300',
//            'fotosobre' => 'image|mimes:jpeg,png,jpg,gif,svg|max:500',
            
           
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes() {
        return [
           'nmescola' => 'Nome da Escola',
            'endereco' => 'Endereço',
            'telefone' => 'Telefone de Contato 1',
            
            'logo' => 'Logo da Instituição',
//            'fotosobre' => 'Foto da área Sobre',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
               'logo.max' => 'Tamanho máximo de 300Kb',
            'logo.image' => 'Permitido apenas arquivos de imagens',
            'logo.mimes' => 'Extensões permitidas: jpeg,png,jpg,gif,svg',
//            'fotosobre.max' => 'Tamanho máximo de 300Mb',
//            'fotosobre.image' => 'Permitido apenas arquivos de imagens',
//            'fotosobre.mimes' => 'Extensões permitidas: jpeg,png,jpg,gif,svg',
        ];
    }

}
