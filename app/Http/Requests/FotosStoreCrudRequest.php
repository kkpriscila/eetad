<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FotosStoreCrudRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nmfoto' => 'required|min:3',            
            'imagem' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
          
            
           
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes() {
        return [
               'imagem' => 'Foto',
               'nmfoto' => 'Descrição da Foto',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
                  'imagem.max' => 'Tamanho máximo de 10MB',
            'imagem.image' => 'Permitido apenas arquivos de imagens',
            'imagem.mimes' => 'Extensões permitidas: jpeg,png,jpg,gif,svg',
        ];
    }

}
