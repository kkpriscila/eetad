<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request, $guard = null)
    {
        if (! $request->expectsJson()) {
            
//            dd($request->segments()[0]);
            if($request->segments()[0]=='subnucleo'){
                return route("login");
            }elseif($request->segments()[0]=='aluno'){
            return route("aluno.login");
            }elseif($request->segments()[0]=='admin'){
            return route("admin");
            }else{
        
            return route('login');
            }
            
        }
    }
}
