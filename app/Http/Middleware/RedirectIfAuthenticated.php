<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
//        if (Auth::guard($guard)->check()) {
////            return redirect('/admin');
////             return redirect()->route("{$guard}.home");
//             return redirect()->route("home");
//        }
//         if (Auth::guard('subnucleo')->check()) {
//             return redirect()->route("home");
//        }elseif (Auth::guard('alunos')->check()) {
//             return redirect()->route("aluno.home");
//        }elseif (Auth::guard('web')->check()) {
//             return redirect()->route("admin.home");
//        }

        if (Auth::guard($guard)->check()) {
//            dd($guard);
            if($guard=='subnucleo'){
                return redirect()->route("home");
            }else{
            return redirect()->route("{$guard}.home");
            }
        }

        return $next($request);
    }

}
