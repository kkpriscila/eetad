<?php

namespace App\Http\Middleware;

use Closure;


class AdminMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
       
      //  dd(count($request->segments()));
        if(count($request->segments())>1){
         if(backpack_auth()->user()->tipo=="QUIZ" or backpack_auth()->user()->tipo=="QUIZ REVISOR"){
              if($request->segments()[1]=='user' or $request->segments()[1]=='configuracao'
                      or $request->segments()[1]=='fotos'
                      or $request->segments()[1]=='monitores'
                      or $request->segments()[1]=='subnucleos'
                      or $request->segments()[1]=='aluno'
                      or $request->segments()[1]=='matricula'
                      or $request->segments()[1]=='matricula-aprovada'
                      or $request->segments()[1]=='matricula-reprovada'
                      ){
                   return redirect('/admin');
              }
         }
         elseif(backpack_auth()->user()->tipo=="SECRETARIA"){
             if($request->segments()[1]=='user' or $request->segments()[1]=='configuracao'){
                   return redirect('/admin');
              }
         }
            // return response('Não autorizado',401);
        }
    
    return $next($request);
    }
}
