-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subnucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp_subnucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subnucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciclo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uf` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtnasc` date DEFAULT NULL,
  `cidnasc` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ufnasc` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nacionalidade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estcivil` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identidade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `igreja` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cargo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qualcargo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `escolaridade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profissao` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formacaoteologica` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `escola` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dttaxamatricula` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motivotxmatricula` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade_eetad` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matricula`
--

LOCK TABLES `matricula` WRITE;
/*!40000 ALTER TABLE `matricula` DISABLE KEYS */;
INSERT INTO `matricula` VALUES (4,NULL,NULL,NULL,NULL,NULL,NULL,'Carla Priscila',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'kkpriscila@yahoo.com.br',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-27 04:54:52','2020-09-30 01:09:02',NULL,'$2y$10$jxnznhPYcAcL5rwdFmR96ubOfHRX7M3XYBVggBeNZLX6aEVSYszkO','MhQDAbb9zFuN22eshzheNFCb81mqUcwYjcWN3x8L8YPvYGU4gmhsgEyUM5Gi','3629008007133370','https://graph.facebook.com/v3.3/3629008007133370/picture?type=normal',NULL,'facebook',NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,'Reginaldo Malaquias',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'reginaldomalaquiasadv@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-29 21:40:50','2020-09-29 21:40:50',NULL,NULL,'Wce1LsnjlvOlSOHjZrgylOFyHyETXFcg9tZKXcdLmlbBUahTFKwdgiIP2Lcv','113418254674970551864','https://lh4.googleusercontent.com/-1KpD0uk071A/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckCrEcduFfnqE9Z8JQjEpYWOfDWfQ/photo.jpg',NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,'Eetad 0325',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'eetad0325@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-29 21:48:49','2020-09-29 21:48:49',NULL,NULL,'HKLEq24BxgEPv5tqLTSqyYe1ctGvdcyOgjJaFtQ134KO8ct0tM0iop5wTQob','117172753363111895611','https://lh3.googleusercontent.com/a-/AOh14Gj6RVSTVB5NnkH-PSnzt3PuCsn68gUoVks0j2PV',NULL,NULL,NULL),(7,NULL,NULL,NULL,'1º Ciclo Básico','MG',NULL,'Carla Priscila de Morais Mendes','Rua Arpoador','243','Gioavanini','Coronel Fabriciano','MG','35170-104','(31) 98331-7487','1986-07-31','Timóteo','MG','Brasileira','Solteira','carlapriscilam@gmail.com','MG12654166','084.228.616-07','Feminino','Sede Fabriciano','Não',NULL,'Superior','Analista de Sistemas','Não',NULL,NULL,NULL,'2020-09-29 23:21:13','2020-10-03 20:34:12',NULL,NULL,'pMRa2av9yGjUKPOFNdYM0t3yAHoQzrrwN6qT19zGgC5nRvXTNPI3kJxyd5rP','108184707452964343525','uploads/avatar/62642b3289e331d8084993b5982b7578.jpg','PENDENTE',NULL,'Ipatinga'),(9,NULL,NULL,NULL,NULL,NULL,NULL,'Carla@camara',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'carla@camaraipatinga.mg.gov.br',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-03 17:50:22','2020-10-03 17:50:22',NULL,'$2y$10$Os5WtJvbpBKSzZeAsmSb1uKxua/d9PesG6qrQqXi2eDCRW0tsScem','0hKWx8eLNv8n7lXN6nX6oK86xr4YGqRBFH74lVkWXTiOpVyIJNJnLKwYkRvN',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `matricula` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:04
