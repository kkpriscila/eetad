-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `monitores`
--

DROP TABLE IF EXISTS `monitores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subnucleo_id` int(10) unsigned NOT NULL,
  `dtnasc` date DEFAULT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitores`
--

LOCK TABLES `monitores` WRITE;
/*!40000 ALTER TABLE `monitores` DISABLE KEYS */;
INSERT INTO `monitores` VALUES (1,'PR. JOSÉ MARTINS DE CALAIS JÚNIOR','DIRETOR DO  NUCLEO','uploads/monitores/bf82e7432a9d16e1087a181eb6db1fe6.jpeg',NULL,NULL,NULL,NULL,'2020-01-25 17:25:13','2020-03-07 00:36:38',3,NULL,'Diretoria'),(2,'PR.  ALEXSANDRO ALVES DE MEDEIROS','COORDENADOR  DO NUCLEO','uploads/monitores/150c99dc966bebd44a7b8506f3392365.jpg',NULL,NULL,NULL,NULL,'2020-01-25 17:26:20','2020-03-07 00:37:08',3,NULL,'Diretoria'),(4,'ELIENE MARQUES DE SOUZA','SECRETÁRIA','uploads/monitores/55aa129193a24ea5963c20eb93ffd8b6.jpg',NULL,NULL,'nuc.0325@eetad.com.br',NULL,'2020-01-25 17:31:58','2020-03-06 20:59:39',3,NULL,'Diretoria'),(6,'GISLANE MARQUES DE SOUZA COSTA','SECRETÁRIA','uploads/monitores/cb30ed420b4754a8bba7ea2b58b469df.jpeg',NULL,NULL,NULL,NULL,'2020-03-06 20:24:47','2020-03-06 20:42:59',3,NULL,'Diretoria'),(8,'Pr Alexsando Alves','Monitor','uploads/monitores/aef5d537c047c1fd739243fb8e6b6847.jpeg',NULL,NULL,NULL,'Pr Regional AD parque Caravelas. Técnico em Agrimensura com ÊNFASE em GEORREFERENCIAMENTO. Graduando em ciências contábeis pela Estácio de Sá. Curso Médio em Teologia/EETAD. Bacharel em Teologia/SEBEMG. Bacharel em Teologia/UCESP-SP.','2020-03-07 19:35:56','2020-03-07 19:41:26',3,NULL,'Monitor'),(9,'Pr. Wallace de oliveira','Monitor','uploads/monitores/94002ff9604734d512859ca0c0d4efa8.jpeg',NULL,NULL,NULL,'Bacharel em teologia pelo IBAD - Instituto Bíblico das Assembleias de Deus.\r\nPedagogo pela Unileste.','2020-03-07 19:36:46','2020-03-08 14:01:29',3,NULL,'Monitor'),(10,'Dc. Marlon Ferreira','Monitor','uploads/monitores/6ca14f555e60efde6a6ed6abe902d55d.jpeg',NULL,NULL,NULL,'Bacharel em Engenharia Mecânica, Faculdade Pitágoras.\r\nCursando Bacharel em Teologia - Uninter.\r\nCursando Bacharel em Engenharia Civil - Faculdade Pitágoras','2020-03-07 19:39:45','2020-03-07 19:58:53',3,NULL,'Monitor'),(11,'Teka Breder','Monitor','uploads/monitores/8b67bf2753ea22a66a33192e40a30ef1.jpeg',NULL,NULL,NULL,'Pedagogia. Bacharel em teologia por Ucesp.\r\nCurso Médio EETAD\r\nCurso Bacharel Teologia Igreja Batista Shalom','2020-03-07 21:23:38','2020-03-09 19:45:18',3,NULL,'Monitor'),(12,'Pr. Juvenil Machado','Monitor','uploads/monitores/fff56678bc1b4056638916b0e85b4e5d.jpeg',NULL,NULL,NULL,'Palestrante\r\nCurso Médio Teologia EETAD','2020-03-09 19:40:14','2020-03-09 19:46:00',3,NULL,'Monitor'),(13,'Pr. Rinaldo Costa Rodrigues','Monitor','uploads/monitores/06de6db696b3c57fd048283f4f7b59d2.jpeg',NULL,NULL,NULL,'Pastor Regional AD Veneza Ii - Rua Caxias do Sul.\r\nBacharel em Teologia - FATI - Faculdade de Teologia de Ipatinga','2020-03-09 19:44:26','2020-03-09 19:46:37',3,NULL,'Monitor'),(14,'Ev. Ildomar José Correia','Monitor','uploads/monitores/40ab41a28fddd106ed8a217bc390e756.jpeg',NULL,NULL,NULL,'Bacharel em Teologia pela FAETAD, convalidação de créditos pela FTSA, diploma de bacharel creditado pela mesma.\r\nPós-graduação em ensino religioso pela UNIDA, Vitoria ES, e pós-graduação em Capelania pela FTSA, Londrina, Paraná.','2020-03-09 20:00:08','2020-03-09 20:00:08',3,NULL,'Monitor'),(15,'Pr. Julio Cesar Nogueira','Monitor','uploads/monitores/d45a49caa992abb8c3fb785edfc6304f.jpeg',NULL,NULL,NULL,'Pastor Regional AD bairro Esperança\r\nBacharel em Teologia\r\nPós Graduado em Ciências da Religião.\r\nPós Graduado em Ensino de Filosofia','2020-03-09 20:09:33','2020-03-09 20:09:33',3,NULL,'Monitor'),(16,'Pr. Gilmar Bicalho  Maia','Monitor','uploads/monitores/165ad9dc211eb7f22550f6d5f361c45f.jpeg',NULL,NULL,NULL,'Pr. Regional AD Iguaçu\r\nCursando Bacharel em Teologia FAETAD','2020-03-09 20:13:48','2020-03-09 20:13:48',3,NULL,'Monitor'),(17,'Ev. José dos Reis Lopes','Monitor','uploads/monitores/52868861d13cb1e88db730cafec6064b.jpeg',NULL,NULL,NULL,'Bacharel em Teologia - FAETAD\r\nGraduações Direito e Comércio Exterior.\r\nPós -Graduação: Engenharia de Produção','2020-03-09 20:17:26','2020-03-09 20:17:26',3,NULL,'Monitor'),(18,'Gilmar Alves','Monitor','uploads/monitores/dfb29782adfa6bc561b43b149f656205.jpeg',NULL,NULL,NULL,'Seminarista\r\nPalestrante\r\nCurso Básico EETAD','2020-03-09 20:19:38','2020-03-09 20:19:38',3,NULL,'Monitor'),(19,'Pb. Jorge A. de Paula','Monitor','uploads/monitores/a44dd58467d1d1653ef97e585e6cb988.jpeg',NULL,NULL,NULL,'Cursando Médio EETAD','2020-03-09 20:22:12','2020-03-09 20:22:12',3,NULL,'Monitor'),(20,'Ronald de Sá','Monitor','uploads/monitores/1d71f3e021bd5a2a00b4cafcb5005837.jpeg',NULL,NULL,NULL,'Bacharel Livre em Teologia - ITMO','2020-03-09 20:25:19','2020-03-09 20:25:19',3,NULL,'Monitor'),(21,'Wlysses Lopes Viana','Monitor','uploads/monitores/f41aab746cfd7449eece185117d1a4bd.jpeg',NULL,NULL,NULL,'Superior em Teologia\r\nSuperior em Direito','2020-03-09 20:36:57','2020-03-09 20:36:57',3,NULL,'Monitor'),(22,'Pb. Abmael Galdino','Monitor','uploads/monitores/28a0fea9a4fc25554868863645439874.jpeg',NULL,NULL,NULL,'Cursando Bacharel FAETAD','2020-03-09 21:17:03','2020-03-09 21:17:03',3,NULL,'Monitor');
/*!40000 ALTER TABLE `monitores` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:00
