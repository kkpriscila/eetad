-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2014_10_12_100000_create_password_resets_table',1),(3,'2020_01_25_152822_create_configuracoes',1),(4,'2020_01_25_160741_create_fotos',1),(5,'2020_01_25_164243_create_monitores',1),(6,'2020_02_22_161320_create_subnucleo',2),(7,'2020_02_22_162713_add_dados_monitores',2),(8,'2020_02_22_185408_add_tipo_monitores',2),(9,'2020_02_26_014623_add_zap-face_configuracoes',2),(10,'2020_03_01_125527_add_home_fotos',2),(12,'2014_10_12_000000_create_users_table',3),(13,'2020_03_13_120055_add_user_subnucleos',4),(14,'2020_03_13_120313_create_subnucleo_password_resets',4),(16,'2020_08_26_224436_add_livro_matricula',5),(21,'2020_09_05_125137_create_categoria_quiz',6),(22,'2020_09_05_161417_create_perguntas_quiz',6),(23,'2020_09_05_162340_create_perguntas_quiz',7),(24,'2020_09_05_162711_create_quiz_respostas',8),(25,'2020_09_17_214732_add_tipo_dtexpira_usuario',9),(26,'2020_09_22_200102_add_created_updated_user_quizperguntas',10),(29,'2020_05_06_053709_create_matricula',11),(30,'2020_09_23_130538_add_user_matricula',11),(31,'2020_09_23_130708_create_matricula_password_resets',11),(32,'2020_09_27_020041_add_provider_id_matricula',12),(33,'2020_09_29_225202_add_status_matricula',13),(34,'2020_09_30_010454_add_provider_matricula',14),(35,'2020_10_03_160513_change_cidade_eetad',15);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:08:53
