-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `quiz_respostas`
--

DROP TABLE IF EXISTS `quiz_respostas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_respostas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resposta` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `certa` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_respostas`
--

LOCK TABLES `quiz_respostas` WRITE;
/*!40000 ALTER TABLE `quiz_respostas` DISABLE KEYS */;
INSERT INTO `quiz_respostas` VALUES (1,'R1','N',1,'2020-09-05 18:57:59','2020-09-05 18:57:59'),(2,'R2','S',1,'2020-09-05 18:59:12','2020-09-05 18:59:12'),(3,'R3','N',1,'2020-09-05 18:59:21','2020-09-05 18:59:21'),(5,'R5','S',1,'2020-09-05 19:01:34','2020-09-17 23:13:33'),(6,'Resposta 1','S',2,'2020-09-06 08:16:55','2020-09-06 08:16:55'),(7,'Resposta 2','N',2,'2020-09-06 08:17:07','2020-09-06 08:17:07'),(8,'Resposta 3','N',2,'2020-09-06 08:18:22','2020-09-06 11:11:56'),(9,'Resposta 1','N',3,'2020-09-06 08:20:01','2020-09-06 11:12:15'),(10,'Resposta 2','S',3,'2020-09-06 08:20:13','2020-09-06 11:12:25'),(11,'Resposta 3','N',3,'2020-09-06 08:20:26','2020-09-06 08:20:26'),(12,'Resposta 1','N',4,'2020-09-06 08:21:12','2020-09-06 08:21:12'),(13,'Resposta 3','S',4,'2020-09-06 08:21:23','2020-09-06 08:21:23'),(14,'Resposta 2','N',4,'2020-09-06 08:21:37','2020-09-06 08:21:37');
/*!40000 ALTER TABLE `quiz_respostas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:02
