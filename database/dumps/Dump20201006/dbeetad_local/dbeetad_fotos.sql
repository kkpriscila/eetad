-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagemp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nmfoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `home` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (3,'uploads/fotos/aef35deafaebe73ca8ebadfbb4e3d4e4.jpg','/uploads/fotos/thumbs/Jovens Betrhania_1583444209.jpg','VISITA CLASSE DOS JOVENS REGIONAL BETHANIA','2020-01-25 16:36:33','2020-03-05 21:36:59','1'),(4,'uploads/fotos/20845b59bbf69bdd82021e1a6d1b016c.jpeg','/uploads/fotos/thumbs/JOVENS VILA MILITAR_1583444164.jpeg','VISITA A CLASSE DOS JOVENS VILA MILITAR','2020-01-25 16:39:23','2020-03-05 21:36:14','1'),(5,'uploads/fotos/543da2ff8564cf33699b6501a17cf28a.jpeg','/uploads/fotos/thumbs/Limoeiro_1583443729.jpeg','VISITA AO SUBNÚCLEO LIMOEIRO','2020-01-25 16:39:51','2020-03-05 21:28:59','1'),(6,'uploads/fotos/5e3e3b52f648afe624c0fe0419a898e7.jpg','/uploads/fotos/thumbs/Adolescente vila militar_1583443533.jpg','VISITA AOS ADOLESCENTES VILA MILITAR','2020-01-25 16:40:07','2020-03-05 21:35:25','1'),(7,'uploads/fotos/e2da4356373811a346ecb11a3e881ff1.jpeg','/uploads/fotos/thumbs/curso Medio II Sede_1583443396.jpeg','CURSO MÉDIO II- SEDE - IPATINGA','2020-01-25 16:41:02','2020-03-05 21:23:29','1');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:08:46
