-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subnucleo` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `resp_subnucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subnucleo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciclo` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cidade-eetad` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `nucleo` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `uf` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `dtnasc` date NOT NULL,
  `cidnasc` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `ufnasc` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `nacionalidade` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `estcivil` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identidade` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `igreja` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qualcargo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `escolaridade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profissao` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formacaoteologica` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `escola` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `dttaxamatricula` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motivotxmatricula` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `livro` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matricula`
--

LOCK TABLES `matricula` WRITE;
/*!40000 ALTER TABLE `matricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `matricula` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:15
