-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagemp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nmfoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `home` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (3,'uploads/fotos/aef35deafaebe73ca8ebadfbb4e3d4e4.jpg','/uploads/fotos/thumbs/Jovens Betrhania_1583444209.jpg','VISITA CLASSE DOS JOVENS REGIONAL BETHANIA','2020-01-25 16:36:33','2020-03-05 21:36:59','1'),(4,'uploads/fotos/20845b59bbf69bdd82021e1a6d1b016c.jpeg','/uploads/fotos/thumbs/JOVENS VILA MILITAR_1583444164.jpeg','VISITA A CLASSE DOS JOVENS VILA MILITAR','2020-01-25 16:39:23','2020-03-05 21:36:14','1'),(5,'uploads/fotos/543da2ff8564cf33699b6501a17cf28a.jpeg','/uploads/fotos/thumbs/Limoeiro_1583443729.jpeg','VISITA AO SUBNÚCLEO LIMOEIRO','2020-01-25 16:39:51','2020-03-05 21:28:59','1'),(6,'uploads/fotos/5e3e3b52f648afe624c0fe0419a898e7.jpg','/uploads/fotos/thumbs/Adolescente vila militar_1583443533.jpg','VISITA AOS ADOLESCENTES VILA MILITAR','2020-01-25 16:40:07','2020-03-05 21:35:25','1'),(7,'uploads/fotos/e2da4356373811a346ecb11a3e881ff1.jpeg','/uploads/fotos/thumbs/curso Medio II Sede_1583443396.jpeg','CURSO MÉDIO II- SEDE - IPATINGA','2020-01-25 16:41:02','2020-03-05 21:23:29','1'),(12,'uploads/fotos/8f1f39792c7b9f055f4a533d95d835a8.jpeg','/uploads/fotos/thumbs/cachoeira Escura_1583866667.jpeg','Subnúcleo Cachoeira Escura - Básico','2020-03-10 18:58:01','2020-03-10 18:58:01','0'),(13,'uploads/fotos/67f69f155a7bd6831c22f0644b76ad43.jpeg','/uploads/fotos/thumbs/belo oriente medio_1583866804.jpeg','Subnúcleo Belo Oriente Curso Médio II','2020-03-10 19:00:21','2020-03-10 19:00:21','0'),(14,'uploads/fotos/10955e39bcbdb29c126a79694778a445.jpeg','/uploads/fotos/thumbs/Belo Oriente_1583866843.jpeg','Subnúcleo  - Regional Belo Oriente - Basico','2020-03-10 19:00:52','2020-03-10 19:00:52','0'),(15,'uploads/fotos/5f8f080486aa5a03d13abebe5d5e5c61.jpeg','/uploads/fotos/thumbs/vila militar_1583866949.jpeg','Subnúcleo Vila Militar - Básico','2020-03-10 19:03:35','2020-03-10 19:03:35','0'),(16,'uploads/fotos/ccf4e80413f355de41e66be42c85db8a.jpeg','/uploads/fotos/thumbs/vila militar_1583867021.jpeg','Subnúcleo Vila Militar - Básico','2020-03-10 19:03:49','2020-03-10 19:03:49','0'),(17,'uploads/fotos/4a500fbc353bc06c697d26df08aa32c3.jpeg','/uploads/fotos/thumbs/Pq Caravelas_1583867095.jpeg','Subnúcleo Parque Caravelas - Básico','2020-03-10 19:05:10','2020-03-10 19:05:10','0'),(18,'uploads/fotos/be36cb049ea12fa6714ec02dd14a2b4c.jpeg','/uploads/fotos/thumbs/limoeiro basico_1583867161.jpeg','Subnúcleo Limoeiro - Básico','2020-03-10 19:06:10','2020-03-10 19:06:10','0'),(19,'uploads/fotos/5596e408346ad6fcaa7dbe22b64e1185.jpg','/uploads/fotos/thumbs/EETAD 058_1584042900.jpg','CURSO FAETAD- Sede Centro de Ipatinga','2020-03-12 19:55:50','2020-03-12 19:55:50','0'),(20,'uploads/fotos/f0389a30b96554efe847a7a6a90d06c7.jpg','/uploads/fotos/thumbs/EETAD 068_1584043044.jpg','CURSO BÁSICO - Sede Centro Ipatinga','2020-03-12 19:57:33','2020-03-12 19:57:33','0'),(21,'uploads/fotos/0d866c53725f958240a0947d665b9e7d.jpg','/uploads/fotos/thumbs/EETAD 069_1584043109.jpg','CURSO MÉDIO II - Sede Centro Ipatinga','2020-03-12 19:58:38','2020-03-12 19:58:38','0'),(22,'uploads/fotos/e5a594ec011918e916be347c361462e4.jpg','/uploads/fotos/thumbs/EETAD 048_1584043300.jpg','Subnúcleo Vila Celeste - Básico','2020-03-12 20:01:49','2020-03-12 20:01:49','0'),(23,'uploads/fotos/ec318b68e71bde122f2aa15d51cd920e.jpeg','/uploads/fotos/thumbs/WhatsApp Image 2020-04-14 at 17.14.29_1586895734.jpeg','Subnucleo Parque Caravelas','2020-04-14 20:22:49','2020-04-14 20:22:49','0'),(24,'uploads/fotos/7e6a6860392ff0021536a2a85a97e1e7.jpeg','/uploads/fotos/thumbs/caladinho baixo_1586895952.jpeg','Subnúcleo Caladinho de Baixo','2020-04-14 20:26:03','2020-04-14 20:26:03','0'),(25,'uploads/fotos/47d8f6b6c3c013e47d7f53bcb2733d7e.jpeg','/uploads/fotos/thumbs/Fazenda Esperança_1586896208.jpeg','Subnúcleo Fazenda Esperança','2020-04-14 20:30:18','2020-04-14 20:30:18','0'),(27,'uploads/fotos/3580142d7f90f28bb535b3bfa6b72d23.jpeg','/uploads/fotos/thumbs/niver pastor alexsandro 1_1588621451.jpeg','Aniversario de nosso COORDENADOR Pr. Alexsandro','2020-04-20 19:36:24','2020-05-04 19:44:19','0'),(28,'uploads/fotos/b778540d413830387b1a8df990612bda.jpeg','/uploads/fotos/thumbs/subnucleo vagalume_1588635716.jpeg','Subnúcleo Regional Vaga lume - Ipatinga','2020-05-04 23:42:09','2020-05-04 23:42:09','0'),(29,'uploads/fotos/8f048da0ff5355eb926651efc5ac51df.jpeg','/uploads/fotos/thumbs/medio II centr0505_1588713008.jpeg','Visita curso Medio II - 04/05/2020','2020-05-05 21:10:18','2020-05-05 21:10:18','0'),(30,'uploads/fotos/adef842775c08e7d0c3d12f9a92b3488.jpeg','/uploads/fotos/thumbs/basico centro 0505_1588713076.jpeg','CURSO Básico- SEDE - IPATINGA 04/05/2020','2020-05-05 21:11:26','2020-05-05 21:11:26','0'),(31,'uploads/fotos/2950b0aac97f97d8cc0af725ceb29fc6.jpeg','/uploads/fotos/thumbs/Curitiba eetad_1591226900.jpeg','Subnúcleo  - Rua Curitiba - Veneza II - Basico','2020-06-03 23:28:29','2020-06-03 23:28:29','0'),(32,'uploads/fotos/55a1847db620fd4b16911b029423e283.jpg','/uploads/fotos/thumbs/GREGO faetad_1591228435.jpg','Aula FAETAD- GREGO - Centro Ipatinga','2020-06-03 23:54:04','2020-06-03 23:54:04','0'),(33,'uploads/fotos/8844ad8583ce57e71335a54c58fb31aa.jpg','/uploads/fotos/thumbs/FAETAD GREGO_1591228672.jpg','Alunos FAETAD','2020-06-03 23:58:01','2020-06-03 23:58:01','0'),(35,'uploads/fotos/7d09edd38c8c3a89d3988cd437c29c9f.jpeg','/uploads/fotos/thumbs/iguaçu_1592246740.jpeg','Subnúcleo  - RegionalIguaçu - Basico','2020-06-15 18:45:49','2020-06-15 18:45:49','0'),(36,'uploads/fotos/89a7bc67e7893a37015aa9e35b216795.jpeg','/uploads/fotos/thumbs/caxias_1593026828.jpeg','Subnúcleo  - Rua Caxias do Sul - Veneza II - Basico e Medio II','2020-06-24 19:27:53','2020-06-24 19:27:53','0'),(37,'uploads/fotos/2feb596f6355a2763aa28532d5fc694e.jpeg','/uploads/fotos/thumbs/vila militar alunos_1595961077.jpeg','Visita ao Subnúcleo Vila Militar','2020-07-28 18:31:26','2020-07-28 18:31:26','0'),(38,'uploads/fotos/df91c9e795b9722255c409818e8c55bc.jpeg','/uploads/fotos/thumbs/vila celeste_1596050861.jpeg','Sub-núcleo - Vila Celeste - Curso Básico','2020-07-29 19:27:51','2020-07-29 19:27:51','0');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:03
