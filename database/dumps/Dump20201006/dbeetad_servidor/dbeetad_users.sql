-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtexpira` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Carla','carlapriscilam@gmail.com','2020-02-06 13:03:42','$2y$10$U5b1/sJ/25u/cUDXs3BQxOhNX9CRNBvj/ut7ftdP1b/Eh72j/SpBy','SNtZEKz5Zz8xlpNMwco0qOomHlUfFuu5SfHlkDLyf6qimZEbnxIn7JXVmZyP','2020-02-06 13:03:42','2020-09-23 23:38:54','ADMINISTRADOR',NULL),(2,'Alexsandro','alexsandroalvesdemedeiros@hotmail.com',NULL,'$2y$10$NSd90kNiWbKBSw70l1iGieDykjHvfHyrqzvSd9gQ88EL8n.qIPohC','jRINC2H9u6TCzWssAaR2YavbewAuKaONfjiaT1wR5J2RIttjaKjiJKsgeG7B','2020-03-05 17:35:50','2020-09-23 00:04:46','ADMINISTRADOR',NULL),(3,'Secretaria Eetad 0325','nuc.0325@eetad.com.br',NULL,'$2y$10$aZPLvisrdNgZzDoynCwurONPdnDm8PlpHDoSER6Osx/b3qSrwImeK','mGJ6k0BA0jrs2y3xj07dzXmZaaENFefSOo4AoN3I2OSFKM6WL5pW1EwYeIbi','2020-03-05 17:36:53','2020-09-23 00:04:17','ADMINISTRADOR',NULL),(4,'Teste Quiz','carla@camaraipatinga.mg.gov.br',NULL,'$2y$10$B/LUIk3NurB4ldXPey7Imup9amaqEA31pn/jc7bo.8/IoqHEySaq2','KKuTcjHnfaBE9TvwWgzZNhaRB52X3R19ie9rAqYLPKJcS7Q265JQfhXmkkAH','2020-09-17 22:05:56','2020-09-17 22:05:56','QUIZ','2020-10-30'),(5,'Walace Oliveira','walahon@yahoo.com.br',NULL,'$2y$10$icYfKA7C3GFIlRKN1OvB1Ovb5lE6HY7zAUPVAfLpSkYfFNPIQIq8i',NULL,'2020-10-05 23:08:08','2020-10-05 23:08:08','QUIZ',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:17
