-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `subnucleo`
--

DROP TABLE IF EXISTS `subnucleo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subnucleo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `responsável` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subnucleo`
--

LOCK TABLES `subnucleo` WRITE;
/*!40000 ALTER TABLE `subnucleo` DISABLE KEYS */;
INSERT INTO `subnucleo` VALUES (3,'SEDE IPATINGA - Curso Básico - Medio II e FAETAD','Eliene e Gislane','Rua Viçosa - 54- centro Ipatinga- MG -','2020-03-05 18:47:31','2020-08-26 19:14:06','lnmarquesvitorio@gmail.com',NULL,'$2y$10$xlUzEMMnDjw4omU/MO.v7uNcmnKIHPh3QLSD17c87N.ldJO2E6mlO','bzZhynyIDmVJRNrX7xZiuc46GfgN0jRsJzCKzgSa1LwKDaXZcACVCEZgjaFt'),(4,'CEL. FABRICIANO - Curso Básico e Médio - aulas segunda-feira- 19hs','José Batista - 985964082','Av. magalhães Pinto - 419 - Alberto Giovannini - Cel. Fabriciano - MG','2020-03-05 18:51:55','2020-08-26 22:38:27','Jobasasantos@gmail.com',NULL,'$2y$10$b8hDMNToLn7KpQcRMsnH7enEs1ZkUmN4pcbITN1v/u/EpkIg6JWtm',NULL),(5,'ESPERANÇA - Curso Básico - aulas segunda-feira- 19hs','Paulo e Gláucia','Av. Esperança','2020-03-05 18:52:34','2020-08-26 20:42:20','glauciabeatrizcarvalho@gmail.com',NULL,'$2y$10$neY4iyc69ZN.MOYoYDnfD.SlTiguFAZ3pM3m3dWGhA4KrbRkprkHW',NULL),(6,'BELO ORIENTE - Curso Básico e Médio II - aulas segunda-feira - 19hs','José Inacio e Francisco',NULL,'2020-03-05 18:53:50','2020-08-26 22:00:20','Jicarmo@yahoo.com.br',NULL,'$2y$10$pRfpDIBi1I9tLgl0yeEI.OJXg6.gL.D1w3uwsWltZpyZcqMEHqWv.',NULL),(7,'VILA CELESTE - Curso Básico - aulas  Terça-feira - 19hrs','Maurina','Av. Luíza Nascimbene - 516- Vila Celeste - Ipatinga','2020-03-05 18:56:10','2020-03-05 20:17:27','',NULL,'',NULL),(8,'IGUAÇU- Curso Básico - aulas terça-feira- 19hs','Joscelino','Rua: Quartzo','2020-03-05 18:58:08','2020-03-05 20:19:02','',NULL,'',NULL),(9,'INDUSTRIAL - Curso Básico - aulas segunda-feira- 19hs','Bernardo e Mazildes','Av. Conselheiro Lafaiete','2020-03-05 19:34:19','2020-03-05 20:22:50','',NULL,'',NULL),(10,'LIMOEIRO - Curso Básico e Médio II - Aulas Basico Segunda- feira 19 hs - Medio II Sabado as 16 hs','Lidia e Libne','Av. José Anatólio Barbosa - 1656 - Limoeiro- Ipatinga','2020-03-05 19:37:20','2020-08-26 22:39:49','lidiarodrigues3996@gmail.com',NULL,'$2y$10$/.BcXL7OJtEdHolZkkPVTeTtkTn9LF2t0ltDM3KRM6LEjvotqlF.W',NULL),(11,'CANÃAZINHO - Curso Básico - Aulas Segunda-feira 19hs','Pb. Hercules','Rua: Magdala','2020-03-05 20:00:09','2020-03-05 20:20:29','',NULL,'',NULL),(12,'VENEZA- CAXIAS DO SULl - Curso Básico e Médio II','Pb. Geraldo Bianor - Pb. Hebert e Icaro','Rua: Caxias   - Veneza II - Ipatinga- MG','2020-03-05 20:01:52','2020-08-31 21:50:56','geraldobianor2017@gmail.com',NULL,'$2y$10$iq1NU5kCnYfi9V.e0h131OG6oDS2cTo0dH7wEi96lg6u.M4fHTwpa',NULL),(13,'CACHOEIRA ESCURA- Curso Básico e Médio II','Pb. Elton e Mateus',NULL,'2020-03-05 20:03:07','2020-08-26 21:09:00','Alinikaleby@gmail.com',NULL,'$2y$10$/rDz1vYtkBgNtgsL6OzPs.C5h2a2ZJiTotJEIXg.VWcr6SGTdm.By',NULL),(14,'NAQUE - Curso Básico','Robson',NULL,'2020-03-05 20:03:28','2020-03-05 20:21:51','',NULL,'',NULL),(15,'CALADINHO DE CIMA - Curso Básico - aulas segunda-feira - 19hs','Pb. Elio','Av. Tancredo Neves','2020-03-05 20:03:56','2020-03-05 20:22:34','',NULL,'',NULL),(16,'CALADINHO DE BAIXO - Curso Básico - aulas Segunda-feira- 19 hs','Leidiane e Leiliziane','leidylima@hotmail.com','2020-03-05 20:04:59','2020-08-31 21:50:09','leidylima@hotmail.com',NULL,'$2y$10$SZY/6IKuYUqYR3253cmC2evwN1AtIUvXSEvPELE.67IqWNM4pPd6O',NULL),(17,'BETHÃNIA - Curso Básico','Geane e Gabriela','Av. alberto Giovaninne','2020-03-05 20:05:40','2020-08-31 21:51:46','geanyfm@hotmail.com',NULL,'$2y$10$QzlbraYZzUYukAtapx1HF.DhwBNXoyoMgwMTj9sguFPKnOP4YSpJK',NULL),(18,'VILA MILITAR- Curso Básico - Aulas Segunda-feira - 19hs','Dc. Edilon e Cleber','Rua: Nova Iorque','2020-03-05 20:07:12','2020-08-26 20:40:53','ccpribeiro2009@hotmail.com',NULL,'$2y$10$zECwXO0TKoKdjFkUt/I7zugKOKZbZVlNbHTzO4HgDPFpYEKHu4yma','OpvWLtna9p3QT5G1HjlISxE6UnyEm2oAgp3oG5eiHMXIJJtzhS6nTB4yFxGB'),(19,'PARQUE CARAVELAS - curso Básico e Medio - aulas Básico Sexta-feira 19 hs - Médio II quarta-feira - 19hs','Marcos','Av. Mogi Mirim','2020-03-05 20:12:37','2020-03-05 20:12:37','',NULL,'',NULL),(20,'VAGALUME - Curso Básico - aulas - segunda-feira 19hs','Ronilda',NULL,'2020-03-05 20:13:38','2020-08-31 21:52:36','ronildavenancio715@gmail.com',NULL,'$2y$10$6L.LL6/7qOv6To46RH6Nceg6DLwBDvY0uPmPvIP1alPLqxoFpbiPa',NULL),(21,'SANTA CRUZ - Curso Básico e Medio II - aulas segunda-feira - 19hs','José Câncio, Fernanda e Norma','Rua: Suecia - 180- Santa Cruz - Cel. Fabriciano','2020-03-05 20:26:20','2020-08-31 22:01:51','fernandaantonieta12@gmail.com',NULL,'$2y$10$CDm7PRRrzb78MCh.6QUgJuvLH2voPgWPC79AzTwo86CB0h7hfNPPe',NULL),(22,'VENEZA CURITIBA - Cusrso Básico - aulas quarta-feira- 19;30 hs','Jesse e Eunice','Rua.; Curitiba','2020-03-05 20:27:38','2020-03-05 20:27:38','',NULL,'',NULL),(23,'CIDADE NOVA - Curso Básico -','Dc. Elton',NULL,'2020-03-05 20:28:45','2020-03-05 20:28:45','',NULL,'',NULL),(24,'IGREJA BATISTA NOVA ALIANÇA- Curso Básico e Médio II- Aulas  básico Segunda e  Medio II Terça- feira- 19:30 hs','Juscimar e Cassia','Av. Ana Moura - 471 - Alvorada- Timoteo- MG','2020-03-05 20:31:32','2020-08-27 21:49:26','juscimar.senra@bol.com.br',NULL,'$2y$10$JX6//gACRya4xEBvHVVsbeGSE9cgakPx.AMqNSJ1iMqgh4DHOKcq6',NULL),(25,'IPABA - Cusro Básico - aulas sabados - 14 hs','Dc. Adonilson',NULL,'2020-03-05 20:35:24','2020-08-26 19:40:31','virgilioadonilson6@gmail.com',NULL,'$2y$10$JL4tuqgC6fVPU.x5AfmkGuTQZFXyr1fk3.WFgxrDH6FOxR/jLU15q',NULL),(26,'JARDIM VITORIA - Curso Básico - aulas quarta-feira - 19hs','Simone','Av. Piquirana - 443 - Jd. Vitoria- Santana do Paraiso','2020-03-05 20:38:10','2020-03-05 20:38:10','',NULL,'',NULL),(27,'VISTA ALEGRE - Curso Básico - aulas quinta-feira- 19hs','Reginaldo e Eliane',NULL,'2020-03-05 20:39:16','2020-08-27 21:47:47','reginaldoramos985@live.com',NULL,'$2y$10$l8cvLuh3kD..UWqDM0y57eEEw6xsiQab/reTH0fryl04T9n5VqvSW',NULL),(28,'IGREJA TOCHA ACESA- Curso Básico - aulas quinta-feira- 19;30 hs','Weverton',NULL,'2020-03-05 20:41:18','2020-08-27 01:56:13','carlapriscilam@gmail.com',NULL,'$2y$10$aHrdYwnKskTtGezAagAnBec1vMR0adlEkmrLMenOuvuQxZ2QRUoq2',NULL),(29,'SANTANA DO PARAISO - Cuso Básico - aulas segunda-feira - 19hs','Cristiane e Wermisson','Av. Getulio Vargas - 330 - Centro - Santana do Paraiso','2020-03-05 20:49:10','2020-08-31 21:46:33','wermisson@live.com',NULL,'$2y$10$NgI2ZPcvl8FjpU56wAXBteka3ytbdNCOGBcER/ASxOje7girPZH5i',NULL),(30,'Belo Oriente - Medio II Francisco','Francisco',NULL,'2020-08-26 19:04:55','2020-08-26 20:43:46','ferreirajoaofrancisco51@gmail.com',NULL,'$2y$10$Nr5WqNQ5xN8spYONCIMPlufhT929fF5bnxLCEa9pj9Z3OkcIsI83m',NULL);
/*!40000 ALTER TABLE `subnucleo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:04
