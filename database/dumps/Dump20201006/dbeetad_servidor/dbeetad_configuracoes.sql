-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nmescola` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txtsobre` text COLLATE utf8mb4_unicode_ci,
  `txtcontato` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
/*!40000 ALTER TABLE `configuracoes` DISABLE KEYS */;
INSERT INTO `configuracoes` VALUES (1,'Núcleo 0325 - Cel. Fabriciano e Ipatinga','uploads/d464e553a85ede89603a7d4ac516e7bd.png','Rua Viçosa, 54 - Centro, Ipatinga / MG - CEP: 35160-028','3138221810 Ramal 27','nuc.0325@eetad.com.br','<p>A Assembleia de Deus Minist&eacute;rio de Cel. Fabriciano e Ipatinga desde o in&iacute;cio da d&eacute;cada de 80 trouxe para a nossa regi&atilde;o um n&uacute;cleo da Eetad. Desde o in&iacute;cio foi um sucesso, v&aacute;rios obreiros do Minist&eacute;rio puderam concluir o curso b&aacute;sico em teologia, e outros o curso m&eacute;dio em teologia. Os anos passaram e centenas de formandos, tanto da Assembleia de Deus como de varias outras denomina&ccedil;&otilde;es conclu&iacute;ram o curso no nosso n&uacute;cleo 325 sediado em Ipatinga-MG. Voc&ecirc; que &eacute; servo de Deus esfor&ccedil;ado e deseja aprofundar seus conhecimentos teol&oacute;gicos, n&atilde;o perca a sua oportunidade. Seja voc&ecirc; um l&iacute;der em sua igreja ou tem algum minist&eacute;rio e necessita aprofundar-se mais no ensino da B&iacute;blia para melhor servir a Deus, este &eacute; o momento, Deus tem uma ben&ccedil;&atilde;o para a sua vida. Talvez voc&ecirc; diga, eu n&atilde;o consigo participar de um curso presencial devido a agenda apartada. A Eetad tem proposta que facilita e flexibiliza a sua participa&ccedil;&atilde;o, busque conhecer o nosso curso. N&atilde;o tem condi&ccedil;&otilde;es financeiras para pagar a mensalidade de uma faculdade porque os valores s&atilde;o altos? A Eetad tem valores acess&iacute;veis para quem quer aprofundar na Palavra de Deus. Portanto esperamos o mais breve poss&iacute;vel que seja um aluno em nossa escola!<br />\r\n<br />\r\nPr Jos&eacute; Martins de Calais J&uacute;nior<br />\r\nDiretor do N&uacute;cleo<br />\r\nCristo o Amigo Fiel!!!</p>',NULL,NULL,'2020-09-11 00:22:59','3171266548','www.facebook.com/eetad.eetad');
/*!40000 ALTER TABLE `configuracoes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:02
