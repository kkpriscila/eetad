-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dbeetad.mysql.dbaas.com.br    Database: dbeetad
-- ------------------------------------------------------
-- Server version	5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `quiz_respostas`
--

DROP TABLE IF EXISTS `quiz_respostas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_respostas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resposta` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `certa` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_respostas`
--

LOCK TABLES `quiz_respostas` WRITE;
/*!40000 ALTER TABLE `quiz_respostas` DISABLE KEYS */;
INSERT INTO `quiz_respostas` VALUES (15,'Gênesis','N',5,'2020-09-17 02:30:42','2020-09-17 02:30:42'),(16,'Exôdo','N',5,'2020-09-17 02:30:53','2020-09-17 02:30:53'),(17,'Jó','S',5,'2020-09-17 02:31:10','2020-09-17 02:31:10'),(18,'Salmos','N',5,'2020-09-17 02:31:28','2020-09-17 02:31:37'),(19,'Deus não esta sujeito a qualquer mudança','N',6,'2020-10-05 22:50:58','2020-10-05 22:50:58'),(20,'A eternidade de Deus é apresentada somente em sentido figurado.','N',6,'2020-10-05 22:51:39','2020-10-05 22:51:39'),(21,'Deus é um ser pessoal','S',6,'2020-10-05 22:52:08','2020-10-05 22:52:08');
/*!40000 ALTER TABLE `quiz_respostas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-06 20:09:12
