<?php

use Illuminate\Database\Seeder;

class ConfiguracoesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
//        factory(App\User::class, 5)->create();
        \App\Models\Configuraco::create([
            'nmescola' => 'EETAD Ipatinga',
            'logo' => 'testedfsdfsdfsdfsdf',
            'endereco' => 'Rua Viçosa, 54 - Centro, Ipatinga / MG - CEP: 35160-028',
            'telefone' => '(31) 3822-2736',
            'email' => 'eetadipatinga@gmail.com',
            'txtsobre' => 'teste',
            'txtcontato' => 'teste',
        ]);
    }

}
