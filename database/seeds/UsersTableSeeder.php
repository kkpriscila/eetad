<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\User::class, 5)->create();
        \App\User::create([
            'name' => 'Carla',
            'email' => 'carla@gmail.com',
            'password' => bcrypt('secret'),//A senha, por padrão, será secret, quando não definida
        ]);
    }
}
