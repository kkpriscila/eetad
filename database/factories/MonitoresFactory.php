<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */


$factory->define(App\Models\Monitore::class, function (Faker $faker) {
    return [

        'nome' => $faker->name,
        'titulo' => $faker->jobTitle,
        'foto' => "uploads/monitores/657d29195cdd4bf048328d173bfd0e72.jpg",
//        'foto' => $faker->image(public_path('uploads/monitores'), 167, 167, null, false),
//        'foto' => $faker->image('/public/uploads/monitores', 167, 167, 'cats') ,
        'endereco' => $faker->address,
        'telefone' => $faker->tollFreePhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'curriculo' => $faker->address,
    ];
});
