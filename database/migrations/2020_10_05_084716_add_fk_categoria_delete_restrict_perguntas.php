<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCategoriaDeleteRestrictPerguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('quiz_perguntas', function($table) {
            $table->foreign('categoria_id')
                    ->references('id')->on('quiz_categoria')
                    ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('quiz_perguntas', function($table) {
            $table->dropForeign('quiz_perguntas_categoria_id_foreign');
        });
    }
}
