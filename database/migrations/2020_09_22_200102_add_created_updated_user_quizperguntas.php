<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedUpdatedUserQuizperguntas extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('quiz_perguntas', function($table) {
            $table->string('created_user')->nullable();
            $table->string('updated_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
         Schema::table('quiz_perguntas', function($table) {
            $table->dropColumn('created_user');
            $table->dropColumn('updated_user');
        });
    }

}
