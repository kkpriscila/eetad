<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderIdMatricula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('matricula', function($table) {
          
            
            $table->string('provider_id',250)->nullable();
            $table->string('avatar',250)->nullable();
            
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matricula', function($table) {
          
            $table->dropColumn('provider_id');
            $table->dropColumn('avatar');
    
        });
    }
}
