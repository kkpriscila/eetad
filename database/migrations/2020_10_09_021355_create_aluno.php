<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAluno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aluno', function (Blueprint $table) {
            $table->increments('id');
             $table->string("nome");
            $table->string("end")->nullable();
            $table->string("num")->nullable();
            $table->string("bairro")->nullable();
            $table->string("cidade")->nullable();
            $table->char("uf",2)->nullable();
            $table->string("cep",15)->nullable();
            $table->string("tel",15)->nullable();
            $table->date("dtnasc")->nullable();
            $table->string("cidnasc")->nullable();
            $table->string("ufnasc",2)->nullable();
            $table->string("nacionalidade")->nullable();
            $table->string("estcivil")->nullable();
            $table->string("email")->nullable();
            $table->string("identidade")->nullable();
            $table->string("cpf")->nullable();
            $table->string("sexo")->nullable();
            $table->string("igreja")->nullable();
            $table->string("cargo")->nullable();
            $table->string("qualcargo")->nullable();
            $table->string("escolaridade")->nullable();
            $table->string("profissao")->nullable();
            $table->string("formacaoteologica")->nullable();
            $table->string("escola")->nullable();
            $table->string('livro')->nullable();
            $table->string('status')->nullable();
            $table->string('cidade_eetad')->nullable();
            $table->integer('subnucleo_id')->nullable()->unsigned();
            $table->integer('matricula_id')->nullable()->unsigned();
            $table->timestamps();
            
            $table->foreign('subnucleo_id')
                    ->references('id')->on('subnucleo')
                    ->onDelete('RESTRICT');
            $table->foreign('matricula_id')
                    ->references('id')->on('matricula')
                    ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aluno', function($table) {            
            $table->dropForeign('aluno_subnucleo_id_foreign');
            $table->dropForeign('aluno_matricula_id_foreign');            
        });
        Schema::dropIfExists('aluno');
    }
}
