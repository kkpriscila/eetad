<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCicloDtmatriculaAluno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('aluno', function($table) {
           
            $table->text('ciclo')->nullable();
            $table->char('rematricula',1)->nullable();
            $table->date("dtmatricula")->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aluno', function($table) {
            
             $table->dropColumn('dtmatricula');
             $table->dropColumn('rematricula');
             $table->dropColumn('justifica_status');
            
        });
    }
}
