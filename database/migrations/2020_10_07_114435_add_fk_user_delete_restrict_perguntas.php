<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkUserDeleteRestrictPerguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE quiz_perguntas MODIFY COLUMN created_user INT(10) Unsigned");
        DB::statement("ALTER TABLE quiz_perguntas MODIFY COLUMN updated_user INT(10) Unsigned");
        Schema::table('quiz_perguntas', function($table) {
            $table->foreign('created_user')
                    ->references('id')->on('users')
                    ->onDelete('RESTRICT');
            $table->foreign('updated_user')
                    ->references('id')->on('users')
                    ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        DB::statement("ALTER TABLE quiz_perguntas MODIFY COLUMN created_user VARCHAR(191)");
//        DB::statement("ALTER TABLE quiz_perguntas MODIFY COLUMN updated_user VARCHAR(191)");
       Schema::table('quiz_perguntas', function($table) {
            $table->dropForeign('quiz_perguntas_created_user_foreign');
            $table->dropForeign('quiz_perguntas_updated_user_foreign');
        });
    }
}
