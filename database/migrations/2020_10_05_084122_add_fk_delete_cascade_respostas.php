<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDeleteCascadeRespostas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('quiz_respostas', function($table) {
            $table->foreign('pergunta_id')
                    ->references('id')->on('quiz_perguntas')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_respostas', function($table) {
            $table->dropForeign('quiz_respostas_pergunta_id_foreign');
        });
    }
}
