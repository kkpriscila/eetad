<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkSubnucleoDeleteRestrictMatricula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('matricula', function($table) {
           
            $table->foreign('subnucleo_id')
                    ->references('id')->on('subnucleo')
                    ->onDelete('RESTRICT');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matricula', function($table) {
            
            $table->dropForeign('matricula_subnucleo_id_foreign');
            
            
        });
    }
}
