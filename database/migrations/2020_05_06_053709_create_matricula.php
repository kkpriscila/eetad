<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatricula extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('matricula', function (Blueprint $table) {
            $table->increments('id');
            $table->string("subnucleo")->nullable();
            $table->string("resp_subnucleo")->nullable();
            $table->string("email_subnucleo")->nullable();
            $table->string("ciclo")->nullable();
            $table->string("cidade_eetad")->nullable();
            $table->char("estado",2)->nullable();
            $table->string("nucleo")->nullable();
            $table->string("nome");
            $table->string("end")->nullable();
            $table->string("num")->nullable();
            $table->string("bairro")->nullable();
            $table->string("cidade")->nullable();
            $table->char("uf",2)->nullable();
            $table->string("cep",15)->nullable();
            $table->string("tel",15)->nullable();
            $table->date("dtnasc")->nullable();
            $table->string("cidnasc")->nullable();
            $table->string("ufnasc",2)->nullable();
            $table->string("nacionalidade")->nullable();
            $table->string("estcivil")->nullable();
            $table->string("email")->nullable();
            $table->string("identidade")->nullable();
            $table->string("cpf")->nullable();
            $table->string("sexo")->nullable();
            $table->string("igreja")->nullable();
            $table->string("cargo")->nullable();
            $table->string("qualcargo")->nullable();
            $table->string("escolaridade")->nullable();
            $table->string("profissao")->nullable();
            $table->string("formacaoteologica")->nullable();
            $table->string("escola")->nullable();
           $table->date('dttaxamatricula')->nullable();
            $table->string("motivotxmatricula")->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('matricula');
    }

}
