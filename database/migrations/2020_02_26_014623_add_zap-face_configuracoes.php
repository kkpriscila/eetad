<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZapFaceConfiguracoes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('configuracoes', function($table) {
            $table->string('whatsapp')->nullable();
            $table->string('facebook')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('configuracoes', function($table) {
            $table->dropColumn('whatsapp');
            $table->dropColumn('facebook');
        });
    }

}
