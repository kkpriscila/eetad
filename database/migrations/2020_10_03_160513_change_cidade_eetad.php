<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCidadeEetad extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('matricula', function (Blueprint $table) {
            $table->dropColumn('cidade-eetad');
            $table->string('cidade_eetad')->nullable();
//            $table->renameColumn('cidade-eetad', 'cidade_eetad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('matricula', function (Blueprint $table) {
//            $table->renameColumn('cidade_eetad', 'cidade-eetad');
             $table->dropColumn('cidade_eetad');
            $table->string('cidade-eetad')->nullable();
        });
    }

}
