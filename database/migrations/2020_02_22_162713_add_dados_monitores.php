<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosMonitores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitores', function($table) {
        $table->integer('subnucleo_id')->unsigned();
        $table->date('dtnasc')->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monitores', function($table) {
            $table->dropColumn('subnucleo_id');
            $table->dropColumn('dtnasc');
        });
    }
}
